$(document).ready( function (){

  $(".footer_button").click (function () {
    $('body,html').animate ({scrollTop:0},999);
    return false;
  })
//alert( 'Your screen resolution is '+$(window).width() );
  /*jQuery*/

  $(function(){
    var ink, d, x, y;
    $(".ripplelink").click(function(e){
      if($(this).find(".ink").length === 0){
          $(this).prepend("<span class='ink'></span>");
      }
           
      ink = $(this).find(".ink");
      ink.removeClass("animate");
       
      if(!ink.height() && !ink.width()){
          d = Math.max($(this).outerWidth(), $(this).outerHeight());
          ink.css({height: d, width: d});
      }
       
      x = e.pageX - $(this).offset().left - ink.width()/2;
      y = e.pageY - $(this).offset().top - ink.height()/2;
       
      ink.css({top: y+'px', left: x+'px'}).addClass("animate");
  });
  });

  /*    rotate button */
  $(".cool_effect").click(function(e){
    var $clicked = $(this);
    var $pAll = $clicked.find('.p_all');
    var $p5 = $clicked.find('.p_5');
    $pAll.slideToggle("slow", function() {
      if($p5.hasClass("rotate180"))
      {
        $p5.removeClass("rotate180");  
      }else
      {
        $p5.addClass("rotate180");  
      }
    });
  });

  /*      Лучшее за месяц по комментам и просмотрам   */


  $( ".cnt_r_by_comments" ).hover(function() {
    $(".cnt_r_by_comments").toggleClass( "active2" );
    $(".cnt_r_by_views").toggleClass( "activeNone" );
  });

  $( ".cnt_r_by_views" ).hover(function() {
    $(".cnt_r_by_comments").toggleClass( "activeNone" );
    $(".cnt_r_by_views").toggleClass( "active2" );
  });
  
  $(".cnt_r_by_comments").click(function(e){
    //$(".cnt_r_comments").show("slide", { direction: "right" }, 800);
    $(".cnt_r_comments").show(800);
    //$(".cnt_r_views").hide("slide", { direction: "left" }, 800);
    $(".cnt_r_views").hide(800);
    $(".cnt_r_by_views").removeClass("active");
    $(".cnt_r_by_comments").addClass("active");
  });

  $(".cnt_r_by_views").click(function(e){
    //$(".cnt_r_views").show("slide", { direction: "left" }, 800);
    //$(".cnt_r_comments").hide("slide", { direction: "right" },800);
    $(".cnt_r_views").show(800);
    $(".cnt_r_comments").hide(800);
    $(".cnt_r_by_comments").removeClass("active");
    $(".cnt_r_by_views").addClass("active");
  });

  
  $(".header_2_mobile_menu").click(function(e){
    $(".header_2 ul").slideToggle("slow");    
  });

  /*        COMMENT ADMIN */
  var count_click = 0;
  $(".footer_1_2").click(function(e){
    count_click++;
    if(count_click>5)
    {
      $(".ecomment_form_login").css("display","block");
    }

  });

});
                  
