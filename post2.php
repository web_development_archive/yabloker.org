<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="css/posts.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/animations.css">  
  <link rel="stylesheet" type="text/css" href="css/mobile.css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
  <!--<link rel="import" href="http://www.polymer-project.org/components/paper-ripple/paper-ripple.html">-->
  <!-- jQuery -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
  <!--      BOOTSTRAP    
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>-->
</head>
<body>
<!--        HEADER    -->
<div class="header_1" id="top">
  <div class="header_1_1 ripplelink">
    <a href="" class="">Яблокер</a>
  </div>
</div>
<div class="header_2 ">
  <div class="header_2_mobile_menu ripplelink">
    <img src="images/header_2_mobile_menu.png" alt>
  </div>
  <ul>
    <li class="active ripplelink"><a href="">все блоги</a></li>
    <li class="ripplelink"><a href="">Лучшие лайфхаки</a></li>
    <li class="ripplelink"><a href="">Технологии</a></li>
    <li class="ripplelink"><a href="">Вдохновение</a></li>
    <li class="ripplelink"><a href="">Сделай сам</a></li>
  </ul>
</div>
<!--     END   HEADER    -->
<!--        CONTENT    -->

<div class="content">
  <div class="content_l">
    <div class="content_post">
      <div class="post_div">
        <div class="posts_width">
          <div class="p_1">
            Новый взгляд на продуктивность, или Что не так с нашей работой
          </div>
          <div class="p_2">
            <div class="p_2_1">
              <div class="p_2_1_1">Чингиз</div>
              <div class="p_2_1_2"></div>
              <div class="p_2_1_3">
                <img class="p_2_1_3_i" src="images/sidebar_eye.png" alt>
                <div class="p_2_1_3_t">551</div>
              </div>
            </div>
            <div class="p_2_2">
              <img src="images/img.png" alt>
            </div>
            <div class="p_2_3">
              <div class="p_2_3_1">Чингиз</div>
              <div class="p_2_3_2"></div>
              <div class="p_2_3_3">
                <img class="p_2_3_3_i" src="images/sidebar_eye.png" alt>
                <div class="p_2_3_3_t">551</div>
              </div>
            </div>
          </div>
          <div class="p_3">
            <div class="p_3_1">
              <img src="images/img.png">
            </div>
            <div class="p_3_2">
              Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.
            </div>
          </div>
          <div class="p_3_3">
            Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.
            Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.
            Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.
            Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.
            <div class="p_2_1_2"></div>
            <div class="p_3_3_share">
              <div class="p_3_3_share_1">
                Поделиться
                <img src="images/share.png">
              </div>
              <div class="p_3_3_share_2">
                <a href="" class="ripplelink"><img src="images/social/gp.png" alt></a>
                <a href=""><img src="images/social/fb.png" alt></a>
                <a href=""><img src="images/social/vk.png" alt></a>
                <a href=""><img src="images/social/tw.png" alt></a>
                <a href=""><img src="images/social/in.png" alt></a>
              </div>
            </div>
          </div> 
        </div>       
      </div>
      <div class="clr"></div>
    </div>
    <div class="blocks recomendation">- РЕКОМЕНДУЕМ -</div>
    <div class="clr"></div>
    <div class="recomendation_all">
      <a href="">
        <div class="blocks ripplelink recomendation_block">
          <figure>
            <img src="images/img.png">
            <figcaption>
              Новый взгляд на продуктивность, или Что не так с нашей работой
            </figcaption>
          </figure>
        </div>
      </a>
      <a href="">
        <div class="blocks ripplelink recomendation_block2">
          <figure>
            <img src="images/img.png">
            <figcaption>
              Новый взгляд на продуктивность, или Что не так с нашей работой
            </figcaption>
          </figure>
        </div>
      </a>
      <a href="">
        <div class="blocks ripplelink recomendation_block">
          <figure>
            <img src="images/img.png">
            <figcaption>
              Новый взгляд на продуктивность, или Что не так с нашей работой
            </figcaption>
          </figure>
        </div>
      </a>
      <a href="">
        <div class="blocks ripplelink recomendation_block2">
          <figure>
            <img src="images/img.png">
            <figcaption>
              Новый взгляд на продуктивность, или Что не так с нашей работой
            </figcaption>
          </figure>
        </div>
      </a>
    </div>
    <div class="clr"></div>
    <div class="blocks"></div>
  </div>
  <div class="content_r">
    <div class="cnt_r_best">
      <div class="cnt_r_best_1">Лучшее за месяц</div>
      <div class="cnt_r_best_2">
        <div class="ripplelink active cnt_r_by_views">По просмотрам          
        </div>
        <div class="ripplelink cnt_r_by_comments">По комментариям
        </div>
      </div>
      <!--

      <div class="cnt_r_best_2">
        <div class="active cnt_r_by_views material-design">По просмотрам</div>
        <div class="cnt_r_by_comments material-design">По комментариям</div>
      </div>
    -->
      <div class="cnt_r_best_3 cnt_r_views">
        <a href="">
          <div class="cnt_r_best_3_1 ripplelink">
            <div class="cnt_r_best_3_1_1">
              9 вещей, которые миллионеры делают по-другому
            </div>
            <div class="cnt_r_best_3_1_2">
              <img src="images/sidebar_eye.png" alt class="cnt_r_best_3_1_2_i">
              <div class="cnt_r_best_3_1_2_t">21 551</div>
            </div>
          </div>
        </a>
        <a href="">
          <div class="cnt_r_best_3_1 ripplelink">
            <div class="cnt_r_best_3_1_1">
              9 вещей, которые миллионеры делают по-другому
            </div>
            <div class="cnt_r_best_3_1_2">
              <img src="images/sidebar_eye.png" alt class="cnt_r_best_3_1_2_i">
              <div class="cnt_r_best_3_1_2_t">21 551</div>
            </div>
          </div>
        </a>
        <a href="">
          <div class="cnt_r_best_3_1 ripplelink">
            <div class="cnt_r_best_3_1_1">
              9 вещей, которые миллионеры делают по-другому
            </div>
            <div class="cnt_r_best_3_1_2">
              <img src="images/sidebar_eye.png" alt class="cnt_r_best_3_1_2_i">
              <div class="cnt_r_best_3_1_2_t">21 551</div>
            </div>
          </div>
        </a>
        <a href="">
          <div class="cnt_r_best_3_1 ripplelink">
            <div class="cnt_r_best_3_1_1">
              9 вещей, которые миллионеры делают по-другому
            </div>
            <div class="cnt_r_best_3_1_2">
              <img src="images/sidebar_eye.png" alt class="cnt_r_best_3_1_2_i">
              <div class="cnt_r_best_3_1_2_t">21 551</div>
            </div>
          </div>
        </a>
        <a href="">
          <div class="cnt_r_best_3_1 ripplelink">
            <div class="cnt_r_best_3_1_1">
              9 вещей, которые миллионеры делают по-другому
            </div>
            <div class="cnt_r_best_3_1_2">
              <img src="images/sidebar_eye.png" alt class="cnt_r_best_3_1_2_i">
              <div class="cnt_r_best_3_1_2_t">21 551</div>
            </div>
          </div>
        </a>
        <a href="">
          <div class="cnt_r_best_3_1 ripplelink">
            <div class="cnt_r_best_3_1_1">
              9 вещей, которые миллионеры делают по-другому
            </div>
            <div class="cnt_r_best_3_1_2">
              <img src="images/sidebar_eye.png" alt class="cnt_r_best_3_1_2_i">
              <div class="cnt_r_best_3_1_2_t">21 551</div>
            </div>
          </div>
        </a>
        <a href="">
          <div class="cnt_r_best_3_1 ripplelink">
            <div class="cnt_r_best_3_1_1">
              9 вещей, которые миллионеры делают по-другому
            </div>
            <div class="cnt_r_best_3_1_2">
              <img src="images/sidebar_eye.png" alt class="cnt_r_best_3_1_2_i">
              <div class="cnt_r_best_3_1_2_t">21 551</div>
            </div>
          </div>
        </a>
      </div>

      <div class="cnt_r_best_3 cnt_r_comments">
        <a href="">
          <div class="cnt_r_best_3_1 ripplelink">
            <div class="cnt_r_best_3_1_1">
              8 вещей, которые миллионеры делают по-другому
            </div>
            <div class="cnt_r_best_3_1_2">
              <img src="images/sidebar_eye.png" alt class="cnt_r_best_3_1_2_i">
              <div class="cnt_r_best_3_1_2_t">21 551</div>
            </div>
          </div>
        </a>
        <a href="">
          <div class="cnt_r_best_3_1 ripplelink">
            <div class="cnt_r_best_3_1_1">
              8 вещей, которые миллионеры делают по-другому
            </div>
            <div class="cnt_r_best_3_1_2">
              <img src="images/sidebar_eye.png" alt class="cnt_r_best_3_1_2_i">
              <div class="cnt_r_best_3_1_2_t">21 551</div>
            </div>
          </div>
        </a>
        <a href="">
          <div class="cnt_r_best_3_1 ripplelink">
            <div class="cnt_r_best_3_1_1">
              8 вещей, которые миллионеры делают по-другому
            </div>
            <div class="cnt_r_best_3_1_2">
              <img src="images/sidebar_eye.png" alt class="cnt_r_best_3_1_2_i">
              <div class="cnt_r_best_3_1_2_t">21 551</div>
            </div>
          </div>
        </a>
        <a href="">
          <div class="cnt_r_best_3_1 ripplelink">
            <div class="cnt_r_best_3_1_1">
              8 вещей, которые миллионеры делают по-другому
            </div>
            <div class="cnt_r_best_3_1_2">
              <img src="images/sidebar_eye.png" alt class="cnt_r_best_3_1_2_i">
              <div class="cnt_r_best_3_1_2_t">21 551</div>
            </div>
          </div>
        </a>
        <a href="">
          <div class="cnt_r_best_3_1 ripplelink">
            <div class="cnt_r_best_3_1_1">
              8 вещей, которые миллионеры делают по-другому
            </div>
            <div class="cnt_r_best_3_1_2">
              <img src="images/sidebar_eye.png" alt class="cnt_r_best_3_1_2_i">
              <div class="cnt_r_best_3_1_2_t">21 551</div>
            </div>
          </div>
        </a>
        <a href="">
          <div class="cnt_r_best_3_1 ripplelink">
            <div class="cnt_r_best_3_1_1">
              8 вещей, которые миллионеры делают по-другому
            </div>
            <div class="cnt_r_best_3_1_2">
              <img src="images/sidebar_eye.png" alt class="cnt_r_best_3_1_2_i">
              <div class="cnt_r_best_3_1_2_t">21 551</div>
            </div>
          </div>
        </a>
        <a href="">
          <div class="cnt_r_best_3_1 ripplelink">
            <div class="cnt_r_best_3_1_1">
              8 вещей, которые миллионеры делают по-другому
            </div>
            <div class="cnt_r_best_3_1_2">
              <img src="images/sidebar_eye.png" alt class="cnt_r_best_3_1_2_i">
              <div class="cnt_r_best_3_1_2_t">21 551</div>
            </div>
          </div>
        </a>
      </div>
    </div>

    <div class="cnt_r_top">
      <div class="cnt_r_top_1">Топ авторов Яблокера</div>
      <div class="cnt_r_top_3">
        <a href="">
          <div class="cnt_r_top_3_1 ripplelink">
            <div class="cnt_r_top_3_1_1">
              <img src="images/img.png" alt>
            </div>
            <div class="cnt_r_top_3_1_2">
              <div class="cnt_r_top_3_1_2_t">Шелдон Купер</div>
              <div class="cnt_r_top_3_1_2_i">
                <img src="images/img.png" alt>
                <img src="images/img.png" alt>
                <img src="images/img.png" alt>
                <img src="images/img.png" alt>
              </div>
            </div>
            <div class="cnt_r_top_3_1_3"><img src="images/arrow_right.png" alt></div>
          </div>        
        </a>

        <a href="">
          <div class="cnt_r_top_3_1 ripplelink">
            <div class="cnt_r_top_3_1_1">
              <img src="images/img.png" alt>
            </div>
            <div class="cnt_r_top_3_1_2">
              <div class="cnt_r_top_3_1_2_t">Шелдон Купер</div>
              <div class="cnt_r_top_3_1_2_i">
                <img src="images/img.png" alt>
                <img src="images/img.png" alt>
                <img src="images/img.png" alt>
                <img src="images/img.png" alt>
              </div>
            </div>
            <div class="cnt_r_top_3_1_3"><img src="images/arrow_right.png" alt></div>
          </div>        
        </a>
        <a href="">
          <div class="cnt_r_top_3_1 ripplelink">
            <div class="cnt_r_top_3_1_1">
              <img src="images/img.png" alt>
            </div>
            <div class="cnt_r_top_3_1_2">
              <div class="cnt_r_top_3_1_2_t">Шелдон Купер</div>
              <div class="cnt_r_top_3_1_2_i">
                <img src="images/img.png" alt>
                <img src="images/img.png" alt>
                <img src="images/img.png" alt>
                <img src="images/img.png" alt>
              </div>
            </div>
            <div class="cnt_r_top_3_1_3"><img src="images/arrow_right.png" alt></div>
          </div>        
        </a>
        <a href="">
          <div class="cnt_r_top_3_1 ripplelink">
            <div class="cnt_r_top_3_1_1">
              <img src="images/img.png" alt>
            </div>
            <div class="cnt_r_top_3_1_2">
              <div class="cnt_r_top_3_1_2_t">Шелдон Купер</div>
              <div class="cnt_r_top_3_1_2_i">
                <img src="images/img.png" alt>
                <img src="images/img.png" alt>
                <img src="images/img.png" alt>
                <img src="images/img.png" alt>
              </div>
            </div>
            <div class="cnt_r_top_3_1_3"><img src="images/arrow_right.png" alt></div>
          </div>        
        </a>
        <a href="">
          <div class="cnt_r_top_3_1 ripplelink">
            <div class="cnt_r_top_3_1_1">
              <img src="images/img.png" alt>
            </div>
            <div class="cnt_r_top_3_1_2">
              <div class="cnt_r_top_3_1_2_t">Шелдон Купер</div>
              <div class="cnt_r_top_3_1_2_i">
                <img src="images/img.png" alt>
                <img src="images/img.png" alt>
                <img src="images/img.png" alt>
                <img src="images/img.png" alt>
              </div>
            </div>
            <div class="cnt_r_top_3_1_3"><img src="images/arrow_right.png" alt></div>
          </div>        
        </a>

               
      </div>
    </div>

    <div class="cnt_r_part">
      <div class="cnt_r_part_1">Стать частью проекта</div>
      <div class="cnt_r_part_3">
        <div>
          Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.

        </div>
        <a href="" class="ripplelink">Присоединиться!</a>
        <section class="cnt_r_part_3_clr"></section>
      </div>
    </div>
  </div>
</div>

<div class="footer_1">
  <section class="footer_button"><img class="rotate180 ripplelink" src="images/down_icon.png" alt></section>        
  <div>
    <header>
      <ul class="footer_1_1">
        <li>О сайте</li>
        <li>Стать частью проекта</li>
        <li>Яблокер в социальных сетях</li>
      </ul>
      <ul class="footer_1_2">
        <li><a href="" class="ripplelink"><img src="images/social/gp.png" alt></a></li>
        <li><a href=""><img src="images/social/fb.png" alt></a></li>
        <li><a href=""><img src="images/social/vk.png" alt></a></li>
        <li><a href=""><img src="images/social/tw.png" alt></a></li>
        <li><a href=""><img src="images/social/in.png" alt></a></li>
      </ul>
    </header>
    <footer>
      © 2015 yabloker.org. Все права защищены
    </footer>
  </div>
</div>

<script type="text/javascript" src="js/main.js"></script>
</body>
</html>