<?php

class Validation {

    private $data = array();
    private $errors = array();
    private $rules = array();
    private $result = array();

    public function __construct($rules, $data){
        $this->data = $data;
        $this->rules = $rules;
    }

    public function run(){
        foreach($this->rules as $r){
            $this->check($r);
        }

        if(count($this->errors) > 0) return false;
        return true;
    }

    public function check($r){
        $rul = explode('|', $r['rules']);

        foreach($rul as $c){
            if($c == 'required'){
                if(empty($this->data[$r['name']])) $this->errors[] = 'Вы не заполнили поле: <b>'.$r['title'].'</b>';
            }
            if(preg_match('/minlength/i', $c)){
                $temp = explode(':', $c);
                if(strlen($this->data[$r['name']]) < $temp[1]) $this->errors[] = 'Поле <b>'.$r['title'].'</b> должно состояь не менее чем из '.$temp[1].' символов';
            }
            if(preg_match('/notEqualTo/i', $c)){
                $temp = explode(':', $c);
                //if(strlen($this->data[$r['name']]) < $temp[1]) $this->errors[] = 'Поле '.$r['title'].' должно состояь не менее чем из '.$temp[1].' символов';
                if($this->data[$r['name']] == $this->data[$temp[1]]) $this->errors[] = 'Поля <b>'.$r['title'].'</b> не должно быть равно полю '.$temp[2];
            }
            if($c == 'numeric'){
                if(!is_numeric($this->data[$r['name']])) $this->errors[] = 'Поле <b>'.$r['title'].'</b> принимает числовые значения';
            }
        }

    }

    public function getResult(){
        foreach($this->rules as $r) $this->result[$r['name']] = $this->data[$r['name']];
        return $this->result;
    }

    public function getErrors(){
        return $this->errors;
    }
}

?>