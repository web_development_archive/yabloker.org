<?php
/**
 * File: adverts.php
 * Содержит функций связанные с объявлениями
 */


function getAdverts($start = null, $limit = null){
    $query = '';
    if(!is_null($start) && !is_null($limit)) $query = " LIMIT $start, $limit";

    $adverts = q(SQL_GET_ADVERTS, null);
    return $adverts;
}

function changeAdvertStatus($status, $id)
{
    if(q2(SQL_UPDATE_ADVERT_STATUS, array('status' => $status, 'id' => $id))) {
        buildMsg('Статус объявления был успешно изменен', 'success');
        return true;
    }else return false;
}

?>