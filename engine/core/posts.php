<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 29.01.2015
 * Time: 18:26
 */

/**
 * @param null $start
 * @param null $limit
 * @return Посты одного автора
 */
function getPostsOfUser($start = null, $limit = null, $id)
{
    $query = '';
    if(!is_null($start) && !is_null($limit)) $query = " LIMIT $start, $limit";

    $users = q(SQL_GET_POSTS_OF_USER.$query,array('id'=>$id) );
    return $users;
}


/**
 * @return Все посты по рекомендации
 */
function getAllPostsByCat($action)
{
    $id = q(SQL_GET_POST_CAT,array('post_id'=>$action));

    return q(SQL_GET_POSTS_BY_CAT,array('cat_id'=>$id['0']['cat_id']));
}

/**
 * @return Все посты по просмотрам
 */
function getAllPostsByView()
{
    return q(SQL_GET_POSTS_BY_VIEWS,null);
}


/**
 * @return Все посты по просмотрам
 */
function getAllPostsByComments()
{
    return q(SQL_GET_POSTS_BY_COMMENTS,null);
}


/**
 * @param null $start
 * @param null $limit
 * @return Все посты
 */
function getPosts($type = null,$start = null, $limit = null)
{
    $query = '';
    if(!is_null($start) && !is_null($limit)) $query = " LIMIT $start, $limit";

    $users = q(SQL_GET_POSTS.$type.$query,null);
    return $users;
}

/**
 * @param $post_id
 * @return Обновление Комментариев
 */
function updatePostComments($post_id, $count)
{
    if(q2(SQL_UPDATE_POST_COMMENTS, array('post_id'=>$post_id, 'count'=>$count)))
        return true;
    return false;
}

/**
 * @param $post_id
 * @return Обновление просмотров
 */
function updatePostView($post_id)
{
    if(q2(SQL_UPDATE_POST_VIEWS, array('post_id'=>$post_id)))
        return true;
    return false;
}

/**
 * Редактировать статью
 */
function getPost($post_id)
{
    updatePostView($post_id);
    $users = q(SQL_GET_POST_BY_ID,array('post_id'=>$post_id));
    if(count($users) == 0) return false;
    return $users[0];
}

/**
 * @return array
 */
function getGroupsPosts()
{
    $g = q(SQL_GET_POST_CATEGORY, null);
    return $g;
}

/**
 * @return get last post
 */
function getLastPost()
{
    $u = q(SQL_GET_LAST_POST, null);
    if(count($u) == 0) return false;
    return $u[0];
}

/**
 * Edit post
 */
function editPost($post,$file)
{
    $fillable = array(
        'post_name' => 'Название',
        'post_description' => 'Описание',
        'post_text' => 'Текст',
        'post_seo_title' => 'SEO Title',
        'post_seo_description' => 'SEO Description'
    );
    $add_fields = array(
        'post_category' => 'Категория'
    );

    $err = array();
    foreach($fillable as $k=>$v)  if(empty($post[$k])) $err[] = 'Введите поле: '.$v;
    foreach($add_fields as $k=>$v)  if(empty($post[$k])) $err[] = 'Введите поле: '.$v;


    $update = false;

    if($post['image_edit']=="on")
    {
        $uploaddir = 'uploads/images/';
        $uploadfile = $uploaddir.basename($file['name']);
        $data = array();

        if (move_uploaded_file($file['tmp_name'], $uploadfile)) {
            $data['post_image'] = basename($file['name']);
            $update = true;
        } else {
            return false;
        }
    }

    if(count($err) > 0){
        foreach($err as $e) buildMsg($e, 'danger');
        return false;
    }else{

        foreach($fillable as $k=>$v) $data[$k] = $post[$k];
        $data['post_id'] = $post['post_id'];

        if($update==false)
        {


            if(q2(SQL_UPDATE_POST, $data)) {
                if(q2(SQL_UPDATE_CATEGORY, array('post_id'=>$post['post_id'],'post_category'=>$post['post_category']))) {

                    buildMsg('Статья успешно отредактирована');
                    return $data;
                } else
                {
                    buildMsg("SQL_UPDATE_CATEGORY", 'danger');
                    return false;
                }
            } else
            {
                buildMsg("SQL_UPDATE_POST", 'danger');
                return false;
            }

        }else
        {
            if(q2(SQL_UPDATE_POST_WITH_IMG, $data)) {
                if(q2(SQL_UPDATE_CATEGORY, array('post_id'=>$post['post_id'],'post_category'=>$post['post_category']))) {

                    buildMsg('Статья успешно отредактирована');
                    return $data;
                } else
                {
                    buildMsg("SQL_UPDATE_CATEGORY", 'danger');
                    return false;
                }
            } else
            {
                buildMsg("SQL_UPDATE_POST", 'danger');
                return false;
            }
        }
    }

}

/**
 * @param Post
 * @return array|bool
 */
function addPost($post,$file)
{
    $fillable = array(
        'post_name' => 'Название',
        'post_description' => 'Описание',
        'post_text' => 'Текст',
        'post_seo_title' => 'SEO Title',
        'post_seo_description' => 'SEO Description'
    );
    $add_fields = array(
        'post_category' => 'Категория'
    );

    $err = array();
    foreach($fillable as $k=>$v)  if(empty($post[$k])) $err[] = 'Введите поле: '.$v;
    foreach($add_fields as $k=>$v)  if(empty($post[$k])) $err[] .= 'Введите поле: '.$v;

    $uploaddir = 'uploads/images/';
    $uploadfile = $uploaddir.basename($file['name']);
    $data = array();

    if (move_uploaded_file($file['tmp_name'], $uploadfile)) {
        $data['post_image'] = basename($file['name']);
    } else {
        return $data;
    }

    if(count($err) > 0){
        foreach($err as $e) buildMsg($e, 'danger');
        return false;
    }else{

        foreach($fillable as $k=>$v) $data[$k] = $post[$k];



        $data['post_image'] = basename($file['name']);
        $data['post_date'] = date('Y-m-d H:i:s', time());

        //foreach($data as $k=>$v) echo $k." ".$v."<hr/>";

        if(q2(SQL_INSERT_POST, $data)) {
            $id = getLastPost();
            $data['id_post'] = $id[0];

            if(q2(SQL_INSERT_STATUS_POST,array('id_post'=>$data['id_post'])))
            {
                if(q2(SQL_INSERT_CAT_POST,
                    array('id_post'=>$data['id_post'], 'id_category'=>$post['post_category'])))
                {
                    if(q2(SQL_INSERT_POST_AUTHOR,array('id_post'=>$data['id_post'],'id_author'=>$_SESSION['user']['user_id'])))
                    {
                        buildMsg('Статья успешно создана');
                        return $data;
                    } else
                    {
                        buildMsg("SQL_INSERT_POST_AUTHOR", 'danger');
                        return false;
                    }
                }else
                {
                    buildMsg("SQL_INSERT_CAT_POST", 'danger');
                    return false;
                }
            }   else
            {
                buildMsg("SQL_INSERT_STATUS_POST", 'danger');
                return false;
            }

        } else
        {
            buildMsg("SQL_INSERT_POST", 'danger');
            return false;
        }


    }

}

?>