<?php
/**
 * File: companies.php
 * Хранит фукнций связанные с компаниями
 */

function newViewCompany($id)
{
    if(q2(SQL_UPDATE_COMPANY_VIEW,array('company_id'=>$id)))
        return true;
}

function getGoodDials($id)
{
    $c = q(SQL_GET_GOOD_DIALS, array('id' => $id,'idd' => $id));
    return $c[0];
}
function getNullDials($id)
{
    $c = q(SQL_GET_NUlL_DIALS, array('id' => $id));
    return $c[0];
}

function getCompUsers($id)
{
    $c = q(SQL_GET_COMP_USERS, array('id' => $id));
    return $c[0];
}

function getCompanies()
{
    $comps = q(SQL_GET_COMPS, null);
    return $comps;
}

function getCompany($id)
{
    $c = q(SQL_GET_COMP, array('id' => $id));
    return $c[0];
}

function getAllComByInt($id)
{
    $c = q(SQL_GET_ALL_COM_BY_INTERES,array('interest_id'=>$id));
    return $c;
}

function getCompanyInterests($id)
{
    $c = q(SQL_GET_COMPANY_INTERESTS, array('id' => $id));
    return $c;
}

function addInterest($interest, $com_id)
{
    if(!isset($_SESSION['user']))
    {
        return array(
            'status' => 0,
            'error' => 'Добавлять могут только зарегестрированные пользователи'
        );
    }

    $interest = trim($interest);

    if(empty($interest)){
        return array(
            'status' => 0,
            'error' => 'Вы не заполниле поле: Интерес'
        );
    }

    $inter = q(SQL_GET_INTEREST, array('interest' => $interest));

    if(count($inter) == 0){
        if(q2(SQL_INSERT_INTEREST, array('interest' => $interest))) $inter_id = qInsertId();
    }else{
        $inter_id = $inter[0]['id'];

        $interestRelation = q(SQL_GET_COMPANY_INTEREST_ONE, array('inter_id' => $inter_id, 'com_id' => $com_id));

        if(count($interestRelation) != 0){
                return array(
                    'status' => 0,
                    'error' => 'У вас уже есть данный интерес в списке'
                );
        }
    }

    if(q2(INSERT_COMPANY_INTEREST, array('com_id' => $com_id, 'inter_id' => $inter_id))){
        return array(
            'status' => 1,
            'inter_id' => $inter_id,
            'interest' => $interest,
            'message' => 'Интерес добавлен в список интересов компании'
        );
    }
}

function deleteCompanyInterest($id)
{
    $company_id = $_SESSION['user']['company_id'];

    if(q2(DELETE_COMPANY_INTEREST, array('interest_id' => $id, 'company_id' => $company_id))) return array('status' => 1, 'message' => 'Интерес успешно удален');
}

function getAllStaff($id)
{

    $content = q(SQL_GET_ALL_STUFF,array('compId'=>$id));

    if($content==null)
        return false;

    return $content;
}

/*
 *  INSERT to favorites
 */

function getNameOfInteres($id)
{
    $content = q(SQL_GET_NAME_OF_INTERES,array('id'=>$id));
    return $content[0]['title'];
}

function addToFavorites($user_id,$com_id)
{
    if(!isset($_SESSION['user']))
    {
        return array(
            'status' => 0,
            'error' => 'Добавлять могут только зарегестрированные пользователи'
        );
    }

    if($_SESSION['user']['id'] != $user_id)
        return array(
            'status' => 0,
            'error' => 'Нельзя добавлять в избранное другого пользователя'
        );

    $fav = q(SQL_GET_FAVORITE, array('user_id' => $user_id, 'com_id' => $com_id));

    if($fav != false){
        if(q2(SQL_DELETE_FAV, array('user_id' => $user_id, 'com_id' => $com_id))){
            return array(
                'status' => 1,
                'deleted' => 1,
                'message' => 'Компания успешно удалена из списка избранных'
            );
        }
    }else{
        if(q2(SQL_ADD_TO_FAVORITES, array('user_id' => $user_id, 'com_id' => $com_id)))
        {
            return array(
                'status' => 1,
                'deleted' => 0,
                'message' => 'Компания успешно добавлена В Избранное'
            );
        }
    }
}

function getFavorite($user, $company)
{
    $fav = q(SQL_GET_FAVORITE, array('user_id' => $user, 'com_id' => $company));
    if($fav[0]['user_id'] == $user AND $fav[0]['com_id'] == $company) return $fav[0];
    return false;
}

function editCompany($comp, $file){

    include_once ROOT."/engine/classes/Validation.php";

    $fillable = array(
        array(
            'name' => 'title',
            'title' => 'Наименование',
            'rules' => 'required|minlength:10'
        ),
        array(
            'name' => 'skype',
            'title' => 'Skype',
            'rules' => 'required'
        ),
        array(
            'name' => 'com_mail',
            'title' => 'Email',
            'rules' => 'required'
        ),
        array(
            'name' => 'phone',
            'title' => 'Телеофн',
            'rules' => 'required'
        ),
        array(
            'name' => 'address',
            'title' => 'Адрес',
            'rules' => 'required'
        ),
        array(
            'name' => 'description',
            'title' => 'Описание',
            'rules' => 'required'
        )
    );

    $valid = new Validation($fillable, $comp);

    if($valid->run() == true){

        $logotype = $comp['old_logo'];

        if(!empty($file['tmp_name'])){
            $logotype = uploadImage($file, array('using' => false, 'type' => 'company', 'target' => 0));
            $logotype = $logotype['id'];
            if($comp['old_logo'] != 0) deleteImage($comp['old_logo']);
        }

        $data = array();
        $data = $valid->getResult();
        $data['lat'] = $comp['lat'];
        $data['lng'] = $comp['lng'];
        $data['company_id'] = $comp['company_id'];
        $data['business_type'] = $comp['business_type'];
        $data['logotype'] = $logotype;
        $data['logic'] = ($comp['logic'] == 1) ? 1 : 0;
        $data['buy'] = ($comp['buy'] == 1) ? 1 : 0;
        $data['sale'] = ($comp['sale'] == 1) ? 1 : 0;

        if(q2(SQL_UPDATE_COMPANY, $data)){
            buildMsg('Компания отредактирована');
            return true;
        }return false;

    }else{
        $errors = $valid->getErrors();
        foreach($errors as $e) buildMsg($e, 'danger');
        return false;
    }
}

function deleteCompany($id)
{
    if($_SESSION['user']['group_id'] != 1){
        buildMsg('У вас нет прав для этой функций', 'warning');
        return false;
    }

    if(q2(SQL_DELETE_COMPANY, array('id' => $id))) return true;
    else return false;
}

function signUpNewCompany($comp, $file)
{
    include_once ROOT."/engine/classes/Validation.php";

    $fillable = array(
        array(
            'name' => 'title',
            'title' => 'Название',
            'rules' => 'required'
        ),
        array(
            'name' => 'phone',
            'title' => 'Телефон',
            'rules' => 'required'
        ),
        array(
            'name' => 'skype',
            'title' => 'Skype',
            'rules' => 'required'
        ),
        array(
            'name' => 'com_mail',
            'title' => 'Электонная почта',
            'rules' => 'required'
        ),
        array(
            'name' => 'address',
            'title' => 'Адрес',
            'rules' => 'required'
        ),
        array(
            'name' => 'description',
            'title' => 'Описание',
            'rules' => 'required'
        )
    );

    $valid = new Validation($fillable, $comp);
    $valid->run();
    $err = $valid->getErrors();

    $user_id = (!empty($_SESSION['regUser'])) ? $_SESSION['regUser'] : 0;

    if(count($comp['regions']) == 0) $err[] = 'Вы не выбрали ни один <b>регион</b>.';
    if($user_id == 0) $err[] = 'Нужно сначала пройти регистрацию';

    if(count($err) == 0){

        $avatar = 0;
        if(!empty($file['name'])){
            $ava = uploadImage($file, array('using' => false, 'type' => 'company', 'target' => 0));
            $avatar = $ava['id'];
        }

        $buy = ($comp['buy'] == 1) ? 1 : 0;
        $sale = ($comp['sale'] == 1) ? 1 : 0;
        $logic = ($comp['logic'] == 1) ? 1 : 0;
        $dt = date('Y-m-d H:i:s');

        if(q2(SQL_INSERT_NEW_COMPANY, array(
            'title' => $comp['title'],
            'description' => $comp['description'],
            'logotype' => $avatar,
            'phone' => $comp['phone'],
            'skype' => $comp['skype'],
            'com_mail' => $comp['com_mail'],
            'business_type' => $comp['business_type'],
            'address' => $comp['address'],
            'lat' => $comp['lat'],
            'lng' => $comp['lng'],
            'buy' => $buy,
            'sale' => $sale,
            'logic' => $logic,
            'reg_date' => $dt
        ))){
            $comp_id = qInsertId();
            foreach($comp['regions'] as $reg) addCompanyRegion($comp_id, $reg);
            addCompanyWorker($comp_id, $user_id);
            unset($_SESSION['regUser']);

            buildMsg('Вы успешно создали свой аккаунт и компанию. Теперь вы можете войти в систему.');
            return true;
        }
        return false;


    }else{
        foreach($err as $e) buildMiniMsg($e);
        return false;
    }
}

function addCompanyWorker($comp_id, $user_id)
{
    $dt = date('Y-m-d H:i:s');
    if(q2(INSERT_COMPANY_WORKER, array('comp_id' => $comp_id, 'user_id' => $user_id, 'dt' => $dt, 'status' => 1, 'type' => 0))) return true;
    else return false;
}

?>