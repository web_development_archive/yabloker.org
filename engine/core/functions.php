<?php
/**
 *
 * Файл: functions.php
 * Здесь хранятся функций, которые несут вспомогательный характер
 * Которые никак не являются основой самого сайта
 *
 */

function send_message($subject, $body, $name, $address)
{
    global $config;

    include_once ROOT."/engine/classes/PHPMailer/PHPMailerAutoload.php";

    $mail = new PHPMailer;
    $mail->isMail();
    $mail->CharSet = 'UTF-8';
    $mail->From = 'info@'.$config['home'];
    $mail->FromName = 'Ярд Инвест';
    $mail->addAddress($address, $name);     // Add a recipient
    $mail->addReplyTo('info@'.$config['home'], 'Information');
    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = $subject;
    $mail->Body    = $body;

    $mail->send();
}

function getSearchResults($term, $type)
{
    switch($type){
        case 0:
            $result = q(SQL_GET_GOODS_BY_SEARCH, array('term' => '%'.$term.'%'));
            return $result;
            break;
        case 1:
            $result = q(SQL_GET_BUYERS_BY_SEARCH, array('term' => '%'.$term.'%', 'term1' => '%'.$term.'%'));
            return $result;
            break;
    }
}

function getAttachments($table, $target)
{
    /*
     * $table
     * 0 - компания
     * 1 - товар
     * 2 - сообщение
     */

    $files = q(SQL_GET_ATTACHMENTS, array('table' => $table, 'target' => $target));
    return $files;
}

function addFeedback($msg){
    include_once ROOT."/engine/classes/PHPMailer/PHPMailerAutoload.php";

    $mail = new PHPMailer;
    $mail->isMail();
    $mail->CharSet = 'UTF-8';
    $mail->From = $msg['mail'];
    $mail->FromName = $msg['name'];
    $mail->addAddress('pein-nagato-real@yandex.ru', 'Рустем');     // Add a recipient
    $mail->addReplyTo('info@yard-invest.ru', 'Information');
    $mail->isHTML(true);                                  // Set email format to HTML
    //<p>Здравствуйте, '.$data['name'].'! Вы успешно зарегистрировались на сайте yard-invest.ru</p><p>Ваши даннын для входа в систему:</p><p><b>Логин (email)</b>: '.$data['email'].'</p><p><b>Пароль:</b> '.$passFor.'</p><p>Для того, чтобы подтвердить ваш email адрес вам нужно перейти по этой ссылке: <a href="http://yard-invest.ru/login.php?action=activation&hash='.$hash.'" target="_blank">http://yard-invest.ru/login.php?action=activation&hash='.$hash.'</a></p><p>Если вы не регистрировались на сайте и вам это письмо пришло ошибочно, то просто проигнорируйте данное письмо.</p><p>Спасибо за вниманеи</p>
    $mail->Subject = $msg['subject'];
    $mail->Body  = $msg['txt'];

    $mail->send();

    buildMsg('Сообщение отправлено!');
}

function totranslit($var, $lower = true, $punkt = true) {
    global $langtranslit;

    if ( is_array($var) ) return "";

    if (!is_array ( $langtranslit ) OR !count( $langtranslit ) ) {

        $langtranslit = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
            'ь' => '', 'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
            "ї" => "yi", "є" => "ye",

            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
            'И' => 'I', 'Й' => 'Y', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
            'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
            'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
            "Ї" => "yi", "Є" => "ye",
        );

    }

    $var = trim( strip_tags( $var ) );
    $var = preg_replace( "/\s+/ms", "-", $var );

    $var = strtr($var, $langtranslit);

    if ( $punkt ) $var = preg_replace( "/[^a-z0-9\_\-.]+/mi", "", $var );
    else $var = preg_replace( "/[^a-z0-9\_\-]+/mi", "", $var );

    $var = preg_replace( '#[\-]+#i', '-', $var );

    if ( $lower ) $var = strtolower( $var );

    $var = str_ireplace( ".php", "", $var );
    $var = str_ireplace( ".php", ".ppp", $var );

    if( strlen( $var ) > 200 ) {

        $var = substr( $var, 0, 200 );

        if( ($temp_max = strrpos( $var, '-' )) ) $var = substr( $var, 0, $temp_max );

    }

    return $var;
}

function enc($str)
{
    return htmlspecialchars(trim(addslashes(strip_tags($str))));
}

function reArrayFiles(&$file_post)
{

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}

/*
 * Создает сообщение
 * @param string $text - текст сообщения
 * @param string $class - класс
 */
function buildMsg($text, $class = "success")
{
    $_SESSION['messages'][] = array('text' => $text, 'class' => $class);
}

/*
 * Создает мини-сообщение (пример: регистрация)
 */

function buildMiniMsg($text)
{
    $_SESSION['messagesMini'][] = $text;
}

/*
 * Вытаскивает сообщения, которые находятся в сессий messages
 * Потом удаляет их
 * Используется для вывода сообщений справа
 */

function getMessages()
{
    $messages = '';
    if(isset($_SESSION['messages']) && is_array($_SESSION['messages'])){
        foreach($_SESSION['messages'] as $msg){
            $messages .= "<div class=\"alert alert-{$msg['class']} alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>{$msg['text']}</div>";
        }
        unset($_SESSION['messages']);
    }
    return $messages;
}
/*
 * Вытаскивает сообщения, которые находятся в сессий messagesMini
 * Потом удаляет их
 * Используется для вывода сообщений справа
 */

function getMiniErrors()
{
    $messages = '<ul style="padding:0;margin:0">';
    if(count($_SESSION['messagesMini']) == 0) return false;
    if(isset($_SESSION['messagesMini'])){
        foreach($_SESSION['messagesMini'] as $msg){
            $messages .= "<li>{$msg}</li>";
        }
        unset($_SESSION['messagesMini']);
    }
    return $messages.'</ul>';
}

/*
 * Используется для показа существительных в множественной форме
 */

function pluralForm($n, $form1, $form2, $form5)
{
    $n = abs($n) % 100;
    $n1 = $n % 10;
    if ($n > 10 && $n < 20) return $form5;
    if ($n1 > 1 && $n1 < 5) return $form2;
    if ($n1 == 1) return $form1;
    return $form5;
}


/*
 * Функция rdate
 * Переводится дату из вида 2014-02-01
 * В вид
 * 01 фев 2014
 */

function rdate($time)
{
    $date =	explode("-", date($time));
    switch ($date[1]){
        case 1: $m='янв'; break;
        case 2: $m='фев'; break;
        case 3: $m='мар'; break;
        case 4: $m='апр'; break;
        case 5: $m='мая'; break;
        case 6: $m='июня'; break;
        case 7: $m='июля'; break;
        case 8: $m='авг'; break;
        case 9: $m='сен'; break;
        case 10: $m='окт'; break;
        case 11: $m='ноя'; break;
        case 12: $m='дек'; break;
    }
    return $date[2].'&nbsp;'.$m.'&nbsp;'.$date[0];
}

/*
 * Функция rdateComplete
 * Переводится дату из вида 2014-07-01
 * В вид
 * 01 июня 2014
 */

function rdateComplete($time)
{
    $date =	explode("-", date($time));
    switch ($date[1]){
        case 1: $m='января'; break;
        case 2: $m='февраля'; break;
        case 3: $m='марта'; break;
        case 4: $m='апреля'; break;
        case 5: $m='мая'; break;
        case 6: $m='июня'; break;
        case 7: $m='июля'; break;
        case 8: $m='августа'; break;
        case 9: $m='сентября'; break;
        case 10: $m='октября'; break;
        case 11: $m='ноября'; break;
        case 12: $m='декабря'; break;
    }
    return $date[2].'&nbsp;'.$m.'&nbsp;'.$date[0];
}

?>