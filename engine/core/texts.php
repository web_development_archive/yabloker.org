<?php

function getTexts(){
    $txt = q(SQL_GET_TEXTS, null);
    return $txt;
}

function getTextOne($id)
{
    $txt = q(SQL_GET_TEXT, array('id' => $id));
    return $txt[0];
}

function editText($text)
{
    $id = $text['text_id'];
    $name = $text['name'];
    $txt = $text['txt'];

    $err = array();
    if(empty($name)) $err[] = 'Заполите поле Наименование';
    if(empty($txt)) $err[] = 'Заполите поле Текст';

    if(count($err) > 0){
        foreach($err as $e) buildMsg($e, 'danger');
        return false;
    }else{
        if(q2(UPDATE_TEXT_BLOCK, array('id' => $id, 'name' => $name, 'txt' => $txt))) {
            buildMsg('Текстовый блок отредактирован');
            return true;
        }
        else return false;
    }
}

function showTextBlock($id){
    $txt = q(SQL_GET_TEXT, array('id' => $id));
    return $txt[0]['txt'];
}

?>