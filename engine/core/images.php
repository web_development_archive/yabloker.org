<?php
/**
 * Файл: images.php
 * Хранит функций связанные с изображениями
 */

function getImageThumb($image, $width = 150, $height = 150)
{
    global $config;
    include_once "/engine/classes/WideImage/WideImage.php";//ROOT.

    $original = $config['imagesUpload'].$image;
    $tDirName = $width.'x'.$height;
    $tDir = $config['thumbsDir'].$tDirName.DS;
    $tFileName = $tDir.$image;

    if(file_exists($tFileName)) return $config['home'].$config['thumbs'].$tDirName.DS.$image;

    if(file_exists($original)){
        $size = getimagesize($original);

        if($size[0] < $width && $size[1] < $height) return $config['home'].$config['images'].$image;

        if(!file_exists($tDir)) mkdir($tDir, 0777);

        $images = WideImage::loadFromFile($original)->resize($width+100)->crop("top", "left", $width, $height);
        $images->saveToFile($tFileName);

        return $config['home'].$config['thumbs'].$tDirName.DS.$image;
    }else return 'http://placehold.it/'.$tDirName;
}

function getImage($id){
    $i = q(SQL_GET_IMAGE, array('id' => $id));
    return $i[0];
}

function deleteImage($id){
    global $config;

    $image = getImage($id);
    if(q2(SQL_DELETE_IMAGE, array('id' => $id))){

        unlink($config['imagesUpload'].$image['filename']); // Удаляет изображение
        unlink($config['thumbsDir'].$image['filename']);	// Удаляет фотографию

        //search_file($config['thumbsDir2'], $image['filename']);

        foreach(glob($config['thumbsDir'].'*/'.$image['filename']) as $file){
            unlink($file);
        }

        return true;
    }else return false;
}



function uploadImage($file, $avatar = array('using' => false, 'type' => 'user', 'target' => 0))
{
    global $config;
    include_once "/engine/classes/WideImage/WideImage.php";//ROOT.

    // Проверка на изображение
    $type = explode('.', $file['name']);
    $types = array('gif', 'png', 'jpg', 'jpeg');
    if(!in_array(end($type), $types)) return false;

    $imageName = time().totranslit(basename($file['name']));			 // Название изображения
    $imageDir  = $config['imagesUpload'].$imageName;		 // Полная картина путь

    $fullImage = WideImage::loadFromFile($file['tmp_name']); // Полная картинка

    $fullImage->saveToFile($imageDir);

    if(q2(SQL_ADD_NEW_IMAGE, array('name' => $imageName, 'good_id' => $avatar['target']))){
        $imageId = qInsertId();
        if($avatar['using'] == true){ // Если нужно обновить аватар
            switch($avatar['type']){
                case "good":
                    setGoodImage($avatar['target'], $imageId);
                    break;
            }
        }
        return array('id' => $imageId, 'name' => $imageName);
    }   else return false;

}


function uploadImageToPost($file, $avatar = array('using' => false, 'type' => 'user', 'target' => 0))
{
    global $config;
    include_once "/engine/classes/WideImage/WideImage.php";//ROOT.

    // Проверка на изображение
    $type = explode('.', $file['name']);
    $types = array('gif', 'png', 'jpg', 'jpeg');
    if(!in_array(end($type), $types)) return false;

    $imageName = time().totranslit(basename($file['name']));			 // Название изображения
    $imageDir  = $config['imagesUpload'].$imageName;		 // Полная картина путь

    $fullImage = WideImage::loadFromFile($file['tmp_name']); // Полная картинка

    $fullImage->saveToFile($imageDir);

    return $imageName;

}

?>