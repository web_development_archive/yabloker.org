<?php
/**
 * Файл: users.php
 * Хранит функций, связанные с пользователями
 */

// Попытка залогиниться
// возвращает либо FALSE либо все данные пользователя, взятые из БД
function engineLoginAttempt($phone, $pass)
{
    $users = q(SQL_LOGIN_ATTEMPT, array('phone'=>$phone, 'pass'=>$pass));
    if(count($users)==0) return FALSE;
    return $users[0];
}

function getUserByEmail($mail)
{
    $user = q(SQL_GET_USER_BY_MAIl, array('mail' => $mail));
    if(count($user) == 0) return false;
    return $user[0];
}

// Регистрация нового пользователя на сайте
function signUpNewUser($post, $file)
{
    include_once ROOT."/engine/classes/Validation.php";

    $fillable = array(
        array(
            'name' => 'name',
            'title' => 'Имя',
            'rules' => 'required'
        ),
        array(
            'name' => 'patro',
            'title' => 'Отчество',
            'rules' => 'required'
        ),
        array(
            'name' => 'surname',
            'title' => 'Фамилия',
            'rules' => 'required'
        ),
        array(
            'name' => 'mail',
            'title' => 'Email',
            'rules' => 'required'
        ),
        array(
            'name' => 'pass',
            'title' => 'Пароль',
            'rules' => 'required'
        ),
        array(
            'name' => 'city',
            'title' => 'Город',
            'rules' => 'required'
        ),
        array(
            'name' => 'birthday',
            'title' => 'Дата рождения',
            'rules' => 'required'
        ),
        array(
            'name' => 'sex',
            'title' => 'Пол',
            'rules' => 'numeric'
        )
    );

    $valid = new Validation($fillable, $post);
    $valid->run();
    $err = $valid->getErrors();

    $temp = getUserByEmail($post['mail']);
    if($temp !== false) $err[] = 'Пользователь с такой <b>Электронной почтой</b> уже зарегестрирован';

    if(count($err) == 0){

        $avatar = 0;
        if(!empty($file['name'])){
            $ava = uploadImage($file, array('using' => false, 'type' => 'user', 'target' => 0));
            $avatar = $ava['id'];
        }

        $data = array();
        $data = $valid->getResult();
        $data['avatar'] = $avatar;
        $data['group_id'] = 3;
        $data['balance'] = 0;

        if(q2(SQL_INSERT_USER, array(
            'name' => $post['name'],
            'patro' => $post['patro'],
            'surname' => $post['surname'],
            'sex' => $post['sex'],
            'avatar' => $avatar,
            'mail' => $post['mail'],
            'pass'=> md5($post['pass']),
            'city' => $post['city'],
            'birthday' => $post['birthday'],
            'group_id' => 3,
            'balance' => 0
        ))){
            $_SESSION['regUser'] = qInsertId();
            buildMsg('Второй шаг. Создайте компанию.');
            return true;
        }
        return false;


    }else{
        foreach($err as $e) buildMiniMsg($e);
        return false;
    }
}

function getSession()
{
    global $config;

    return $_SESSION['user'];
}

function getUsers($start = null, $limit = null)
{
    $query = '';
    if(!is_null($start) && !is_null($limit)) $query = " LIMIT $start, $limit";

    $users = q(SQL_GET_USERS.$query, null);
    return $users;
}

function getLastUser()
{
    $u = q(SQL_GET_LAST_USER, null);
    if(count($u) == 0) return false;
    return $u[0];
}

function getUser($id)
{
    $u = q(SQL_GET_USER, array('id' => $id));
    if(count($u) == 0) return false;
    return $u[0];
}

function editUser($post)
{
    $fillable = array(
        'surname' => 'Фамилия',
        'name' => 'Имя',
        'patro' => 'Отчество',
        'city' => 'Город',
        'mail' => 'Почта',
        'birthday' => 'Дата рождения',
        'sex' => 'Пол',
        'info_others'=>'Примечание'
    );
    $main_information = array(
        'nickname'=>'Кличка',
        'main_phone'=>'Главный телефон'
    );

    $err = array();
    foreach($fillable as $k=>$v)  if(empty($post[$k])) $err[] = 'Введите поле: '.$v;
    foreach($main_information as $k=>$v)  if(empty($post[$k])) $err[] = 'Введите поле: '.$v;

    if(empty($post['group_id'])) $group_id = $post['old_group'];
    else $group_id = $post['group_id'];

    if($post['group_id'] == 2 && $_SESSION['user']['type_id'] != 0) $err[] .= 'Вы не имеете права добавлять администраторов';
    if($post['group_id'] == 3 && $_SESSION['user']['type_id'] != 0) $err[].= 'Вы не имеете права добавлять модераторов';

    if(count($err) > 0){
        foreach($err as $e) buildMsg($e, 'danger');
        return false;
    }else{
        $data = array();
        $data['id'] = $post['user_id'];

        foreach($fillable as $k=>$v) $data[$k] = $post[$k];

        if(q2(SQL_UPDATE_USER_INFO, $data)) {
            //$data['group_id'] = $group_id;
            if(q2(SQL_UPDATE_USER_GROUP, array('group_id'=>$group_id,'id'=>$post['user_id']))) {
                if(q2(SQL_UPDATE_USER,array('id'=>$post['user_id'],'nickname'=>$post['nickname'],
                'main_phone'=>$post['main_phone'])))
                {
                    buildMsg('Пользователь успешно отредактирован');
                    return $data;
                }else return $data;
            }   else return $data;

        } else return $data;
    }

}

// Add worker
function addWorker($post,$file)
{
    $fillable = array(
        'surname' => 'Фамилия',
        'name' => 'Имя',
        'patro' => 'Отчество',
        'city' => 'Город',
        'sex'=>'Пол',
        'birthday' => 'Дата рождения',
        'group_id' => 'Группа',
        'pass' => 'Пароль',
        'mail' => 'Email'
    );

    $err = array();
    foreach($fillable as $k=>$v)  if(empty($post[$k])) $err[] = 'Вы не заполнили поле: '.$v;
    if($file==null)
    {
        $err[] = "Вы не заполнили поле: Фото сотрудника";
    }

    if(count($err) > 0){
        foreach($err as $e) buildMsg($e, 'danger');
        return false;
    }else{
        $data = array();
        $data['sex'] = $post['sex'];
        $avatar = uploadImage($file, array('using' => false, 'type' => 'user', 'target' => 0));
        $data['avatar'] = $avatar['id'];
        $data['balance'] = 0;

        foreach($fillable as $k=>$v) $data[$k] = $post[$k];
//:mail, :surname, :name, :patro, :city, :birthday, :sex, :group_id, :avatar, :pass, :balance)");
        if(q2(SQL_INSERT_WORKERS, $data)) {

            $getUserId = qInsertId();

            if(q2(INSERT_COMPANY_WORKER, array('comp_id' => $_SESSION['user']['company_id'], 'user_id' => $getUserId, 'dt' => $data['birthday'], 'status' => 0, 'type' => 0)))
            {
                $compName = getCompany($_SESSION['user']['company_id']);
                $login = $data['mail'];
                $pass = $data['pass'];

                $body= <<<HTML
                <h3>Поздравляем! Вы добавлены к компании {$compName['title']}</h3>
                <p>Ваш логин: <strong>{$login}</strong> </p>
                <p>Ваш пароль: <strong>{$pass}</strong> </p>
HTML;
                send_message('Добавление пользователя', $body, $data['name'],$data['mail']);
                buildMsg('Пользователь успешно создан');
                return true;
            }

        }   else return false;

    }

}

// Добавления пользователя через админку
function addUser($post)
{
    $fillable = array(
        'surname' => 'Фамилия',
        'name' => 'Имя',
        'patro' => 'Отчество',
        'city' => 'Город',
        'mail' => 'Почта',
        'birthday' => 'Дата рождения',
        'sex' => 'Пол',
        'info_others'=>'Примечание'
    );
    $main_information = array(
        'nickname'=>'Кличка',
        'main_phone'=>'Главный телефон',
        'pass'=>'Пароль',
        'group_id'=>'Группа'
    );

    $err = array();
    foreach($fillable as $k=>$v)  if(empty($post[$k])) $err[] = 'Введите поле: '.$v;
    foreach($main_information as $k=>$v)  if(empty($post[$k])) $err[] .= 'Введите поле: '.$v;

    if(count($err) > 0){
        foreach($err as $e) buildMsg($e, 'danger');
        return false;
    }else{
        $data = array();

        foreach($fillable as $k=>$v) $data[$k] = $post[$k];

        if(q2(SQL_INSERT_USER, array('nickname'=>$post['nickname'],
            'main_phone'=>$post['main_phone'], 'pass'=>$post['pass']))) {
            //$data['group_id'] = $group_id;

            //$id = getUserIdByNickname($post['nickname']);
            $id = getLastUser();
            $data['id'] = $id[0];
            if(q2(SQL_INSERT_USER_INFO, $data)) {

                $group_id = $post['group_id'];
                if(q2(SQL_INSERT_USER_TYPE_USER,array(
                    'id_user'=>$data['id'],'id_type'=>$group_id)))
                {
                    buildMsg('Пользователь успешно создан');
                    return $data;
                }else return $data;
            }   else return $data;

        } else return $data;


    }

}

/**
 * Check User Priority
 */

function checkUserPriority()
{

}

/**
 * @param $id
 * @return bool
 */
function getUserIdByNickname($nickname)
{
    $u = q(SQL_GET_USER_BY_NICKNAME, array('nickname' => $nickname));
    if(count($u) == 0) return false;
    return $u[0];
}

function deleteUser($id)
{
    if($_SESSION['user']['type_id'] != 1){
        buildMsg('У вас нет прав для этой функций', 'warning');
        return false;
    }

    if(q2(SQL_DELETE_USER, array('id' => $id))) return true;
    else return false;
}

function getGroups()
{
    $g = q(SQL_GET_GROUPS, null);
    return $g;
}

?>