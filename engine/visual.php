<?php

include_once "visual/posts.php";
include_once "visual/posts.php";// Функций вывода связанные с пользователями


/*
 * Текстовые блоки (соглашение, услуги)
 */

function showTexts()
{
    $texts = getTexts();
    if(count($texts) == 0) return 'Ничего не найдено';

    $show = '<table class="table table-bordered table-clickable"><tr><th>ID</th><th>Наименование</th></tr>';
    foreach($texts as $text){
        $show .= "<tr onclick=\"window.location.href='texts.php?action=edit&text_id={$text['id']}'\"><td>{$text['id']}</td><td>{$text['name']}</td></tr>";
    }
    $show .= '</table>';

    return $show;
}

function editTextForm($id)
{
    $text = getTextOne($id);

    return <<<HTML
    <form action="texts.php?action=doEdit" method="post">
        <div class="form-group">
            <label>Название</label>
            <input type="text" name="name" value="{$text['name']}" class="form-control" />
        </div>
        <div class="form-group">
            <label>Текст</label>
            <textarea name="txt" class="form-control" id="fields"  rows="7">{$text['txt']}</textarea>
        </div>
        <div class="form-group">
            <input type="hidden" name="text_id" value="{$text['id']}" />
            <input type="submit" value="Редактировать" class="btn btn-default btn-sm" name="sub" />
        </div>
    </form>
HTML;

}

/*
 * Adverts
 */

function showAdverts()
{
    include_once ROOT."/engine/classes/Pagination/Pagination.class.php";
    global $config;

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $adverts = getAdverts($start, $limit);

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    if(count($adverts) == 0) return 'Ничего не найдено';

    $show = <<<HTML
    <div class="panel panel-primary">
        <div class="panel-heading">Управление объявлениями контексной рекламы</div>
        <table class="table table-bordered">
        <tr><th>Фото</th><th>Название</th><th>Статус</th><th>Дата создания</th><th>Старт</th><th>Конец</th><th>Создал</th></tr>
HTML;

    foreach($adverts as $adv){
        $m[$adv['status']] = ' selected';
        $status = <<<HTML
        <form action="adverts.php?action=changeStatus" class="form-inline" method="post">
            <input type="hidden" name="advert_id" value="{$adv['id']}" />
            <select name="status" class="form-control input-sm">
                <option value="0"{$m[0]}>Проходит модерацию</option>
                <option value="1"{$m[1]}>Модерация пройдена</option>
                <option value="2"{$m[2]}>Модерация не пройдена</option>
            </select>
            <input type="submit" name="sub" value="Сменить" class="btn btn-default btn-sm" />
        </form>
HTML;

        $show .= '<tr><td><img src="'.$adv['filename'].'" width="30" /></td><td>'.$adv['title'].'</td><td>'.$status.'</td><td>'.$adv['dt_created'].'</td><td>'.$adv['dt_start'].'</td><td>'.$adv['dt_finish'].'</td><td><a href="index.php?action=edit&user_id='.$adv['user_id'].'">'.$adv['name'].'</a></td></tr>';
    }
    $show .= '</table>';
    if(!empty($markup)) $show .= '<div class="panel panel-footer">'.$markup.'</div>';
    $show .= '</div>';

    return $show;
}

/*
 * Deals
 */

function showDeals()
{
    include_once ROOT."/engine/classes/Pagination/Pagination.class.php";
    global $config;

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $deals = getDeals($start, $limit);

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    if(count($deals) == 0) return 'Ничего не найдено';

    $show = '<div class="panel panel-info"><div class="panel-heading">Сделки</div><table class="table table-bordered table-clickable"><tr><th>Тип</th><th>Товар</th><th>Между</th><th>Дата сделки</th><th>Дата отгрузки</th><th>Статус</th></tr>';

    foreach($deals as $d){
        switch($d['type']){
            case 0: $d['type'] = 'Покупка'; break;
            case 1: $d['type'] = 'Продажа'; break;
            case 2: $d['type'] = 'Логистика'; break;
        }
        switch($d['status']){
            case 0: $d['status'] = 'Ожидает выполнения'; break;
            case 1: $d['status'] = 'Завершен'; break;
            case 2: $d['status'] = 'Отменена'; break;
        }
        $show .= '<tr onclick="document.location.href=\'deals.php?action=edit&deal_id='.$d['id'].'\'"><td>'.$d['type'].'</td><td>'.$d['good'].'</td><td><a href="index.php?action=edit&user_id='.$d['first'].'">'.$d['firstName'].'</a> - <a href="index.php?action=edit&user_id='.$d['second'].'">'.$d['secondName'].'</a></td><td>'.$d['make_dt'].'</td><td>'.$d['shipment_dt'].'</td><td>'.$d['status'].'</td>';
    }
    $show .= '</table></div>';

    return $show;
}

function editDealForm($id)
{
    $deal = getDeal($id);

    $users = getUsers(null, null);
    $goods = getGoods(null, null);

    $type[$deal['type']] = ' selected="selected"';

    $first = '';
    $second = '';
    foreach($users as $u){
        $a1 = ($u['id'] == $deal['first']) ? ' selected="selected"' : '';
        $a2 = ($u['id'] == $deal['second']) ? ' selected="selected"' : '';

        $first .= '<option value="'.$u['id'].'"'.$a1.'>'.$u['name'].'</option>';
        $second .= '<option value="'.$u['id'].'"'.$a2.'>'.$u['name'].'</option>';
    }

    $good = '';
    foreach($goods as $g){
        $a = ($g['id'] == $deal['good_id']) ? ' selected="selected"' : '';
        $good .= '<option value="'.$g['id'].'"'.$a.'>'.$g['title'].'</option>';
    }
    $status[$deal['status']] = ' selected';

    return <<<HTML
    <h2>Редактирование сделки</h2>
    <form action="deals.php?action=doEdit" method="post">
        <div class="row">
            <div class="col-sm-4">
                <label>Тип</label>
                <select name="type" class="form-control">
                    <option value="0"{$type[0]}>Покупка</option>
                    <option value="1"{$type[1]}>Продажа</option>
                    <option value="2"{$type[2]}>Логистика</option>
                </select>
            </div>
            <div class="col-sm-4">
                <label>Первый пользователь</label>
                <select name="first" class="form-control">
                    {$first}
                </select>
            </div>
            <div class="col-sm-4">
                <label>Второй пользователь</label>
                <select name="second" class="form-control">
                    {$second}
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <label>Товар</label>
                <select name="good_id" class="form-control">
                    {$good}
                </select>
            </div>
            <div class="col-sm-4">
                <label>Дата сделки</label>
                <input type="text" name="make_dt" value="{$deal['make_dt']}" class="form-control datetimepicker" />
            </div>
            <div class="col-sm-4">
                <label>Дата отгрузки</label>
                <input type="text" name="shipment_dt" value="{$deal['shipment_dt']}" class="form-control datetimepicker" />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <label>Статус</label>
                <select name="status" class="form-control">
                    <option value="0"{$status[0]}>Ожидает выполнения</option>
                    <option value="1"{$status[1]}>Завершен</option>
                    <option value="2"{$status[2]}>Отменена</option>
                </select>
            </div>
            <div class="col-sm-7">
                <label>Комментарий</label>
                <textarea class="form-control" name="comment">{$deal['comment']}</textarea>
            </div>
        </div>
        <div class="row">
            <input type="hidden" name="deal_id" value="{$deal['id']}" />
            <input type="submit" name="sub" class="btn btn-default" />
        </div>
    </form>
HTML;

}

/*
 * Services
 */

function showServices()
{
    $services = getServices();

    $show = '<div class="panel panel-default"><div class="panel-heading">Управление ценами платных услуг</div><table class="table table-bordered table-clickable"><tr><th>Название</th><th>Цена</th></tr>';

    foreach($services as $s){
        $show .= '<tr onclick="document.location.href=\'services.php?action=edit&ser_id='.$s['id'].'\'"><td>'.$s['name'].'</td><td>'.$s['price'].' руб</td><td><a href="services.php?action=delete&ser_id='.$s['id'].'"><i class="glyphicon glyphicon-remove"></i></a></td></tr>';
    }
    $show .= '</table></div>';
    return $show;
}

function editServiceForm($id)
{
    $service = getService($id);

    return <<<HTML
    <form action="services.php?action=doEdit" method="post">
        <div class="row">
            <div class="col-sm-8">
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="name" class="form-control" value="{$service['name']}" />
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Цена</label>
                    <input type="text" name="price" class="form-control" value="{$service['price']}" />
                </div>
            </div>
        </div>
        <div class="row">
            <input type="hidden" name="ser_id" value="{$service['id']}" />
            <input type="submit" value="Отправить" class="btn btn-default" name="sub" />
        </div>
    </form>
HTML;

}

/*
 * Companies
 */

function editCompanyForm($id)
{
    global $config;
    $c = getCompany($id);

    $b = array();
    $b[$c['business_type']] = ' selected';

    $c['logotype'] = (!empty($c['logotype'])) ? $c['logotype'] : 0;

    $c['logo'] = (empty($c['logo'])) ? 'http://placehold.it/150x150' : $config['home'].$config['thumbs'].$c['logo'];

    $buy = ($c['buy'] == 1) ? 'checked' : '';
    $sale = ($c['sale'] == 1) ? 'checked' : '';
    $logic = ($c['logic'] == 1) ? 'checked' : '';

    return <<<HTML
    <h2>Редактирование компании</h2>
        <form action="companies.php?action=doEdit" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form-group">
                                    <label>Наименование</label>
                                    <input type="text" name="title" value="{$c['title']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>Загрузить логотип</label>
                                    <input type="file" name="logotype" />
                                </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Skype</label>
                                <input type="text" name="skype" value="{$c['skype']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" name="com_mail" value="{$c['com_mail']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Телефон</label>
                                <input type="text" name="phone" value="{$c['phone']}" class="form-control input-sm" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Адрес</label>
                                <input type="text" name="address" id="address" value="{$c['address']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Тип</label>
                                <select name="business_type" class="form-control input-sm">
                                    <option value="0"{$b[0]}>Организация</option>
                                    <option value="1"{$b[1]}>Частный предприниматель</option>
                                    <option value="2"{$b[2]}>Неофицальный бизнес</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-lg-4 col-sm-4">
                    <div class="form-group">
                        <label>Описание</label>
                        <textarea name="description" rows="8" class="form-control input-sm">{$c['description']}</textarea>
                    </div>
                </div>
                <div class="col-lg-2 col-lg-2 col-sm-2">
                    <img src="{$c['logo']}" />
                </div>
            </div>
            <div class="row">
                <input type="hidden" name="old_logo" value="{$c['logotype']}" />
                <input type="hidden" name="lat" id="lat" value="{$c['lat']}" />
                <input type="hidden" name="lng" id="lng" value="{$c['lng']}" />
                <input type="hidden" name="company_id" value="{$c['id']}" />
                <div class="row" style="width:360px">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label><input type="checkbox" value="1" name="sale" {$sale} /> Продажа</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label><input type="checkbox" value="1" name="buy" {$buy} /> Покупка</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label><input type="checkbox" value="1" name="logic" {$logic} /> Логистика</label>
                        </div>
                    </div>
                </div>
                <input type="submit" name="sub" value="Редактировать" class="btn btn-default" />
            </div>
        </form>
        <script>
			    $(document).ready(function(){
			        $('#address').change(function(){
			            var val = $(this).val();
                        val = val.replace(/\s/g, '+');

			            $.ajax({
			                type: 'get',
			                url: 'ajax.php?action=address',
			                data: {address: 'http://maps.googleapis.com/maps/api/geocode/json?address='+val+'&sensor=false&language=ru'},
			                success: function(data){
			                    var js = $.parseJSON(data);

			                    $('#lat').val(js.results[0].geometry.location.lat);
			                    $('#lng').val(js.results[0].geometry.location.lng);
			                }
			            });
			        });
			    });

        </script>
HTML;

}

function showAllCompanies()
{
    global $config;
    include_once ROOT."/engine/classes/Pagination/Pagination.class.php";

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $comps = getCompanies($start, $limit);

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    if(count($comps) == 0) return 'Ничего не найдено';

    $show = '<div class="panel panel-default"><div class="panel-heading">Управление компаниями</div><table class="table table-bordered table-clickable"><tr><th>Название</th><th>Телефон</th><th>Email</th><th>Skype</th><th>Тип</th><th>Адрес</th></tr>';

    foreach($comps as $c){
        switch($c['business_type']){
            case 0: $type = 'Организация'; break;
            case 1: $type = 'Частный предприниматель'; break;
            case 2: $type = 'Неофицальный бизнес'; break;
        }
        $show .= '<tr onclick="window.location.href=\'companies.php?action=edit&company_id='.$c['id'].'\'"><td>'.$c['title'].'</td><td>'.$c['phone'].'</td><td>'.$c['com_mail'].'</td><td>'.$c['skype'].'</td><td>'.$type.'</td><td>'.$c['address'].'</td><td><a href="companies.php?action=delete&company_id='.$c['id'].'"><i class="glyphicon glyphicon-remove"></i></a></td></tr>';
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= '</table>'.$markup;

    return $show;
}

/*
 * Категории
 */

function showAllCats()
{
    global $config;
    include_once ROOT."/engine/classes/Pagination/Pagination.class.php";

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $users = getCats($start, $limit);

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    if(count($users) == 0) return 'Ничего не найдено'; // Если нет строк

    $menu = <<<HTML
    <nav class="navbar navbar-default" role="navigation">
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li><a href="goods.php?action=toModerate">Товары для модерации</a></li>
        <li class="active"><a href="cats.php">Категории</a></li>
        <li><a href="cats.php?action=new">Добавить категорию</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
</nav>
HTML;


    $show = '<div class="panel panel-info"><div class="panel-heading">Управление категориями</div>'.$menu.'<table class="table table-bordered table-clickable"><tr><th>Название</th><th>Родитель</th></tr>';
    // Разбор массива пользователей
    foreach($users as $u)
    {
        if(empty($u['parent'])) $u['parent'] = 'Отсутствует';
        $show .= "<tr onclick=\"window.location.href='cats.php?action=edit&cat_id={$u['id']}'\"><td>{$u['name']}</td><td>{$u['parent']}</td><td><a href=\"cats.php?action=delete&cat_id={$u['id']}\"><i class='glyphicon glyphicon-remove'></i></a></td>";
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= "</table>".$markup;
    return $show;

}

function editCatForm($id)
{
    $cat = getCat($id);

    $act = array();
    $act[$cat['parent_id']] = ' selected';

    $cats = getCats();
    $options = '';

    foreach($cats as $c) $options .= '<option value="'.$c['id'].'"'.$act[$c['id']].'>'.$c['name'].'</option>';
    unset($cats);

    return <<<HTML
    <form action="cats.php?action=doEdit" method="post">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="name" value="{$cat['name']}" class="form-control input-sm" />
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="form-group">
                    <label>Родитель</label>
                    <select name="parent_id" class="form-control input-sm">
                        <option value="0"{$act[0]}> Родитель отсутствует</option>
                        {$options}
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <input type="hidden" name="cat_id" value="{$cat['id']}" />
            <input type="submit" class="btn btn-default" value="Редактировать" />
        </div>
    </form>
HTML;

}

function addCatForm()
{

    $cats = getCats();
    $options = '';

    foreach($cats as $c) $options .= '<option value="'.$c['id'].'">'.$c['name'].'</option>';
    unset($cats);

    return <<<HTML
    <form action="cats.php?action=doAdd" method="post">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="name" value="" class="form-control input-sm" />
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="form-group">
                    <label>Родитель</label>
                    <select name="parent_id" class="form-control input-sm">
                        <option value="0"> Родитель отсутствует</option>
                        {$options}
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <input type="submit" class="btn btn-default" value="Добавить" />
        </div>
    </form>
HTML;

}

/*
 * Goods
 */

function showAllGoods()
{
    global $config;
    include_once ROOT."/engine/classes/Pagination/Pagination.class.php";

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $users = getGoods($start, $limit, " ORDER BY g.id DESC");

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    // End pagination

    if(count($users) == 0) return 'Ничего не найдено'; // Если нет строк

    $menu = <<<HTML
    <nav class="navbar navbar-default" role="navigation">
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li><a href="goods.php?action=toModerate">Товары для модерации</a></li>
        <li><a href="cats.php">Категории</a></li>
        <li><a href="goods.php?action=new">Добавить товар</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
    </nav>
HTML;


    $show = '<div class="panel panel-info"><div class="panel-heading">Управление товарами</div>'.$menu.'<table class="table table-bordered table-clickable"><tr><th>Название</th><th>Цена</th><th>Компания</th><th>Категория</th></tr>';
    // Разбор массива пользователей
    foreach($users as $u)
    {
        $show .= "<tr onclick=\"window.location.href='goods.php?action=edit&good_id={$u['id']}'\"><td>{$u['title']}</td><td>{$u['price']}</td><td>{$u['company']}</td><td>{$u['cat']}</td><td><a href=\"goods.php?action=delete&good_id={$u['id']}\"><i class='glyphicon glyphicon-remove'></i></a></td>";
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= "</table>".$markup;
    return $show;
}

function showGoodsToModerate()
{
    global $config;
    include_once ROOT."/engine/classes/Pagination/Pagination.class.php";

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $users = getGoods($start, $limit, ' WHERE g.cat_id = 1');

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    // End pagination

    if(count($users) == 0) return 'Ничего не найдено'; // Если нет строк

    $menu = <<<HTML
    <nav class="navbar navbar-default" role="navigation">
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="goods.php?action=toModerate">Товары для модерации</a></li>
        <li><a href="cats.php">Категории</a></li>
        <li><a href="goods.php?action=new">Добавить товар</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
</nav>
HTML;

    $show = '<div class="panel panel-info"><div class="panel-heading">Управление товарами для модерации</div>'.$menu.'<table class="table table-bordered table-clickable"><tr><th>Название</th><th>Цена</th><th>Компания</th><th>Категория</th></tr>';
    // Разбор массива пользователей
    foreach($users as $u)
    {
        $show .= "<tr onclick=\"window.location.href='goods.php?action=edit&good_id={$u['id']}'\"><td>{$u['title']}</td><td>{$u['price']}</td><td>{$u['company']}</td><td>{$u['cat']}</td><td><a href=\"goods.php?action=delete&good_id={$u['id']}\"><i class='glyphicon glyphicon-remove'></i></a></td>";
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= "</table>".$markup;
    return $show;
}

function editGoodForm($id)
{
    $good = getGood($id);

    $cSelect = '<select name="company_id" class="form-control input-sm">';
    $comps = getCompanies();

    foreach($comps as $c){
        $active = ($c['id'] == $good['company_id']) ? ' selected' : '';
        $cSelect .= '<option value="'.$c['id'].'"'.$active.'>'.$c['title'].'</option>';
    }
    $cSelect .= '</select>';
    
    $c1Select = '<select name="cat_id" class="form-control input-sm">';

    unset($comps);

    $cats = getCats();

    foreach($cats as $c){
        $active = ($c['id'] == $good['cat_id']) ? ' selected' : '';
        $c1Select .= '<option value="'.$c['id'].'"'.$active.'>'.$c['name'].'</option>';
    }
    $c1Select .= '</select>';

    unset($cats);

    return <<<HTML
    <h2>Редактирование товара</h2>
        <form action="goods.php?action=doEdit" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7">
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="form-group">
                                <label>Название</label>
                                <input type="text" name="title" value="{$good['title']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <label>Загузить фото</label>
                            <input type="file" name="image" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Компания</label>
                                {$cSelect}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Категория</label>
                                {$c1Select}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Цена</label>
                                <input type="text" name="price" value="{$good['price']}" class="form-control input-sm" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-lg-5 col-sm-5">
                    <div class="form-group">
                        <label>Описание</label>
                        <textarea name="description" rows="5" class="form-control input-sm">{$good['description']}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <input type="hidden" name="good_id" value="{$good['id']}" />
                <input type="hidden" name="old_image" value="{$good['image_id']}" />
                <input type="submit" name="sub" value="Редактировать" class="btn btn-default" />
                <a href="goods.php?action=images&good_id={$good['id']}" class="btn btn-info">Картинки</a>
            </div>
        </form>
HTML;

}

function showGoodImages($id)
{
    $iArray = getGoodImages($id);
    if(count($iArray) == 0) $images = 'Фотографий отсутствуют';
    else{
        $images = '<ul id="goodImages">';
        foreach($iArray as $img)
        {
            $url = getImageThumb($img['filename']);
            $images .= <<<HTML
            <li><img src="{$url}" /><a href="goods.php?action=deleteImage&image_id={$img['id']}&good_id={$id}">X</a></li>
HTML;
        }
        $images .= '</ul>';
    }

    return <<<HTML
    <div class="row">
        <h2>Фотографий / <a href="goods.php?action=edit&good_id={$id}"><small>Назад</small></a></h2>
    </div>
    <div class="row">
        <div class="col-sm-7">
            {$images}
        </div>
        <div class="col-sm-5">
            <form action="goods.php?action=uploadImage" method="POST" enctype="multipart/form-data">
                <input type="file" name="image" />
                <input type="hidden" name="good_id" value="{$id}" />
                <input type="submit" value="Загрузить" name="subImage" />
            </form>
        </div>
    </div>
HTML;

}

function addGoodForm()
{

    $cSelect = '<select name="company_id" class="form-control input-sm">';
    $comps = getCompanies();

    foreach($comps as $c){
        $cSelect .= '<option value="'.$c['id'].'">'.$c['title'].'</option>';
    }
    $cSelect .= '</select>';

    $c1Select = '<select name="cat_id" class="form-control input-sm">';

    unset($comps);

    $cats = getCats();

    foreach($cats as $c){
        $c1Select .= '<option value="'.$c['id'].'">'.$c['name'].'</option>';
    }
    $c1Select .= '</select>';

    unset($cats);

    return <<<HTML
    <h2>Добавление товара</h2>
        <form action="goods.php?action=doAdd" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7">
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="form-group">
                                <label>Название</label>
                                <input type="text" name="title" value="" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <label>Загузить фото</label>
                            <input type="file" name="image" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Компания</label>
                                {$cSelect}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Категория</label>
                                {$c1Select}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Цена</label>
                                <input type="text" name="price" value="" class="form-control input-sm" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-lg-5 col-sm-5">
                    <div class="form-group">
                        <label>Описание</label>
                        <textarea name="description" rows="5" class="form-control input-sm"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <input type="submit" name="sub" value="Добавить" class="btn btn-default" />
            </div>
        </form>
HTML;

}

/*
 * Пользователи
 */

function showUsers()
{
    global $config;
    include_once ROOT."/engine/classes/Pagination/Pagination.class.php";

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $users = getUsers($start, $limit);

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    // End pagination

    if(count($users) == 0) return 'Ничего не найдено'; // Если нет строк

    $show = '<div class="panel panel-default"><div class="panel-heading">Управление пользователями / <a href="index.php?action=new">Добавить пользователя</a></div><table class="table table-bordered table-clickable"><tr><th>Nickname</th><th>Группа</th><th>Телефон</th></tr>';
    // Разбор массива пользователей
    foreach($users as $u)
    {
        $show .= "<tr onclick=\"window.location.href='index.php?action=edit&user_id={$u['user_id']}'\"><td>{$u['user_nickname']}</td><td>{$u['type_name']}</td><td>{$u['user_phone']}</td><td><a href=\"index.php?action=delete&user_id={$u['user_id']}\"><i class='glyphicon glyphicon-remove'></i></a></td>";
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= "</table>".$markup;
    return $show;
}

function showAddUserForm()
{
    $groups = getGroups();
    $groupsSelect = '<select name="group_id" class="form-control input-sm"><option>Выберите группу</option>';

    $i = 0;
    foreach($groups as $g) {
        $disable = '';//($g['id'] == 1 || ($_SESSION['user']['addModer'] == 0 && $g['id'] == 2)) ? ' disabled' : '';
        $groupsSelect .= '<option value="'.$g['type_id'].'"'.$disable.'>'.$g['type_name'].'</option>';
    }
    $groupsSelect .= '</select>';

    return <<<HTML
    <h2>Добавление Пользователя</h2>
        <form action="users.php?action=doAdd" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Фамилия</label>
                                <input type="text" name="surname" value="" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Имя</label>
                                <input type="text" name="name" value="" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Отчество</label>
                                <input type="text" name="patro" value="" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Логин</label>
                                <input type="text" name="nickname" value="" class="form-control input-sm" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Город</label>
                                <input type="text" name="city" value="" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Пол</label>
                                <select name="sex" class="form-control input-sm">
                                    <option value="0">Женский</option>
                                    <option value="1">Мужской</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Дата рождения</label>
                                <input type="date" name="birthday" value="" class="datepicker form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Группа</label>{$groupsSelect}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Почта</label>
                                <input type="mail" name="mail" value="" class="datepicker form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Главный телефон</label>
                                <input type="number" name="main_phone" value="" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Примечание</label>
                                <textarea  name="info_others" class="form-control input-sm"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Пароль</label>
                                <input type="text" name="pass" value="" class="form-control input-sm" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
            <input type="submit" name="sub" value="Добавить" class="btn btn-default" />
            </div>
        </form>
HTML;
}

function showUserEditForm($user_id)
{
    $u = getUser($user_id);
    if($u == false) return 'Такого пользователя не существует';

    $groups = getGroups();
    $groupsSelect = '<select name="group_id" class="form-control input-sm">"';
    foreach($groups as $g) {
        $active = ($g['type_id'] == $u['type_id']) ? ' selected' : '';
        $disable = '';//($g['id'] == 1 || ($_SESSION['user']['addModer'] == 0 && $g['id'] == 2)) ? ' disabled' : '';
        $groupsSelect .= '<option value="'.$g['type_id'].'"'.$active.$disable.'>'.$g['type_name'].'</option>';
    }
    $groupsSelect .= '</select>';

    $sex[$u['info_sex']] = ' selected';
    return <<<HTML
        <h2>Редактирование пользоватея</h2>
        <form action="users.php?action=doEdit" method="post">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Фамилия</label>
                                <input type="text" name="surname" value="{$u['info_familiya']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Имя</label>
                                <input type="text" name="name" value="{$u['info_imya']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Отчество</label>
                                <input type="text" name="patro" value="{$u['info_otchstvo']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Логин</label>
                                <input type="text" name="nickname" value="{$u['user_nickname']}" class="form-control input-sm" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Город</label>
                                <input type="text" name="city" value="{$u['info_address']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Пол</label>
                                <select name="sex" class="form-control input-sm">
                                    <option value="0"{$sex[0]}>Женский</option>
                                    <option value="1"{$sex[1]}>Мужской</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Дата рождения</label>
                                <input type="date" name="birthday" value="{$u['info_bd']}" class="datepicker form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Группа</label>{$groupsSelect}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Почта</label>
                                <input type="mail" name="mail" value="{$u['info_mail']}" class="datepicker form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Главный телефон</label>
                                <input type="number" name="main_phone" value="{$u['user_phone']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Примечание</label>
                                <textarea  name="info_others" class="form-control input-sm">{$u['info_others']}</textarea>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <input type="hidden" name="old_group" value="{$u['type_id']}" />
                                <input type="hidden" name="user_id" value="{$u['user_id']}" />
                                <input type="submit" name="sub" value="Редактировать" class="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

            </div>
        </form>
HTML;

}

/*
 * Админка
 */

/**
 * All
*/
function showAllFlood()
{
    $content = <<<HTML
    <div class="blocks content_header_text">- Время менять этот мир к лучшему. Начнем с себя -</div>
  <div class="clr30"></div>
  <div>
    <div class="content_1 blocks">
      <div class="content_1_header">
        Сообщения / <div class="content_1_header_new">+1</div>
      </div>
      <div class="content_1_footer">
        <img src="images/admin_sms.png">
      </div>
    </div>
    <div class="clr34"></div>
    <div class="content_1 blocks">
      <div class="content_1_header">
        Написать новый шедевр
      </div>
      <div class="content_1_footer">
        <img src="images/admin_plus.png">
      </div>
    </div>
    <div class="clr34"></div>
    <div class="content_1 blocks">
      <div class="content_1_header">
        Профиль
      </div>
      <div class="content_1_footer">
        <div class="content_1_footer_1">
          <div class="content_1_footer_1_n">312</div>
          <div class="content_1_footer_1_n">1200</div>
          <div class="content_1_footer_1_n">543</div>
        </div>
        <div class="content_1_footer_1">
          <div class="content_1_footer_1_t">статей</div>
          <div class="content_1_footer_1_t">просмотров</div>
          <div class="content_1_footer_1_t">комментария</div>
        </div>
      </div>
    </div>
  </div>
  <div class="clr30"></div>

  <div class="blocks content_header_text">- Мои статьи -</div>
  <div class="clr30"></div>
  <div class="content_table">
    <table class="blocks">
      <tr>
        <th>Статус</th>
        <th>Заголовок</th>
        <th>дата</th>
        <th>категория</th>
        <th>просмотры</th>
      </tr>
      <tr>
        <td><img src="images/status_done.png"></td>
        <td>Новый взгляд на продуктивность, или Что не так с нашей работой</td>
        <td>25 Янвая 2015</td>
        <td> Лайфхаки</td>
        <td>99 921</td>
      </tr>
      <tr>
        <td><img src="images/status_time.png"></td>
        <td>Новый взгляд на продуктивность, или Что не так с нашей работой</td>
        <td>25 Янвая 2015</td>
        <td> Лайфхаки</td>
        <td>99 921</td>
      </tr>
      <tr>
        <td><img src="images/status_x.png"></td>
        <td>Новый взгляд на продуктивность, или Что не так с нашей работой</td>
        <td>25 Янвая 2015</td>
        <td> Лайфхаки</td>
        <td>99 921</td>
      </tr>
    </table>
  </div>
HTML;
    return $content;
}

/**
 * Все пользователи
 */

function showAllUsers()
{
    global $config;
    include_once ROOT."/engine/classes/Pagination/Pagination.class.php";//ROOT.

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $users = getUsers($start, $limit, " ORDER BY u.user_id DESC");

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    // End pagination

    if(count($users) == 0) return 'Ничего не найдено'; // Если нет строк


    $show = '<table class="blocks">
              <tr>
                <th>Логин</th>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Телефон</th>
                <th>Группа</th>
              </tr>';
    // Разбор массива пользователей
    foreach($users as $u)
    {
        $show .= "
<tr onclick=\"window.location.href='users.php?action=edit&user_id={$u['user_id']}'\">
<td>{$u['user_nickname']}</td><td>{$u['info_imya']}</td><td>{$u['info_familiya']}</td><td>{$u['user_phone']}</td><td>{$u['type_name']}</td>";
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= "</table>".$markup;
    return $show;
}

// getPosts
function showAllPosts()
{
    global $config;
    include_once ROOT."/classes/Pagination/Pagination.class.php";//ROOT.

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $users = getPosts($start, $limit);

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    // End pagination


    if(count($users) == 0) return 'Ужас! Постов нет!'; // Если нет строк


    $show = '<table class="blocks">
              <tr>
                <th>Статус</th>
                <th>Заголовок</th>
                <th>дата</th>
                <th>категория</th>
                <th>просмотры</th>
              </tr>';

    /*
     *


     */
    // Разбор массива пользователей
    foreach($users as $u)
    {
        if($u['status_id']==1) $stat = '<img src="images/status_done.png">';
        if($u['status_id']==2) $stat = '<img src="images/status_time.png">';
        if($u['status_id']==3) $stat = '<img src="images/status_x.png">';
        if($u['status_id']==4) $stat = '<img src="images/status_time.png">';
        $show .= "
<tr onclick=\"window.location.href='posts.php?action=edit&post_id={$u['post_id']}'\">
<td>{$stat}</td><td>{$u['post_name']}</td><td>{$u['post_date']}</td>
<td>{$u['cat_name']}</td><td>{$u['post_views']}</td>";
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= "</table>".$markup;
    return $show;
}


/**
 * Вывод страницы админки
 */

function showContentAdmin($user)
{

    $posts = showAllPostsOfUser($user['user_id']);
    $html ='<div class="content">
  <div class="blocks content_header_text">- Время менять этот мир к лучшему. Начнем с себя -</div>
  <div class="clr30"></div>
  <div>
    <div class="content_1 blocks">
      <div class="content_1_header">
        Сообщения / <div class="content_1_header_new">+1</div>
      </div>
      <div class="content_1_footer">
        <img src="images/admin_sms.png">
      </div>
    </div>
    <div class="clr34"></div>
    <div class="content_1 blocks">
      <div class="content_1_header">
        Написать новый шедевр
      </div>
      <div class="content_1_footer" onclick="window.location.href=\'posts.php?action=new\'">
        <img src="images/admin_plus.png">
      </div>
    </div>
    <div class="clr34"></div>
    <div class="content_1 blocks">
      <div class="content_1_header">
        Профиль
      </div>
      <div class="content_1_footer">
        <div class="content_1_footer_1">
          <div class="content_1_footer_1_n">312</div>
          <div class="content_1_footer_1_n">1200</div>
          <div class="content_1_footer_1_n">543</div>
        </div>
        <div class="content_1_footer_1">
          <div class="content_1_footer_1_t">статей</div>
          <div class="content_1_footer_1_t">просмотров</div>
          <div class="content_1_footer_1_t">комментария</div>
        </div>
      </div>
    </div>
  </div>
  <div class="clr30"></div>

  <div class="blocks content_header_text">- Мои статьи -</div>
  <div class="clr30"></div>

  <div class="content_table">
  '.$posts.'

  </div>
</div>
<div class="clr40"></div>';

    return $html;
}

/**
 * @return Вывод меню для главной в админке для Богов
 */
function showGodContent()
{
    $html = '
            <div class="blocks content_header_text">- Время менять этот мир к лучшему. Начнем с себя -</div>
          <div class="clr30"></div>
          <div>
            <div class="content_1 blocks content_1_show_first">
              <div class="content_1_header">
                Пользователи
              </div>
              <div class="content_1_header_add" onclick="window.location.href=\'users.php?action=new\'">+</div>
            </div>
            <div class="clr34"></div>
            <div class="content_1 blocks content_1_show_second">
              <div class="content_1_header">
                Страницы
              </div>
              <div class="content_1_header_add" onclick="window.location.href=\'pages.php?action=new\'">+</div>
            </div>
            <div class="clr34"></div>
            <div class="content_1 blocks content_1_show_third">
              <div class="content_1_header">
                Посты
              </div>
              <div class="content_1_header_add" onclick="window.location.href=\'posts.php?action=new\'">+</div>
            </div>
          </div>
          <div class="clr30"></div>
          <div class="blocks content_header_text content_table_users">- Авторы -</div>
          <div class="blocks content_header_text content_table_pages">- Страницы -</div>
          <div class="blocks content_header_text content_table_posts">- Посты -</div>
            <div class="clr30"></div>
          <div class="content_table content_table_users">
            '.showAllUsers().'
          </div>
          <div class="content_table content_table_pages">
    <table class="blocks">
      <tr>
        <th>Статус</th>
        <th>Заголовок</th>
        <th>дата</th>
        <th>категория</th>
        <th>просмотры</th>
      </tr>
      <tr>
        <td><img src="images/status_done.png"></td>
        <td>Новый взгляд на продуктивность, или Что не так с нашей работой</td>
        <td>25 Янвая 2015</td>
        <td> Лайфхаки</td>
        <td>99 921</td>
      </tr>
      <tr>
        <td><img src="images/status_time.png"></td>
        <td>Новый взгляд на продуктивность, или Что не так с нашей работой</td>
        <td>25 Янвая 2015</td>
        <td> Лайфхаки</td>
        <td>99 921</td>
      </tr>
      <tr>
        <td><img src="images/status_x.png"></td>
        <td>Новый взгляд на продуктивность, или Что не так с нашей работой</td>
        <td>25 Янвая 2015</td>
        <td> Лайфхаки</td>
        <td>99 921</td>
      </tr>
    </table>
  </div>
          <div class="content_table content_table_posts">
          '.showAllPosts().'

          </div>
          ';

    return $html;
}

function renderAuth() // окошко авторизации
{

    $alert = '';
    if($_REQUEST['authorize']==1) // Если мы сюда попали с предыдущей попытки авторизации,
    {
        // сообщим пользователю, что он неудачник
        $alert = "
				<script>
					alert('В доступе отказано!')
				</script>";
    }


    $html = "
			<html>
				<head>
					<title>
						Вход в админстраторскую панель
					</title>
					<link rel='stylesheet' type='text/css' href='css/main.css' />
				</head>
				<body>
					$alert
					<div id='authDiv'>
						<h2>Представьтесь:</h2>
						<form method='POST'>
							<input type='number' name='phone' placeholder='7 775 296 39 94'/>
							<input type='password' name='pass' placeholder='Пароль'/>
							<input type='hidden' name='authorize' value='1'>
							<input type='submit' value='Войти'>
						</form>
					</div>
				</body>
			</html>
			";
    return $html;
}

function renderMenu($active)
{

    $highlight = array();
    $highlight[$active] = " active";
    /*$html = ($_SESSION['user']['usersAccess']) ? '<li'.$highlight[0].'><a href="index.php"><i class="glyphicon glyphicon-user"></i> Пользователи</a></li>' : '';
    $html .= ($_SESSION['user']['goodsAccess']) ? '<li'.$highlight[1].'><a href="goods.php"><i class="glyphicon glyphicon-th-list"></i> Каталог товаров</a></li>' : '';
    $html .= ($_SESSION['user']['compsAccess']) ? '<li'.$highlight[2].'><a href="companies.php"><i class="glyphicon glyphicon-home"></i> Компании</a></li>' : '';
*/

    $html = '<ul>
        <li class="'.$highlight[0].' ripplelink"><a href="http://yabloker.org/">все блоги</a></li>
        <li class="'.$highlight[1].' ripplelink"><a href="index.php?action=lifehacks">Лучшие лайфхаки</a></li>
        <li class="'.$highlight[2].' ripplelink"><a href="index.php?action=technology">Технологии</a></li>
        <li class="'.$highlight[3].' ripplelink"><a href="index.php?action=inspiration">Вдохновение</a></li>
        <li class="'.$highlight[4].' ripplelink"><a href="index.php?action=doit">Сделай сам</a></li>
    </ul>';
    return $html;
}


/**
 * @return Показать рекомендации
 */
function showRecommendation($action)
{

    $show ='';

    $all = getAllPostsByCat($action);

    $show .= <<<HTML
    <div class="blocks recomendation">- РЕКОМЕНДУЕМ -</div>
    <div class="clr"></div>
    <div class="recomendation_all">
HTML;

    $count = 0;
    foreach($all as $a)
    {
        if($count%2==0) $class= 'recomendation_block';
        else $class= 'recomendation_block2';

        $show .= <<<HTML
            <a href="post.php?post={$a['post_id']}">
        <div class="blocks ripplelink {$class}">
          <figure>
            <img src="admin/uploads/images/{$a['post_image']}">
            <figcaption>
              {$a['post_name']}
            </figcaption>
          </figure>
        </div>
      </a>
HTML;
        $count++;
    }


    $show .= <<<HTML
    </div>
    <div class="clr"></div>
    <div class="blocks"></div>
HTML;

    return $show;
}

/**
 * @return Вывод одной статьи
 */

function showPost($post_id)
{

    include_once ROOT."/ecomment.php";
    $post = getPost($post_id);

    $comm = $post['post_comments'];

    //$comm = (file_exists('.dat')? substr_count(file_get_contents('./postphp.dat'), '"name"') : 0);;

    $show = <<<HTML
    <div class="content_post">
      <div class="post_div blocks">
        <div class="posts_width">
          <div class="p_1">
            {$post['post_name']}
          </div>
          <div class="p_2">
            <div class="p_2_1">
              <div class="p_2_1_1">{$post['info_imya']}</div>
              <div class="p_2_1_2"></div>
              <div class="p_2_1_3">
                <img class="p_2_1_3_i" src="images/sidebar_eye.png" alt>
                <div class="p_2_1_3_t">{$post['post_views']}</div>
              </div>
            </div>
            <div class="p_2_2">
              <img src="images/img.png" alt>
            </div>
            <div class="p_2_3">
              <div class="p_2_3_1">{$post['info_familiya']}</div>
              <div class="p_2_3_2"></div>
              <div class="p_2_3_3">
                <img class="p_2_3_3_i" src="images/sidebar_eye.png" alt>
                <div class="p_2_3_3_t">{$comm}</a></div>
              </div>
            </div>
          </div>
          <div class="p_3">
            <div class="p_3_1">
              <img src="admin/uploads/images/{$post['post_image']}" width="581">
            </div>
            <div class="p_3_2">
              {$post['post_description']}
            </div>
          </div>
          <div class="p_3_3">
            {$post['post_text']}
            <div class="p_2_1_2"></div>
            <div class="p_3_3_share">
              <div class="p_3_3_share_1">
                Поделиться
                <img src="images/share.png">
              </div>
              <div class="p_3_3_share_2">
        <!--    <a href="" class="ripplelink"><img src="images/social/gp.png" alt></a>
                <a href=""><img src="images/social/fb.png" alt></a>
                <a href=""><img src="images/social/vk.png" alt></a>
                <a href=""><img src="images/social/tw.png" alt></a>
                <a href=""><img src="images/social/in.png" alt></a>-->

<script type="text/javascript">(function(w,doc) {
if (!w.__utlWdgt ) {
    w.__utlWdgt = true;
    var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
    s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
    var h=d[g]('body')[0];
    h.appendChild(s);
}})(window,document);
</script>
<div data-share-size="20" data-like-text-enable="false" data-background-alpha="0.0" data-pid="1345560" data-mode="share" data-background-color="#ffffff" data-hover-effect="rotate-cw" data-share-shape="round" data-share-counter-size="12" data-icon-color="#ffffff" data-text-color="#000000" data-buttons-color="#ffffff" data-counter-background-color="#ffffff" data-share-counter-type="disable" data-orientation="horizontal" data-following-enable="true" data-sn-ids="fb.vk.tw.ok.gp." data-selection-enable="true" data-exclude-show-more="false" data-share-style="1" data-counter-background-alpha="1.0" data-top-button="false" data-follow-fb="erzhan.kst" class="uptolike-buttons" ></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="clr"></div>
      <div class="blocks">
<!-- Put this div tag to the place, where the Comments block will be -->
<div id="vk_comments"></div>
<script type="text/javascript">
 VK.Widgets.Comments('vk_comments', {onChange: my_vk_callback});

 function my_vk_callback(num, last_comment, date, sign) {
        $.ajax({
            url: '/ajax.php?action=comments',
            type: 'post',
            data: {count: num, id: {$post_id}},
            success: function(data){
                $(".p_2_3_3_t").text(data);
            }
        });
    $.get( "ajax.php", { action: "comments", count: num } );
    }
</script>
        <!--<div class="wrapper"><script language="JavaScript" src="ecomment.js" type="text/javascript"></script></div>-->
        </div>

      <!--  <div class="blocks" style="    padding: 15px;">

        <div class="wrapper"><script language="JavaScript" src="ecomment.js" type="text/javascript"></script></div>
        </div>
      KAMENT
<div id="kament_comments"></div>
<script type="text/javascript">
	/* * * НАСТРОЙКА * * */
	var kament_subdomain = 'yabloker';

	/* * * НЕ МЕНЯЙТЕ НИЧЕГО НИЖЕ ЭТОЙ СТРОКИ * * */
	(function() {
		var node = document.createElement('script'); node.type = 'text/javascript'; node.async = true;
		node.src = 'http://' + kament_subdomain + '.svkament.ru/js/embed.js';
		(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(node);
	})();
</script>
<noscript>Для отображения комментариев нужно включить Javascript</noscript>
<!-- /KAMENT -->
    </div>
HTML;


    return $show;
}

function getAllPosts($type = 0)
{
    if(isset($type) && !is_null($type))
    {
        switch($type)
        {
            case 1:
                $type = ' AND cc.cat_id = 1 ORDER BY p.post_date DESC ';
                break;
            case 2:
                $type = ' AND cc.cat_id = 2 ORDER BY p.post_date DESC ';
                break;
            case 3:
                $type = ' AND cc.cat_id = 3 ORDER BY p.post_date DESC ';
                break;
            case 4:
                $type = ' AND cc.cat_id = 4 ORDER BY p.post_date DESC ';
                break;
            default:
                $type = 'ORDER BY p.post_date DESC ';
                break;
        }
    }

    global $config;
    include_once ROOT."/engine/classes/Pagination/Pagination.class.php";//

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $users = getPosts($type,$start, $limit);

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    // End pagination


    if(count($users) == 0) return 'Ужас! Постов нет!'; // Если нет строк


    $show = '<div class="content_post">';

    /*
     *


     */
    // Разбор массива пользователей
    foreach($users as $u)
    {
        if($u['status_id']==1) $stat = '<img src="images/status_done.png">';
        if($u['status_id']==2) $stat = '<img src="images/status_time.png">';
        if($u['status_id']==3) $stat = '<img src="images/status_x.png">';
        if($u['status_id']==4) $stat = '<img src="images/status_time.png">';
        $show .= '
<a href="post.php?post='.$u['post_id'].'" class=" ">
            <div class="post_div blocks ripplelink ">
              <div class="posts_width">
                <div class="p_1">
                  '.$u['post_name'].'
                </div>
                <div class="p_2">
                  <div class="p_2_1">
                    <div class="p_2_1_1">'.$u['info_imya'].'</div>
                    <div class="p_2_1_2"></div>
                    <div class="p_2_1_3">
                      <img class="p_2_1_3_i" src="admin/images/sidebar_eye.png" alt>
                      <div class="p_2_1_3_t">'.$u['post_views'].'</div>
                    </div>
                  </div>
                  <div class="p_2_2">
                    <img src="images/img.png" alt>
                  </div>
                  <div class="p_2_3">
                    <div class="p_2_3_1">'.$u['info_familiya'].'</div>
                    <div class="p_2_3_2"></div>
                    <div class="p_2_3_3">
                      <img class="p_2_3_3_i" src="images/sidebar_eye.png" alt>
                      <div class="p_2_3_3_t">551</div>
                    </div>
                  </div>
                </div>
                <div class="p_3">
                  <div class="p_3_1">
                    <img src="admin/uploads/images/'.$u['post_image'].'">
                  </div>
                  <div class="p_3_2">
                    '.$u['post_description'].'
                  </div>
                </div>
              </div>
            </div>
            <div class="p_5"><img src="images/down_icon.png" alt></div>
          </a>
          <div class="clr"></div>
<tr onclick="window.location.href=\'posts.php?action=edit&post_id='.$u['post_id'].'\'">
';
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= '</table>'.$markup.'</div>';

    return $show;




    $content = <<<HTML


          <div class="content_l_moreposts ripplelink">Ещё больще статей!</div>
HTML;

    return $content;
}

function getPostsByViews()
{

    $show ='';

    $all = getAllPostsByView();

    foreach($all as $a)
    {

    $show .= <<<HTML
            <a href="post.php?post={$a['post_id']}">
              <div class="cnt_r_best_3_1 ripplelink">
                <div class="cnt_r_best_3_1_1">
                  {$a['post_name']}
                </div>
                <div class="cnt_r_best_3_1_2">
                  <img src="images/sidebar_eye.png" alt class="cnt_r_best_3_1_2_i">
                  <div class="cnt_r_best_3_1_2_t">{$a['post_views']}</div>
                </div>
              </div>
            </a>
HTML;

    }

    return $show;
}

function getPostsByComments()
{

    $show ='';

    $all = getAllPostsByComments();

    foreach($all as $a)
    {

        $show .= <<<HTML
            <a href="post.php?post={$a['post_id']}">
              <div class="cnt_r_best_3_1 ripplelink">
                <div class="cnt_r_best_3_1_1">
                  {$a['post_name']}
                </div>
                <div class="cnt_r_best_3_1_2">
                  <img src="images/sidebar_eye.png" alt class="cnt_r_best_3_1_2_i">
                  <div class="cnt_r_best_3_1_2_t">{$a['post_comments']}</div>
                </div>
              </div>
            </a>
HTML;

    }

    return $show;
}


function getRightContent()
{
    $byviews = getPostsByViews();
    $bycomments = getPostsByComments();
    $content = <<<HTML
      <div class="content_r">
        <div class="cnt_r_best blocks">
          <div class="cnt_r_best_1">Лучшее за месяц</div>
          <div class="cnt_r_best_2">
            <div class="ripplelink active cnt_r_by_views">По просмотрам
            </div>
            <div class="ripplelink cnt_r_by_comments">По комментариям
            </div>
          </div>
          <!--

          <div class="cnt_r_best_2">
            <div class="active cnt_r_by_views material-design">По просмотрам</div>
            <div class="cnt_r_by_comments material-design">По комментариям</div>
          </div>
        -->
          <div class="cnt_r_best_3 cnt_r_views">
            {$byviews}
          </div>

          <div class="cnt_r_best_3 cnt_r_comments">
            {$bycomments}
          </div>
        </div>

        <div class="cnt_r_top blocks">
          <div class="cnt_r_top_1">Топ авторов Яблокера</div>
          <div class="cnt_r_top_3">
            <a href="">
              <div class="cnt_r_top_3_1 ripplelink">
                <div class="cnt_r_top_3_1_1">
                  <img src="images/img.png" alt>
                </div>
                <div class="cnt_r_top_3_1_2">
                  <div class="cnt_r_top_3_1_2_t">Шелдон Купер</div>
                  <div class="cnt_r_top_3_1_2_i">
                    <img src="images/img.png" alt>
                    <img src="images/img.png" alt>
                    <img src="images/img.png" alt>
                    <img src="images/img.png" alt>
                  </div>
                </div>
                <div class="cnt_r_top_3_1_3"><img src="images/arrow_right.png" alt></div>
              </div>
            </a>

            <a href="">
              <div class="cnt_r_top_3_1 ripplelink">
                <div class="cnt_r_top_3_1_1">
                  <img src="images/img.png" alt>
                </div>
                <div class="cnt_r_top_3_1_2">
                  <div class="cnt_r_top_3_1_2_t">Шелдон Купер</div>
                  <div class="cnt_r_top_3_1_2_i">
                    <img src="images/img.png" alt>
                    <img src="images/img.png" alt>
                    <img src="images/img.png" alt>
                    <img src="images/img.png" alt>
                  </div>
                </div>
                <div class="cnt_r_top_3_1_3"><img src="images/arrow_right.png" alt></div>
              </div>
            </a>
            <a href="">
              <div class="cnt_r_top_3_1 ripplelink">
                <div class="cnt_r_top_3_1_1">
                  <img src="images/img.png" alt>
                </div>
                <div class="cnt_r_top_3_1_2">
                  <div class="cnt_r_top_3_1_2_t">Шелдон Купер</div>
                  <div class="cnt_r_top_3_1_2_i">
                    <img src="images/img.png" alt>
                    <img src="images/img.png" alt>
                    <img src="images/img.png" alt>
                    <img src="images/img.png" alt>
                  </div>
                </div>
                <div class="cnt_r_top_3_1_3"><img src="images/arrow_right.png" alt></div>
              </div>
            </a>
            <a href="">
              <div class="cnt_r_top_3_1 ripplelink">
                <div class="cnt_r_top_3_1_1">
                  <img src="images/img.png" alt>
                </div>
                <div class="cnt_r_top_3_1_2">
                  <div class="cnt_r_top_3_1_2_t">Шелдон Купер</div>
                  <div class="cnt_r_top_3_1_2_i">
                    <img src="images/img.png" alt>
                    <img src="images/img.png" alt>
                    <img src="images/img.png" alt>
                    <img src="images/img.png" alt>
                  </div>
                </div>
                <div class="cnt_r_top_3_1_3"><img src="images/arrow_right.png" alt></div>
              </div>
            </a>
            <a href="">
              <div class="cnt_r_top_3_1 ripplelink">
                <div class="cnt_r_top_3_1_1">
                  <img src="images/img.png" alt>
                </div>
                <div class="cnt_r_top_3_1_2">
                  <div class="cnt_r_top_3_1_2_t">Шелдон Купер</div>
                  <div class="cnt_r_top_3_1_2_i">
                    <img src="images/img.png" alt>
                    <img src="images/img.png" alt>
                    <img src="images/img.png" alt>
                    <img src="images/img.png" alt>
                  </div>
                </div>
                <div class="cnt_r_top_3_1_3"><img src="images/arrow_right.png" alt></div>
              </div>
            </a>


          </div>
        </div>

        <div class="cnt_r_part blocks">
          <div class="cnt_r_part_1">Стать частью проекта</div>
          <div class="cnt_r_part_3">
            <div>
              Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.

            </div>
            <a href="" class="ripplelink">Присоединиться!</a>

          </div>
        </div>
      </div>
HTML;

    return $content;
}


function getLeftContent($cnt)
{
    $content = <<<HTML
      <div class="content_l">

          <!--
          <div class="cool_effect">
            <div class="ripplelink">
              <div class="post_div">
                <div class="posts_width">
                  <div class="p_1">
                    Новый взгляд на продуктивность, или Что не так с нашей работой
                  </div>
                  <div class="p_2">
                    <div class="p_2_1">
                      <div class="p_2_1_1">Чингиз</div>
                      <div class="p_2_1_2"></div>
                      <div class="p_2_1_3">
                        <img class="p_2_1_3_i" src="images/sidebar_eye.png" alt>
                        <div class="p_2_1_3_t">551</div>
                      </div>
                    </div>
                    <div class="p_2_2">
                      <img src="images/img.png" alt>
                    </div>
                    <div class="p_2_3">
                      <div class="p_2_3_1">Чингиз</div>
                      <div class="p_2_3_2"></div>
                      <div class="p_2_3_3">
                        <img class="p_2_3_3_i" src="images/sidebar_eye.png" alt>
                        <div class="p_2_3_3_t">551</div>
                      </div>
                    </div>
                  </div>
                  <div class="p_3">
                    <div class="p_3_1">
                      <img src="images/img.png">
                    </div>
                    <div class="p_3_2">
                      Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.
                    </div>
                  </div>
                  <div class="p_all">
                    Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.
                    Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.
                    Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.
                    Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.
                  </div>
                </div>
              </div>
              <div class="p_5"><img src="images/down_icon.png" alt></div>
            </div>
          </div>
          <div class="clr"></div>-->
          {$cnt}


        <!--
         <div class="cool_effect">
            <div class="post_div">
              <div class="posts_width">
                <div class="p_1">
                  Новый взгляд на продуктивность, или Что не так с нашей работой
                </div>
                <div class="p_2">
                  <div class="p_2_1">
                    <div class="p_2_1_1">Чингиз</div>
                    <div class="p_2_1_2"></div>
                    <div class="p_2_1_3">
                      <img class="p_2_1_3_i" src="images/sidebar_eye.png" alt>
                      <div class="p_2_1_3_t">551</div>
                    </div>
                  </div>
                  <div class="p_2_2">
                    <img src="images/img.png" alt>
                  </div>
                  <div class="p_2_3">
                    <div class="p_2_3_1">Чингиз</div>
                    <div class="p_2_3_2"></div>
                    <div class="p_2_3_3">
                      <img class="p_2_3_3_i" src="images/sidebar_eye.png" alt>
                      <div class="p_2_3_3_t">551</div>
                    </div>
                  </div>
                </div>
                <div class="p_3">
                  <div class="p_3_1">
                    <img src="images/img.png">
                  </div>
                  <div class="p_3_2">
                    Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.
                  </div>
                </div>
                <div class="p_all">
                  Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.
                  Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.
                  Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.
                  Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.
                </div>
              </div>
            </div>
            <div class="p_5"><img src="images/down_icon.png" alt></div>
            <paper-ripple fit></paper-ripple>
          </div>
          <div class="clr"></div>
        -->

          <!--
          <div class="google-expando--wrap mat-example big">
            <div class="google-expando">

              <div class="google-expando__icon big">
                <div class="posts_width">
                  <div class="p_1">
                    Новый взгляд на продуктивность, или Что не так с нашей работой
                  </div>
                  <div class="p_2">
                    <div class="p_2_1">
                      <div class="p_2_1_1">Чингиз</div>
                      <div class="p_2_1_2"></div>
                      <div class="p_2_1_3">
                        <img class="p_2_1_3_i" src="images/sidebar_eye.png" alt>
                        <div class="p_2_1_3_t">551</div>
                      </div>
                    </div>
                    <div class="p_2_2">
                      <img src="images/img.png" alt>
                    </div>
                    <div class="p_2_3">
                      <div class="p_2_3_1">Чингиз</div>
                      <div class="p_2_3_2"></div>
                      <div class="p_2_3_3">
                        <img class="p_2_3_3_i" src="images/sidebar_eye.png" alt>
                        <div class="p_2_3_3_t">551</div>
                      </div>
                    </div>
                  </div>
                  <div class="p_3">
                    <div class="p_3_1">
                      <img src="images/img.png">
                    </div>
                    <div class="p_3_2">
                      Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.
                    </div>
                  </div>
                </div>

              </div>
              <div class="google-expando__card" aria-hidden="true">
                <div class="p_4">

                  1.1 Мы предпочитаем сериалы и реалити-шоу научным передачам и лекциям. Почему? Ответ очень банален: они интереснее. Но науку тоже можно преподнести интересно и захватывающе. И люди, о которых я расскажу ниже, делают это лучше всех остальных.
                </div>
              </div>
              <div class="p_5"><img src="images/down_icon.png" alt></div>
            </div>
          </div>
          -->

      </div>
HTML;

    return $content;
}

function masterRender($title, $content, $activeMenuItem = null,$js = null) // Вставляет контент в общую страницу — всякий заголовки, менюшки и т.д.
{
    //if($_SESSION['user']['adminAccess'] == 1){

        global $config;
        /* Массив вывода сообщений и ошибок пользователюя */
        $messages = getMessages();

        $menu = renderMenu($activeMenuItem);

        $html = <<<HTML
    <!DOCTYPE html>
    <html>
    <head>
        <title>{$title}</title>
        <meta charset="utf-8">
        <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
        <script src="//vk.com/js/api/openapi.js" type="text/javascript" charset="windows-1251"></script>
        <script type="text/javascript">
          VK.init({apiId: 4809340, onlyWidgets: true});
        </script>
        <link rel="stylesheet" href="ecomment.css" type="text/css" />
      <link rel="stylesheet" type="text/css" href="css/posts.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
      <link rel="stylesheet" type="text/css" href="css/animations.css">
      <link rel="stylesheet" type="text/css" href="css/mobile.css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
      <!--<link rel="import" href="http://www.polymer-project.org/components/paper-ripple/paper-ripple.html">-->
      <!-- jQuery -->
      <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
      <script src="//code.jquery.com/jquery-1.10.2.js"></script>
      <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
      <!--      BOOTSTRAP
      <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
      <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>-->

        {$js}

    </head>
    <body>
    <!-- Yandex.Metrika counter -->
<script type="text/javascript">

    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter28484716 = new Ya.Metrika({
                    id:28484716,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/28484716" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
    {$messages}
    <!--        HEADER    -->
    <div class="header_1" id="top">
      <div class="header_1_1 ripplelink blocks">
        <img src="images/WebLogo.png" alt><a href="{$config['home']}" class="">Яблокер</a>
      </div>
    </div>
    <div class="header_2 ">
      <div class="header_2_mobile_menu ripplelink">
        <img src="images/header_2_mobile_menu.png" alt>
      </div>
      {$menu}
    </div>
    <!--     END   HEADER    -->
    <!--        CONTENT    -->

    <div class="content">
        {$content}
    </div>

    <div class="footer_1">
      <section class="footer_button"><img class="rotate180 ripplelink" src="images/down_icon.png" alt></section>
      <div>
        <header>
          <ul class="footer_1_1">
            <li>О сайте</li>
            <li>Стать частью проекта</li>
            <li>Яблокер в социальных сетях</li>
          </ul>
          <ul class="footer_1_2">
            <li><a href="" class="ripplelink"><img src="images/social/gp.png" alt></a></li>
            <li><a href=""><img src="images/social/fb.png" alt></a></li>
            <li><a href=""><img src="images/social/vk.png" alt></a></li>
            <li><a href=""><img src="images/social/tw.png" alt></a></li>
            <li><a href=""><img src="images/social/in.png" alt></a></li>
          </ul>
        </header>
        <footer>
          © 2015 yabloker.org. Все права защищены
        </footer>
      </div>
    </div>

    <script type="text/javascript" src="js/main.js"></script>
    <!-- KAMENT -->
<script type="text/javascript">
	/* * * НАСТРОЙКА * * */
	var kament_subdomain = 'yabloker';

	/* * * НЕ МЕНЯЙТЕ НИЧЕГО НИЖЕ ЭТОЙ СТРОКИ * * */
	(function () {
		var node = document.createElement('script'); node.type = 'text/javascript'; node.async = true;
		node.src = 'http://' + kament_subdomain + '.svkament.ru/js/counter.js';
		(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(node);
	}());
</script>
<noscript>Для отображения комментариев нужно включить Javascript</noscript>
<!-- /KAMENT -->
    </body>
    </html>
HTML;
        return $html;

}


/**
 * Посты
 */
/**
 * @param $user_id
 * @return all posts of user
 */
function showAllPostsOfUser($user_id)
{
    global $config;
    include_once ROOT."/engine/classes/Pagination/Pagination.class.php";

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $users = getPostsOfUser($start, $limit, $user_id);

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    // End pagination

    $title = '<h2>Посты пользователя:</h2>';

    if(count($users) == 0) return $title.'Пока постов нет'; // Если нет строк


    $show = '<table class="blocks">
              <tr>
                <th>Статус</th>
                <th>Заголовок</th>
                <th>Дата</th>
                <th>Категория</th>
                <th>Просмотры</th>
              </tr>';
    // Разбор массива пользователей
    foreach($users as $u)
    {
        if($u['status_id']==1) $stat = '<img src="images/status_done.png">';
        if($u['status_id']==2) $stat = '<img src="images/status_time.png">';
        if($u['status_id']==3) $stat = '<img src="images/status_x.png">';
        if($u['status_id']==4) $stat = '<img src="images/status_time.png">';

        $show .= "
<tr onclick=\"window.location.href='posts.php?action=edit&post_id={$u['post_id']}'\">
<td>{$stat}</td><td>{$u['post_name']}</td><td>{$u['post_date']}</td><td>{$u['cat_name']}</td>
<td>{$u['post_views']}</td>
";
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= "</table>".$markup;
    return $show;
}

function showAllActivityOfUser($user_id)
{
    global $config;
    include_once ROOT."/classes/Pagination/Pagination.class.php";//ROOT.

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $users = getActivityOfUser($start, $limit, $user_id);

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    // End pagination

    $title = '<h2>Действия пользователя:</h2>';

    if(count($users) == 0) return $title.'Пока действий нет'; // Если нет строк


    $show = '<table class="blocks">
              <tr>
                <th>Время</th>
                <th>Категория</th>
              </tr>';
    // Разбор массива пользователей
    foreach($users as $u)
    {
        $show .= "
<tr>
<td>{$u['ac_date']}</td><td>{$u['uat_name']}</td>";
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= "</table>".$markup;
    return $show;
}


/**
 * Show Add Posts
 */

function showAddPostsForm()
{

    $groups = getGroupsPosts();
    $groupsSelect = '<select name="post_category" class="form-control input-sm"><option>Выберите категорию</option>';

    $i = 0;
    foreach($groups as $g) {
        $disable = '';//($g['id'] == 1 || ($_SESSION['user']['addModer'] == 0 && $g['id'] == 2)) ? ' disabled' : '';
        $groupsSelect .= '<option value="'.$g['cat_id'].'"'.$disable.'>'.$g['cat_name'].'</option>';
    }
    $groupsSelect .= '</select>';

    return <<<HTML
    <h2>Добавление поста</h2>
        <form action="posts.php?action=doAdd" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Название</label>
                                <input type="post_title" name="post_name" value="" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Категория</label>
                                {$groupsSelect}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Загрузить фото</label>
                                <input type="file" name="image" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Описание</label>
                                <textarea  name="post_description" class="form-control input-sm"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Текст</label>
                                <textarea name="post_text" id="editor1" cols="45" rows="5"></textarea>
                                <script type="text/javascript">
                                CKEDITOR.replace( 'editor1');
                                </script>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>SEO Title</label>
                                <input type="text" name="post_seo_title" value="" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>SEO Description</label>
                                <input type="text" name="post_seo_description" value="" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <input type="hidden" name="sub" value="1">
                                <input type="submit" name="sub" value="Добавить" class="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

            </div>
        </form>
HTML;
}


/**
 * Show Edit Posts
 */

function showEditPostsForm($post_id)
{

    $post = getPost($post_id);
    $groups = getGroupsPosts();
    $groupsSelect = '<select name="post_category" class="form-control input-sm"><option>Выберите категорию</option>';

    foreach($groups as $g) {
        $activ = '';
        $disable = '';//($g['id'] == 1 || ($_SESSION['user']['addModer'] == 0 && $g['id'] == 2)) ? ' disabled' : '';
        if($post['cat_id']==$g['cat_id']) $activ = "selected";
        $groupsSelect .= '<option value="'.$g['cat_id'].'"'.$activ.'>'.$g['cat_name'].'</option>';
    }
    $groupsSelect .= '</select>';

    return <<<HTML
    <h2>Редактирование поста</h2>
        <form action="posts.php?action=doEdit" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Название</label>
                                <input type="post_title" name="post_name" value="{$post['post_name']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Категория</label>
                                {$groupsSelect}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                            <img src="uploads/images/{$post['post_image']}" style="width: 325px;" alt>
                                <label>Изменить фото</label>
                                <input type="checkbox" name="image_edit">
                                <input type="file" name="image" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Описание</label>
                                <textarea  name="post_description" class="form-control input-sm">{$post['post_description']}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Текст</label>
                                <textarea name="post_text" id="editor1" cols="45" rows="5">{$post['post_text']}</textarea>
                                <script type="text/javascript">
                                CKEDITOR.replace( 'editor1');
                                </script>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>SEO Title</label>
                                <input type="text" name="post_seo_title" value="{$post['post_seo_title']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>SEO Description</label>
                                <input type="text" name="post_seo_description" value="{$post['post_seo_title']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <input type="hidden" name="post_id" value="{$post_id}">
                                <input type="submit" name="sub" value="Изменить" class="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

            </div>
        </form>
HTML;
}

?>