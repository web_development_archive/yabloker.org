<?php

/*
 * Вывод Страницы сотрудников
 */
function showEmployees($companyId)
{
    $content = '';
    $company = getCompany($companyId);
    $titleCom = $company['title'];

    $sale = companyType($company['sale'], "sale");
    $buy = companyType($company['buy'], "buy");
    $logic = companyType($company['logic'], "logic");

    //$titleImg = getImageThumb($company['logo'], 225, 130);
    $stuffAll = getAllStaff($companyId);

    $stuff = '';
    if($stuffAll==null)
    {
        $stuff .= '<tr>
              <td colspan="5">Сотрудников нет</td>
            </tr>';
    }else{
        $counter = 1;
        foreach($stuffAll as $stAll)
        {
            switch($stAll['type'])
            {
                case 0:
                    $type = 'Владелец';
                    break;
                case 1:
                    $type = 'Сотрудник';
                    break;
                default:
                    $type = '';
            }
            switch($stAll['status'])
            {
                case 0:
                    $imgStat = '/img/workersNoneBlock.png';
                    $textStat = 'Разблокирован';
                    break;
                case 1:
                    $imgStat = '/img/workersBlock.png';
                    $textStat = 'Заблокирован';
                    break;
                default:
                    $imgStat = '';
                    $textStat = '';
            }

            $stuff .= <<<HTML
            <tr>
              <td class="workersTableN">{$counter}</td>
              <td class="workersTableFIO">{$stAll['fio']}</td>
              <td class="workersTableUsers">{$type}</td>
              <td class="workersTableDate">{$stAll['dt']}</td>
              <td class="workersTableDateStatus"><img src="{$imgStat}" class="workersTableImgBlock"><section class="workersTableNBText">{$textStat}</section></td>
            </tr>
HTML;
            $counter++;
        }
    }

    $content .= <<<HTML

    <section id="modal-background"></section>
    <section id="modal-content">
      <img id="modal-close" class="wmbtnClose" src="/img/xWorkers.png">
      <form action="/cabinet.php?action=addWorker" method="post" enctype="multipart/form-data">
          <section class="wmCenter">
            <section class="wmTitle">Добавление сотрудника</section>
            <section class="wmLeftSec">
              <section class="wmLabel">Email</section>
              <input type="text" name="mail" id="wmEmail">
              <section class="wmLabel">Пароль</section>
              <input type="password" name="pass" id="wmEmail">
              <section class="wmLabel">Город</section>
              <input type="text" name="city" id="wmEmail">
              <section class="wmLabel">Должность</section>
              <select name="group_id" class="wmSlt">
                <option value="1">Администратор</option>
                <option value="2" selected>Пользователь</option>
              </select>
              <section class="wmLabel">Фото сотрудника</section>
              <input type="file" name="image" />
            </section>
            <section class="wmRightSec">
              <section class="wmLabel">Имя</section>
              <input type="text" name="name" id="wmInputRight">
              <section class="wmLabel">Фамилия</section>
              <input type="text" name="surname" id="wmInputRight">
              <section class="wmLabel">Отчество</section>
              <input type="text" id="wmInputRight" name="patro">
              <section>
                <label>
                    <input type="radio" name="sex" value="1" id="wmr2">
                    <section class="wmrl">мужской пол</section>
                </label>
                <label>
                    <input type="radio" name="sex" value="0" id="wmr">
                    <section class="wmrl">женский пол</section>
                </label>
              </section>
              <section class="wmLabel">Дата рождения</section>
              <input type="text" class="datepicker" name="birthday" id="wmDate">
            </section>
          </section>
          <section class="wmfooter">
            <input type="submit" class="wmBtnAdd btn btn-info" name="sub" value="Добавить сотрудника">
          </section>
      </form>
    </section>

    <section class="rightContainer inlineTable">
        <section class="reviewRightTitle">
          {$titleCom}
        </section>
        <section class="workersRightLine">
                {$sale}
                {$buy}
                {$logic}
        </section>
        <section class="workersRightLine2">
          <section class="workersRightLine2Text">Сотрудники</section>
          <section id="modal-launcher" class="workersRightLine2Btn btn btn-info">Добавить сотрудника</section>
        </section>
        <section class="workersTable">
          <table>
            <tr>
              <th>№</th>
              <th>Фамилия, имя, отчество</th>
              <th>Должность</th>
              <th>Был на сайте</th>
              <th>Статус</th>
            </tr>
            {$stuff}
          </table>
        </section>
      </section>
HTML;


    return $content;
}

/*
 * Вывод Товаров
 */

function showGoodsCom($idCompany)
{
    $company = getCompany($idCompany);
    $titleCom = $company['title'];

    $sale = companyType($company['sale'], "sale");
    $buy = companyType($company['buy'], "buy");
    $logic = companyType($company['logic'], "logic");

    $goods = getGoodsSite(null, null, $company['id']);
    $tovar = sortGoodsByCats($goods);

    $iArray = getCompanyInterests($company['id']); // Интересы компании
    $interests = '';

    if(count($iArray) > 0) foreach($iArray as $inter) $interests .= '<div class="dropdown" inter_id="'.$inter['id'].'" style="display: inline-table;"><section class="itemsInteresList" data-toggle="dropdown" id="inter'.$inter['id'].'">'.$inter['title'].' <b class="caret"></b></section><ul class="dropdown-menu" role="menu" aria-labelledby="inter'.$inter['id'].'"><li><a href="/interest/'.$inter['id'].'">Другие компании</a></li><li><a href="#" onclick="deleteInterest('.$inter['id'].'); return false;">Удалить интерес</a></li></ul></div>';
    else $interests = 'Нет интересов';

    $cats = '<option value="0">Выберите категорию</option>';
    $cArray = getCats();
    if(count($cArray) > 0){
        foreach($cArray as $cat){
            $cats .= '<option value="'.$cat['id'].'">'.$cat['name'].'</option>';
        }
    }

    $specifics = '';
    $temp = array();
    if(isset($_SESSION['addGood'])){
        $temp = $_SESSION['addGood'];
        if(isset($temp['spc']) && isset($temp['spcName'])){
            $count = count($temp['spc']);
            for($k = 0;$k<$count;$k++) $specifics .= '<section class="wmLabel">'.$temp['spcName'][$k].'</section><input type="hidden" name="spcName[]" value="'.$temp['spcName'][$k].'"><input type="text" name="spc[]" value="'.$temp['spc'][$k].'" id="wmItemsInputRight1">';
        }
        unset($_SESSION['addGood']);
    }

    $content = <<<HTML
    <section id="modal-background"></section>
    <section id="modal-content">
      <img id="modal-close" class="wmbtnClose" src="/img/xWorkers.png">
      <form action="/cabinet.php?action=addGood" method="post" enctype="multipart/form-data">
      <section class="wmCenter">
        <section class="wmTitle">Добавление товара</section>
        <section class="wmLeftSec">
          <section class="wmLabel">Название</section>
          <input type="text" name="title" value="{$temp['title']}" id="wmEmail">
          <section class="wmLabel">Фотографии товара</section>
          <input type="file" id="imagesUploader" name="images[]"  multiple/>
          <section class="wmLabel">Описание товара</section>
          <textarea class="itemsTextArea" name="description" value="{$temp['description']}" style="height:250px;width:270px"></textarea>
        </section>
        <section class="wmItemsRightSec">
          <section class="wmLabel">Категория</section>
          <select class="wmItemsSlt" name="cat_id">{$cats}</select>
          <section class="clrItems" style="height:20px"></section>
          <div id="desktop-chars">
              <section class="wmLabel">Цена</section>
              <input type="text" name="price" value="{$temp['price']}" id="wmItemsInputRight1">
              {$specifics}
          </div>
          <section class="itemsHar">
            <img src="/img/plusItems.png" class="itemsImgRight">
            <section class="itemsTextRight" onclick="addCharasterictics();">Добавить характеристику</section>
          </section>
        </section>

      </section>
      <section class="wmfooter">
        <input type="submit" class="wmBtnAdd btn btn-info" name="sub" value="Добавить товар">
      </section>
      </form>
        <!--<button id="modal-close">Close Modal Window</button>-->
    </section>
        <section class="rightContainer inlineTable">
        <section class="reviewRightTitle">
          {$titleCom}
        </section>
        <section class="workersRightLine">
                {$sale}
                {$buy}
                {$logic}
        </section>
        <section class="workersRightLine2">
          <section class="workersRightLine2Text">Интересует</section>
        </section>
        <section class="itemsInteresSec" id="myInterests">
          {$interests}
          <section class="itemsInteresListAdd" onclick="addInterest()">
            Добавить интерес
          </section>
        </section>
        <section class="workersRightLine2Text">Товары</section>
        <section id="modal-launcher" class="itemsRightLine2Btn btn btn-info">Добавить товар</section>
        {$tovar}
      </section>
HTML;

    return $content;
}

function showProfile($company_id)
{
    $company = getCompany($company_id);
    //$logo = getImageThumb($company['logo'], 225, 130);
    $comps_users = qCount(SQL_GET_COMP_USERS, array('id' => $company_id));

    $title = $company['title'];

    $dateOfReg = $company['reg_date'];
    $nullDeal = getNullDials($company_id);

    $counOfNullDeal = $nullDeal[0];
    if($counOfNullDeal==null)
        $counOfNullDeal=0;

    $goodDials = getGoodDials($company_id);
    $counOfGoodDial = $goodDials[0];

    $count_of_rates = $company['count_of_rate'];
    $rate = $company['rate'];
    $views = $company['views'];
    $toDate = date("Y-m-d");

    /*
    $allDeals = '';
    for($i = 0; $i<$counOfGoodDial;$i++)
    {
        $date_make2 = strtotime($goodDials['make_dt']);

        $date_make = date("m/d/Y",$date_make2);
        if($goodDials['first'] == $company_id)
        {
            $secondCompany = getCompany($goodDials['second']);
            $name = $secondCompany['title'];
        }elseif($goodDials['second'] == $company_id)
        {
            $secondCompany = getCompany($goodDials['first']);
            $name = $secondCompany['title'];
        }
        $comment = $goodDials['comment'];
        $allDeals .= <<<HTML
    <tr class="nonActiveRow allDateRow">
      <td class="descFirstRow">{$date_make}</td>
      <td>{$name}</td>
      <td>{$comment}</td>
    </tr>
HTML;
    }
    */

    $sale = companyType($company['sale'], "sale");
    $buy = companyType($company['buy'], "buy");
    $logic = companyType($company['logic'], "logic");

    return <<<HTML
    <script>
    $(function() {
        $( "#datepicker" ).datepicker({
          dateFormat: 'yy-mm-dd',
          showOn: "button",
          buttonImage: "/img/descIcons1.png",
          buttonImageOnly: true
        });
        refreshDeals();
  });
    </script>
      <section class="rightContainer inlineTable">
        <section class="reviewRightTitle">
          {$title}
        </section>
        <section class="workersRightLine">
            {$sale}
            {$buy}
            {$logic}
        </section>
        <section class="descRightLine2">
          <section class="descRightLine2Text">Календарь</section>
        </section>
        <section id='calendar'></section>
        <section class="descRLeft">
          <section class="descRightLine2">
            <section class="descRightLine2Text">Сделки на
            <section class="descRightLine3Text"> {$toDate}</section>
            <input onchange="getDateFunc()" style="display:none" type="text" id="datepicker">
            <!--<img class="descIcons" src="img/descIcons1.png"></section>-->
          </section>
          <section class="descTable">
            <table id="myDeals">
              <tr><th>Дата</th><th>Название</th><th>Комментарий</th></tr>
              <tr class="noDeals allDateRow"><td colspan="3">Сделок нет</td></tr>
            </table>
          </section>
          <section>
            <section class="descRightLine2">
              <section class="descRightLine2Text">Статистика компании</section>
            </section>
            <br>
            <section class="descRightStat">
              <img src="/img/descIcon1.png" class="descIcons">
              <section class="descRightStatText">Дата регистрации</section>
              <section class="descRightStatDate">{$dateOfReg}</section>
            </section>
            <section class="descRightStat">
              <img src="/img/descIcon2.png" class="descIcons">
              <section class="descRightStatText">Сделки</section>
              <section class="descRightStatDate"><section class="descNumMel1">{$counOfGoodDial}</section><section class="descNumMel2">{$counOfNullDeal}</section></section>
            </section>
            <section class="descRightStat">
              <img src="/img/descIcon3.png" class="descIcons">
              <section class="descRightStatText">Посещение страницы</section>
              <section class="descRightStatDate">{$views}</section>
            </section>
            <section class="descRightStat">
              <img src="/img/descIcon3.png" class="descIcons">
              <section class="descRightStatText">Количество сотрудников</section>
              <section class="descRightStatDate">{$comps_users}</section>
            </section>
            <section class="descRightStat">
              <img src="/img/descIcon4.png" class="descIcons">
              <section class="descRightStatText">Оценки пользователей</section>
              <section class="descNumMel1">{$rate}</section><section class="descRightStatDate">из {$count_of_rates} оценок</section>
            </section>
          </section>
        </section>
        <section class="descRRight">

        </section>
      </section>
      <script>
  function getDateFunc()
  {
        var myDate = $("#datepicker").val();
        $(".descRightLine3Text").html(myDate);
        $(".allDateRow").addClass("nonActiveRow");
        refreshDeals(myDate);
  }

    function refreshDeals(myDate){
        var table = $('#myDeals');

        $.ajax({
            url: '/ajax.php?action=getDealsByDate',
            type: 'post',
            data: {date: myDate},
            success: function(data){
                var obj = eval('('+data+')');
                var count = obj.deals.length;


                if(obj.status == 1){
                    if(count > 0){
                        var show = '<tr><th>Дата</th><th>Название</th><th>Комментарий</th></tr>';
                        for(var k = 0;k<count;k++){
                            var name = (obj.deals[k].firstName == '{$title}') ? obj.deals[k].secondName : obj.deals[k].firstName;
                            show += '<tr><td>'+obj.deals[k].shipment_dt+'</td><td>'+name+'</td><td>'+obj.deals[k].comment+'</td>';
                        }
                        table.html(show);
                    }else{
                        table.html('<tr class="noDeals allDateRow"><td colspan="3">Сделок нет</td></tr>');
                    }
                }


            }
        });
    }

  function showDeals(whatDate)
  {
    if($("tr:contains("+whatDate+")").removeClass("nonActiveRow"))
    {
        return true;
    }
    if($("tr").text() !=whatDate)
    {
        return false;
    }
   return false;
  }

  </script>
  </section>
HTML;
}

/*
 * Вывод левой части
 */
function showLeftCont($active,$company_id)
{
    $company = getCompany($company_id);
    $titleImg = getImageThumb($company['logo'], 225, 130);
    $firstClass= $secondClass= $thirdClass =$fourthClass =$fifthClass =$sixthClass =$seventhClass ='leftMenuWorkers';
    $firstImg = '/img/iconsWorkers1.png';
    $secondImg = '/img/iconsWorkers2.png';
    $thirdImg = '/img/iconsWorkers3.png';
    $fourthImg = '/img/iconsWorkers4.png';
    $fifthImg = '/img/iconsWorkers5.png';
    $sixthImg = '/img/iconsWorkers6.png';
    $seventhImg = '/img/iconsWorkers7.png';
    $content ='';
    switch($active)
    {
        case 1:
            $firstClass = 'leftMenuWorkersActiv';
            $firstImg = '/img/iconsWorkers1Activ.png';
            break;
        case 2:
            $thirdClass = 'leftMenuWorkersActiv';
            $thirdImg = '/img/iconsWorkers2Activ.png';
            break;
        case 3:
            $thirdClass = 'leftMenuWorkersActiv';
            $thirdImg = '/img/iconsWorkers3Activ.png';
            break;
        case 4:
            $fourthClass = 'leftMenuWorkersActiv';
            $fourthImg = '/img/iconsWorkers4Activ.png';
            break;
        case 5:
            $fifthClass = 'leftMenuWorkersActiv';
            $fifthImg = '/img/iconsWorkers5Activ.png';
            break;
        case 6:
            $sixthClass = 'leftMenuWorkersActiv';
            $sixthImg = '/img/iconsWorkers6Activ.png';
            break;
        case 7:
            $seventhClass = 'leftMenuWorkersActiv';
            $seventhImg = '/img/iconsWorkers7Activ.png';
            break;
        default:

    }
    $content = <<<HTML
    <section class="leftContainer inlineTable">
        <section style="margin:0 20px 0 20px">
          <img alt="img" src="{$titleImg}" class="previewTitle">
          <section>
            <section class="{$firstClass}">
                <a href ="/cabinet/">
                  <img class="leftImgOfMenuWorkers" src="{$firstImg}">
                  <section class="leftTextOfMenuWorkers">Рабочий стол</section>
                </a>
            </section>
            <section class="{$secondClass}">
                <a href ="/cabinet/deals">
                  <img class="leftImgOfMenuWorkers" src="{$secondImg}">
                  <section class="leftTextOfMenuWorkers">Сделки</section>
                </a>
            </section>
            <section class="{$thirdClass}">
                <a href ="/cabinet/goods/">
                  <img class="leftImgOfMenuWorkers" src="{$thirdImg}">
                  <section class="leftTextOfMenuWorkers">Товары</section>
                </a>
            </section>
            <section class="{$fourthClass}">
                <a href ="/cabinet/employees">
                  <img class="leftImgOfMenuWorkers" src="{$fourthImg}">
                  <section class="leftTextOfMenuWorkers">Сотрудники</section>
                </a>
            </section>
            <section class="{$fifthClass}">
                <a href ="/cabinet/stat">
                  <img class="leftImgOfMenuWorkers" src="{$fifthImg}">
                  <section class="leftTextOfMenuWorkers">Статистика</section>
                </a>
            </section>
            <section class="{$sixthClass}">
                <a href ="/cabinet/page">
                  <img class="leftImgOfMenuWorkers" src="{$sixthImg}">
                  <section class="leftTextOfMenuWorkers">Страница</section>
                </a>
            </section>
            <section class="{$seventhClass}">
                <a href ="/cabinet/settings">
                  <img class="leftImgOfMenuWorkers" src="{$seventhImg}">
                  <section class="leftTextOfMenuWorkers">Настройки</section>
                </a>
            </section>
          </section>
        </section>
      </section>
HTML;
    return $content;
}