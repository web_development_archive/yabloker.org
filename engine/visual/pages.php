<?php
/**
 * Содержит функций вывода страниц: главная страница, контакты, усулги и.т.д
 */


/**
 * @param $term
 * @param $type
 * @return string
 */

function showSearchResults($term, $type)
{
    if(empty($term)) return '<div class="search">Введите поисковую фразу</div>';
    $results = getSearchResults($term, $type);

    $show = '<div class="search">';
    if(count($results) == 0) return '<div class="search">Ничего не найдено</div>';

    if($type == 0) $show .= showGoodsShort($results);
    elseif ($type == 1) $show .= showCompaniesShortForm($results);

    return $show.'</div>';
}

function showFeedbackForm()
{
    return <<<HTML
    <form action="" method="post">
        <div class="row-fluid">
            <div class="span4">
                <div>
                    <label>Тема</label>
                    <input type="text" name="subject" value="" />
                </div>
                <div>
                    <label>Ваше имя</label>
                    <input type="text" name="name" value="" />
                </div>
                <div>
                    <label>Ваше email</label>
                    <input type="text" name="mail" value="" />
                </div>
            </div>
            <div class="span8">
                <div>
                    <label>Сообщение</label>
                    <textarea name="txt" rows="7" style="width:400px"></textarea>
                </div>
            </div>
        </div>
        <div><input type="submit" class="btn btn-default" value="Отправить" name="subFeed" /></div>
    </form>
HTML;
}

function showTextInMainPage(){
    return <<<HTML
        <section class="mainSlide">
        <div id="mainCar" class="carousel slide">
          <ol class="carousel-indicators">
            <li data-target="#mainCar" data-slide-to="0" class="active"></li>
            <li data-target="#mainCar" data-slide-to="1"></li>
            <li data-target="#mainCar" data-slide-to="2"></li>
          </ol>
          <!-- Carousel items -->
          <div class="carousel-inner">
            <div class="active item"><img src="/img/groupMainPage.png" /></div>
            <div class="item" style="background: url(/img/imgSlider.png);height:350px">
                  <section class="centerAll">
                    <img src="/img/bottle.png" class="mainSlideImg">
                  </section>
                  <section class="textMainSlide">Не можете найти покупателей вашего самого лучшего молока?</section>
                  <section class="centerAll" style="padding-bottom:20px">
                    <section class="btn btn-info btnMany">У нас их много!</section>
                  </section>
            </div>
            <div class="item" style="background: url(/img/meetSlice.jpg); width:735px;height:350px;">
                  <section class="centerAll" style="padding-top:220px">
                    <a href="/login.php?action=showSignUp" class="btn btn-info btnMany">Я покупаю</a>
                    <a href="/login.php?action=showSignUp" class="btn btn-info btnMany" style="margin-left:10px">Я продаю</a>
                  </section>
            </div>
          </div>
        </div>
        </section>
        <section class="inlineTable threeText">
          <header>
            <img src="/img/three1.png" alt="img" class="threeTextImg">
            <section class="threeTextHeader">
              <strong>Более 3000</strong> торговых сетей
            </section>
          </header>
          <footer>
            Разнообразный и богатый опыт консультация с широким активом требуют определения и уточнения форм развития.
          </footer>
        </section>
        <section class="inlineTable threeText">
          <header>
            <img src="/img/three2.png" alt="img" class="threeTextImg" />
            <section class="threeTextHeader">
              <strong>Более 3000</strong> торговых сетей
            </section>
          </header>
          <footer>
            Разнообразный и богатый опыт консультация с широким активом требуют определения и уточнения форм развития.
          </footer>
        </section>
        <section class="inlineTable threeText">
          <header>
            <img src="/img/three3.png" alt="img" class="threeTextImg" />
            <section class="threeTextHeader" >
              <strong>Более 3000</strong> торговых сетей
            </section>
          </header>
          <footer>
            Разнообразный и богатый опыт консультация с широким активом требуют определения и уточнения форм развития.
          </footer>
        </section>
        <section class="rightText">
          Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности требуют от нас анализа модели развития.
        </section>
        <section class="leftSection">
          <img src="img/leftImg.png" alt="img" />
          <a href="/login.php?action=showSignUp" class="btn btn-info btnReg">Зарегистрироваться!</a>
        </section>
        <section class="rightSection">
          Не следует, однако забывать, что реализация намеченных плановых заданий способствует подготовки и реализации существенных финансовых и административных условий. Не следует, однако забывать, что начало повседневной работы по формированию позиции обеспечивает широкому кругу (специалистов) участие в формировании новых предложений. Равным образом новая модель организационной деятельности способствует подготовки и реализации дальнейших направлений развития.
        </section>
HTML;

}

?>