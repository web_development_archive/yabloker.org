<?php

session_start();
header('Content-type: text/html; charset=utf-8');
date_default_timezone_set('Asia/Almaty');
define("ROOT", $_SERVER['DOCUMENT_ROOT']);
define("DS", DIRECTORY_SEPARATOR);

ini_set('error_reporting', E_ALL ^ E_NOTICE);
ini_set('display_errors', 1);

$config = array(
    'home' => 'http://yabloker.org/',
    'images' => '/uploads/images/',
    'thumbs' => 'uploads/images/thumbs/',
    'imagesUpload' => 'uploads/images/',//ROOT.
    'thumbsDir' => 'uploads/images/thumbs/',//ROOT.
    'thumbsDir2' => 'uploads/images/thumbs',//ROOT.
    'rowPerPage' => 10,
    'defaultAvatarMini' => 'http://firepic.org/images/2014-07/24/nl3phjadnfm2.png'
);

include_once "sql.php";


include_once "core/functions.php";
include_once "core/posts.php";
/*include_once "core/users.php";
include_once "core/activity.php";
/*include_once "core/images.php";
include_once "core/deals.php";
include_once "core/goods.php";
include_once "core/companies.php";
include_once "core/services.php";
include_once "core/adverts.php";
include_once "core/texts.php";
include_once "core/regions.php";
*/
?>