<?php
/**
 * Created by PhpStorm.
 * User: erzhan_kst
 * Date: 29.01.2015
 * Time: 10:38
 */

include "engine/core.php";
include "engine/visual.php";

$action = $_GET['action'];

switch($action){
  case "lifehacks":
    $content = getLeftContent(getAllPosts(1));
    $content .= getRightContent();
    echo masterRender('Яблокер',  $content,1);
    break;
  case "technology":
    $content = getLeftContent(getAllPosts(2));
    $content .= getRightContent();
    echo masterRender('Яблокер',  $content,2);
    break;
  case "inspiration":
    $content = getLeftContent(getAllPosts(3));
    $content .= getRightContent();
    echo masterRender('Яблокер',  $content,3);
    break;
  case "doit":
    $content = getLeftContent(getAllPosts(4));
    $content .= getRightContent();
    echo masterRender('Яблокер',  $content,4);
    break;
  default:
    $content = getLeftContent(getAllPosts());
    $content .= getRightContent();
    echo masterRender('Яблокер',  $content,0);
}

?>