<?php
// Мы пишем SQL-запросы только в этом файле
// потому что если нас попросят перевести вс систему с mysql под oracle
// то мы офигеем искать запросы, разбросанные по всему проекту
// а тут они удобно в одном месте

$dbConnection = new PDO('mysql:dbname=yabloker_db;host=localhost;charset=utf8', 'yabloker_db', '6L7BTyXggbcu');

$dbConnection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$dbConnection->exec("set names utf8");

function q($sql, $params) // запрос к базе — короткое имя для удобства
{
    global $dbConnection;
    $stmt = $dbConnection->prepare($sql);
    $stmt->execute($params);
    $result = $stmt->fetchAll();
    return $result;
}

function q2($sql, $params){ // Используется для insert и update
    global $dbConnection;
    $stmt = $dbConnection->prepare($sql);

    if($stmt->execute($params)) return true;
    else return false;

}

function qCount($sql, $params){ // Выводит количество записей
    global $dbConnection;
    $stmt = $dbConnection->prepare($sql);
    $stmt->execute($params);
    return $stmt->fetchColumn();
}

function qRows(){ // Выводит кол-во строк другим способом
    global $dbConnection;
    $stmt = $dbConnection->query('SELECT FOUND_ROWS() as num');
    return $stmt->fetchColumn(0);
}

function qInsertId(){ // Последнйи автоинкриментный ID
    global $dbConnection;
    return $dbConnection->lastInsertId();
}

/**
 * Users
 */

define("SQL_LOGIN_ATTEMPT", "SELECT u.*, t.* FROM users u, user_type t, user_type_user utu WHERE u.user_phone=:phone AND u.user_password=:pass AND utu.id_user = u.user_id AND t.type_id=utu.id_type");

/*
 * Поиск
 */

define("SQL_GET_GOODS_BY_SEARCH", "SELECT g.*, p.filename as image FROM goods g LEFT JOIN photos p ON (p.id=g.image_id) WHERE LOWER(g.title) LIKE LOWER(:term)");
define("SQL_GET_BUYERS_BY_SEARCH", "SELECT companies.*, p.filename as logo FROM companies LEFT JOIN photos p ON(p.id=companies.logotype) WHERE LOWER(companies.title) LIKE LOWER(:term) OR LOWER(companies.description) LIKE LOWER(:term1) AND companies.buy = 1");

/*
 * Regions
 */

define("SQL_GET_REGIONS", "SELECT * FROM regions");
define("INSERT_NEW_COMP_REGION", "INSERT INTO comps_regions(com_id, region_id) VALUES(:comp_id, :reg_id)");

/*
 * Favorites
 */

define("SQL_GET_FAVORITE", "SELECT * FROM favorites WHERE user_id = :user_id AND com_id = :com_id");
define("SQL_DELETE_FAV", "DELETE FROM favorites WHERE user_id = :user_id AND com_id = :com_id");
define("SQL_ADD_TO_FAVORITES","INSERT INTO favorites(user_id, com_id) VALUES(:user_id,:com_id)");
/*
 * Adverts
 */

define("SQL_GET_ADVERTS", "SELECT SQL_CALC_FOUND_ROWS a.*, p.dt_created, p.dt_start, p.dt_finish, p.user_id, u.name, CONCAT('{$config['home']}{$config['thumbs']}', i.filename) as filename FROM adverts a LEFT JOIN payments p ON (a.pay_id=p.id) LEFT JOIN users u ON (u.id=p.user_id) LEFT JOIN photos i ON (a.image=p.id)");
define("SQL_UPDATE_ADVERT_STATUS", "UPDATE adverts SET status = :status WHERE id = :id");

/*
 * Deals
 */

define("SQL_GET_DEALS", "SELECT SQL_CALC_FOUND_ROWS d.*, g.title as good, c1.title as firstName, c.title as secondName FROM deals d LEFT JOIN companies c1 ON (c1.id=d.first) LEFT JOIN companies c ON (c.id=d.second) LEFT JOIN goods g ON (d.good_id=g.id)");
define("SQL_GET_DEAL", "SELECT d.*, g.title as good, u.name as firstName, u2.name as secondName FROM deals d LEFT JOIN users u ON (u.id=d.first) LEFT JOIN users u2 ON (u2.id=d.second) LEFT JOIN goods g ON (d.good_id=g.id) WHERE d.id = :id");
define("SQL_UPDATE_DEAL", "UPDATE deals SET type = :type, first = :first, second = :second, good_id = :good_id, make_dt = :make_dt, shipment_dt = :shipment_dt, status = :status, comment = :comment WHERE id = :deal_id");

/*
 * Services
 */

define("SQL_GET_SERVICES", "SELECT * FROM services");
define("SQL_GET_SERVICE", "SELECT * FROM services WHERE id = :id");
define("SQL_UPDATE_SERVICE", "UPDATE services SET name = :name, price = :price WHERE id = :ser_id");
define("SQL_DELETE_SERVICE", "DELETE FROM services WHERE id = :id");

/*
 * Categories
 */

define("SQL_GET_CATS", "SELECT SQL_CALC_FOUND_ROWS c.*, c1.name as parent, count(g.id) as num FROM cats c LEFT JOIN cats c1 ON (c.parent_id=c1.id) LEFT JOIN goods g ON (g.cat_id=c.id) GROUP BY c.id");
define("SQL_GET_CAT", "SELECT * FROM cats WHERE id = :id");
define("SQL_UPDATE_CAT", "UPDATE cats SET name = :name, parent_id = :parent_id WHERE id = :cat_id");
define("SQL_DELETE_CAT", "DELETE FROM cats WHERE id = :id");
define("SQL_ADD_CAT", "INSERT INTO cats(name, parent_id) VALUES(:name, :parent_id)");

/*
 * Images
 */

define("SQL_GET_IMAGE", "SELECT * FROM photos WHERE id = :id");
define("SQL_DELETE_IMAGE", "DELETE FROM photos WHERE id = :id");
define("SQL_GET_GOOD_IMAGES", "SELECT * FROM photos WHERE good_id = :good_id");

/*
 * Интересы
 */

define("SQL_GET_ALL_COM_BY_INTERES","SELECT c.*, i.title as interest, p.filename as logo FROM companies c JOIN comps_interests ci ON (c.id = ci.company_id AND ci.interest_id = :interest_id) LEFT JOIN interests i ON (ci.interest_id = i.id) LEFT JOIN photos p ON (c.logotype=p.id)");
define("SQL_GET_NAME_OF_INTERES","SELECT i.title FROM interests i WHERE i.id = :id");
define("DELETE_COMPANY_INTEREST", "DELETE FROM comps_interests WHERE company_id = :company_id AND interest_id = :interest_id");

/*
 * Attachments
 */

define("SQL_GET_ATTACHMENTS", "SELECT * FROM `attachments` WHERE `table` = ':table' AND `target_id` = ':target'");

/*
 * Текстовые блоки
 */

define("SQL_GET_TEXTS", "SELECT * FROM texts");
define("SQL_GET_TEXT", "SELECT * FROM texts WHERE id = :id");
define("UPDATE_TEXT_BLOCK", "UPDATE texts SET name = :name, txt = :txt WHERE id = :id");

/*
 *
 * Товары
 */

define("SQL_GET_GOODS", "SELECT SQL_CALC_FOUND_ROWS g.*, c.title as company, c1.name as cat, p.filename as image FROM goods g LEFT JOIN companies c ON (g.company_id=c.id) LEFT JOIN cats c1 ON (g.cat_id=c1.id) LEFT JOIN photos p ON (p.id=g.image_id)");
define("SQL_GET_GOOD", "SELECT g.*, p.filename as logo, GROUP_CONCAT(i.filename SEPARATOR '::') as images, c.name as cat FROM goods g LEFT JOIN photos p ON (p.id=g.image_id) LEFT JOIN photos i ON (i.good_id=g.id) LEFT JOIN cats c ON (c.id=g.cat_id) WHERE g.id = :id GROUP BY g.id");
define("SQL_UPDATE_GOOD", "UPDATE goods SET title = :title, description = :description, price = :price, company_id = :company_id, cat_id = :cat_id, image_id = :image_id WHERE id = :good_id");
define("SQL_INSERT_GOOD", "INSERT INTO goods(title, description, price, company_id, cat_id, image_id, dt) VALUES(:title, :description, :price, :company_id, :cat_id, :image_id, :dt)");
define("SQL_DELETE_GOOD", "DELETE FROM goods WHERE id = :id");
define("SQL_INSERT_GOOD_SPECIFIC", "INSERT INTO good_specifications(good_id, spec_name, spec_value) VALUES(:good_id, :name, :value)");
define("SQL_UPDATE_GOOD_IMAGE", "UPDATE goods SET image_id = :image_id WHERE id = :good_id");

/*
 * Пользователи
 */
define("SQL_INSERT_WORKERS", "INSERT INTO users(mail, surname, name, patro, city, birthday, sex, group_id, avatar, pass, balance) VALUES(:mail, :surname, :name, :patro, :city, :birthday, :sex, :group_id, :avatar, :pass, :balance)");

define("SQL_GET_USERS",
"SELECT SQL_CALC_FOUND_ROWS u.*,t.type_name, i.* FROM users u, user_type t, user_type_user utu, user_info i WHERE
u.user_id = utu.id_user AND utu.id_type=t.type_id AND i.info_id=u.user_id");
define("SQL_GET_LAST_USER","SELECT LAST_INSERT_ID() FROM users");
define("SQL_GET_USER", "SELECT u.*, t.*, i.* FROM users u, user_type_user utu,
user_type t, user_info i WHERE u.user_id = :id AND u.user_id = utu.id_user AND
utu.id_type=t.type_id AND i.info_id=u.user_id");
define("SQL_GET_USER_BY_NICKNAME", "SELECT u.user_id FROM users u WHERE u.user_nickname = :nickname");
define("SQL_GET_ALL_PHONES","SELECT p.* FROM user_phone p WHERE p.phone_user_id=:id");
define("SQL_GET_GROUPS", "SELECT * FROM user_type");
define("SQL_UPDATE_USER_INFO", "UPDATE user_info u SET u.info_familiya = :surname, u.info_imya = :name,
u.info_otchstvo = :patro, u.info_address = :city, u.info_bd = :birthday, u.info_sex = :sex,
u.info_mail = :mail, u.info_others=:info_others WHERE u.info_id = :id");
define("SQL_UPDATE_USER_GROUP", "UPDATE user_type_user u SET u.id_type = :group_id WHERE u.id_user = :id");
define("SQL_UPDATE_USER", "UPDATE users u SET u.user_nickname=:nickname, u.user_phone=:main_phone
 WHERE u.user_id = :id");
define("SQL_UPDATE_USER_2", "UPDATE users u SET u.user_nickname=:nickname, u.user_phone=:main_phone, u.user_password=:user_password
 WHERE u.user_id = :id");

define("SQL_INSERT_USER", "INSERT INTO users(user_phone,user_nickname, user_password) VALUES(
:main_phone,:nickname, :pass)");
define("SQL_INSERT_USER_TYPE_USER", "INSERT INTO user_type_user(id_user, id_type) VALUES(
:id_user,:id_type)");
define("SQL_INSERT_USER_INFO", "INSERT INTO user_info(info_id, info_imya, info_familiya,
info_otchstvo, info_address, info_mail, info_sex, info_bd, info_others) VALUES(:id
,:name, :surname, :patro, :city, :mail, :sex,:birthday, :info_others)");

define("SQL_DELETE_USER", "DELETE FROM users WHERE user_id = :id");
define("SQL_GET_USER_BY_MAIl", "SELECT * FROM users WHERE LOWER(mail) = LOWER(:mail)");
define("SQL_GET_ALL_USERS", "SELECT u.id FROM users u ORDER BY id desc");

/*
 * Фотографий
 */

define("SQL_ADD_NEW_IMAGE", "INSERT INTO photos(filename, good_id) VALUES(:name, :good_id)");

/**
 * Посты
 */

define("SQL_DELETE_POST", "DELETE FROM posts WHERE post_id=:id_post");
define("SQL_DELETE_PCP", "DELETE FROM post_cat_post  WHERE id_post=:id_post");
define("SQL_DELETE_PA", "DELETE FROM post_author  WHERE id_post=:id_post ");

define("SQL_GET_COUNT_OF_POSTS_OF_USER",
"SELECT COUNT(p.post_id) as count_post FROM posts p, post_author a, post_category cc, post_cat_post pcp,
 post_status s, post_status_post psp WHERE a.id_author=:id AND a.id_post=p.post_id AND pcp.id_post=p.post_id AND cc.cat_id=pcp.id_category AND psp.id_post=p.post_id AND psp.id_status=s.status_id");

define("SQL_GET_SUM_OF_VIEWS_POSTS_OF_USER",
"SELECT SUM(p.post_views) as views_post FROM posts p, post_author a, post_category cc, post_cat_post pcp,
 post_status s, post_status_post psp WHERE a.id_author=:id AND a.id_post=p.post_id AND pcp.id_post=p.post_id AND cc.cat_id=pcp.id_category AND psp.id_post=p.post_id AND psp.id_status=s.status_id");


define("SQL_GET_POSTS_OF_USER",
"SELECT SQL_CALC_FOUND_ROWS p.*,cc.*, s.* FROM posts p, post_author a, post_category cc, post_cat_post pcp,
 post_status s, post_status_post psp WHERE a.id_author=:id AND a.id_post=p.post_id AND pcp.id_post=p.post_id
 AND cc.cat_id=pcp.id_category AND psp.id_post=p.post_id AND psp.id_status=s.status_id");

define("SQL_GET_POST_BY_ID","SELECT SQL_CALC_FOUND_ROWS p.*,cc.*, s.* FROM posts p, post_category cc, post_cat_post pcp, post_status s, post_status_post psp WHERE pcp.id_post=p.post_id AND cc.cat_id=pcp.id_category AND psp.id_post=p.post_id AND psp.id_status=s.status_id AND p.post_id=:post_id");

define("SQL_GET_LAST_POST","SELECT LAST_INSERT_ID() FROM posts");

define("SQL_INSERT_POST", "INSERT INTO posts( post_name, post_description, post_text, post_image, post_date,
post_views, post_seo_title, post_seo_description) VALUES(:post_name, :post_description,
 :post_text, :post_image ,:post_date, 0, :post_seo_title, :post_seo_description)");

define("SQL_INSERT_STATUS_POST", "INSERT INTO post_status_post(id_post, id_status) VALUES(:id_post,4)");
define("SQL_INSERT_CAT_POST", "INSERT INTO post_cat_post(id_post, id_category) VALUES(:id_post,:id_category)");
define("SQL_INSERT_POST_AUTHOR", "INSERT INTO post_author(id_post, id_author) VALUES(:id_post,:id_author)");

define("SQL_GET_POST_CATEGORY", "SELECT * FROM post_category");

define("SQL_UPDATE_POST", "UPDATE posts p SET p.post_name =:post_name, p.post_description =:post_description,p.post_text =:post_text
, p.post_seo_title =:post_seo_title, p.post_seo_description =:post_seo_description WHERE p.post_id = :post_id");

define("SQL_UPDATE_POST_WITH_IMG", "UPDATE posts p SET p.post_image = :post_image, p.post_name = :post_name, p.post_description = :post_description,
p.post_text = :post_text, p.post_seo_title = :post_seo_title, p.post_seo_description = :post_seo_description
 WHERE p.post_id = :post_id");

define("SQL_UPDATE_CATEGORY", "UPDATE post_cat_post p SET p.id_category = :post_category WHERE p.id_post = :post_id");


define("SQL_GET_POSTS",
"SELECT SQL_CALC_FOUND_ROWS p.*,cc.*, s.* FROM posts p, post_category cc, post_cat_post pcp,
 post_status s, post_status_post psp WHERE pcp.id_post=p.post_id
 AND cc.cat_id=pcp.id_category AND psp.id_post=p.post_id AND psp.id_status=s.status_id");

/**
 * Активность
 */
define("SQL_GET_ACTIVITY_OF_USER",
"SELECT SQL_CALC_FOUND_ROWS a.*,t.* FROM user_activity a, user_activity_type t WHERE a.ac_user_id=:id AND a.ac_activity_type=t.uat_id");



?>