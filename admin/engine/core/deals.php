<?php
/**
 * File: deals.php
 * Хранит функций связанные со сделками
 */

function getDeals($start = null, $limit = null, $add = null)
{
    $query = '';
    if(!is_null($add)) $query .= $add;
    if(!is_null($start) && !is_null($limit)) $query .= " LIMIT $start, $limit";

    $d = q(SQL_GET_DEALS.$query, null);
    return $d;
}

function getDeal($id)
{
    $d = q(SQL_GET_DEAL, array('id' => $id));
    return $d[0];
}

function editDeal($post)
{
    include_once "classes/Validation.php";

    $fillable = array(
        array(
            'name' => 'type',
            'title' => 'Тип',
            'rules' => 'numeric'
        ),
        array(
            'name' => 'first',
            'title' => 'Первый пользователь',
            'rules' => 'required|notEqualTo:second:Второй пользователь'
        ),
        array(
            'name' => 'second',
            'title' => 'Второй пользователь',
            'rules' => 'required|notEqualTo:first:Первый пользователь'
        ),
        array(
            'name' => 'good_id',
            'title' => 'Товар',
            'rules' => 'numeric'
        ),
        array(
            'name' => 'make_dt',
            'title' => 'Дата сделки',
            'rules' => 'required'
        ),
        array(
            'name' => 'shipment_dt',
            'title' => 'Дата отгузки',
            'rules' => 'required'
        ),
        array(
            'name' => 'status',
            'title' => 'Статус',
            'rules' => 'numeric'
        ),
        array(
            'name' => 'comment',
            'title' => 'Коммент',
            'rules' => 'required'
        )
    );

    $valid = new Validation($fillable, $post);

    if($valid->run() == true){

        $data = array();
        $data = $valid->getResult();
        $data['deal_id'] = $post['deal_id'];


        if(q2(SQL_UPDATE_DEAL, $data)){
            buildMsg('Сделка отредактирована');
            return true;
        }return false;

    }else{
        $errors = $valid->getErrors();
        foreach($errors as $e) buildMsg($e, 'danger');
        return false;
    }
}

?>