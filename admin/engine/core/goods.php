<?php
/**
 * File: goods.php
 * Хранит функций связанные с товарами и их категориями
 */

function getCats($start = null, $limit = null)
{
    $liting = (is_null($start) && is_null($limit)) ? '' : " LIMIT $start, $limit";
    $cats = q(SQL_GET_CATS.$liting, null);
    return $cats;
}

function getCat($id)
{
    $c = q(SQL_GET_CAT, array('id' => $id));
    return $c[0];
}

function editCat($cat)
{
    $err = array();
    if(empty($cat['name'])) $err[] = 'Введите название';
    if($cat['parent_id'] == $cat['cat_id']) $err[] = 'Категория не может быть родителем над собой';

    if(count($err) > 0){
        foreach($err as $e) buildMsg($e, 'danger');
        return false;
    }else{
        if(q2(SQL_UPDATE_CAT, array('name' => $cat['name'], 'parent_id' => $cat['parent_id'], 'cat_id' => $cat['cat_id']))){
            buildMsg('Категрия  успешно отредактирована');
            return true;
        }else return false;
    }
}

function addCat($cat)
{
    $err = array();
    if(empty($cat['name'])) $err[] = 'Введите название';
    //if($cat['parent_id'] == $cat['cat_id']) $err[] = 'Категория не может быть родителем над собой';

    if(count($err) > 0){
        foreach($err as $e) buildMsg($e, 'danger');
        return false;
    }else{
        if(q2(SQL_ADD_CAT, array('name' => $cat['name'], 'parent_id' => $cat['parent_id']))){
            buildMsg('Категрия  успешно добавлена');
            return true;
        }else return false;
    }
}

function deleteCat($id)
{
    if(q2(SQL_DELETE_CAT, array('id' => $id))) return true;
    else return false;
}

function getGoods($start = null, $limit = null, $add = null)
{
    $query = '';
    if(!is_null($start) && !is_null($limit)) $query = " LIMIT $start, $limit";

    $goods = q(SQL_GET_GOODS.$add.$query, null);
    return $goods;
}

function getGoodsSite($start = null, $limit = null, $comp_id = 0)
{
    //global $config;

    $query = '';
    if(!is_null($start) && !is_null($limit)) $query = " LIMIT $start, $limit";

    $where = array();
    if(is_numeric($_GET['cat'])){
        $cat = enc($_GET['cat']);
        $where[] = " g.cat_id='$cat'";
    }

    if($comp_id != 0) $where[] = " g.company_id='$comp_id'";

    $whereString = '';
    if(count($where) > 0){
        $whereString = ' WHERE';
        $whereString .= implode(' AND', $where);
    }

    $goods = q(SQL_GET_GOODS.$whereString.$query, null);
    return $goods;
}

function getGood($id)
{
    $goods = q(SQL_GET_GOOD, array('id' => $id));
    if(count($goods) == 0) return false;
    return $goods[0];
}

function editGood($good, $file)
{
    $fillable = array(
        'title' => 'Название',
        'cat_id' => 'Категория',
        'company_id' => 'Компания',
        'price' => 'Цена',
        'description' => 'Описание',
        'good_id' => 'Товар'
    );

    $data = array();
    $err = array();
    foreach($fillable as $k=>$v){
        if(empty($good[$k])) $err[] = "Вы не заполнили поле: $v";
        $data[$k] = $good[$k];
    }

    if(count($err) > 0) {
        foreach($err as $e) buildMsg($e, 'danger');
        return false;
    }else{

        if(empty($file['tmp_name'])){
            $data['image_id'] = $good['old_image'];
        }else{
            $img = uploadImage($file);
            $data['image_id'] = $img['id'];
        }

        if(q2(SQL_UPDATE_GOOD, $data)){
            buildMsg('Товар отредактирован');
            return true;
        }else return false;
    }


}

function addGood($good, $file)
{
    $fillable = array(
        'title' => 'Название',
        'cat_id' => 'Категория',
        'company_id' => 'Компания',
        'price' => 'Цена',
        'description' => 'Описание'
    );

    $data = array();
    $err = array();
    foreach($fillable as $k=>$v){
        if(empty($good[$k])) $err[] = "Вы не заполнили поле: $v";
        $data[$k] = $good[$k];
    }

    $image_id = 0;
    if(!empty($file['tmp_name'])){
        $image = uploadImage($file, array('using' => false, 'type' => 'good', 'target' => 0));
        $image_id = $image['id'];
    }

    if(count($err) > 0) {
        foreach($err as $e) buildMsg($e, 'danger');
        return false;
    }else{
        $data['image_id'] = $image_id;
        $data['dt'] = date('Y-m-d H:i:s');
        if(q2(SQL_INSERT_GOOD, $data)){
            buildMsg('Товар добавлен');
            return true;
        }else return false;
    }


}

function addGoodSite($good, $files)
{
    $fillable = array(
        'title' => 'Название',
        'price' => 'Цена',
        'description' => 'Описание'
    );

    $data = array();
    $err = array();

    foreach($fillable as $k=>$v){
        if(empty($good[$k])) $err[] = "Вы не заполнили поле: $v";
        $data[$k] = $good[$k];
    }

    $image_id = 0;

    if(is_numeric($good['cat_id']) && $good['cat_id'] != 0) $data['cat_id'] = $good['cat_id'];
    else $err[] = 'Выберите категорию.';

    $files = reArrayFiles($files);

    $data['company_id'] = $_SESSION['user']['company_id'];

    if(count($err) > 0) {
        $_SESSION['addGood'] = $good;
        foreach($err as $e) buildMsg($e, 'danger');
        return false;
    }else{

        $data['image_id'] = $image_id;
        $data['dt'] = date('Y-m-d H:i:s');

        if(q2(SQL_INSERT_GOOD, $data)){

            $good_id = qInsertId();

            $i = 0;
            foreach($files as $file){
                $using = ($i == 0) ? true : false;
                uploadImage($file, array('using' => $using, 'type' => 'good', 'target' => $good_id));
                $i++;
            }

            if(isset($good['spc']) && isset($good['spcName'])){
                $num = count($good['spc']);
                for($k = 0;$k<$num;$k++) addGoodSpecific($good_id, $good['spc'][$k], $good['spcName'][$k]);
            }

            buildMsg('Товар добавлен');
            return true;
        }else return false;
    }


}

function setGoodImage($good_id, $image_id){
    if(q2(SQL_UPDATE_GOOD_IMAGE, array('good_id' => $good_id, 'image_id' => $image_id))) return true;
    else return false;
}

function addGoodSpecific($good_id, $value, $name)
{
    $value = trim($value);
    $name = trim($name);

    if(q2(SQL_INSERT_GOOD_SPECIFIC, array('good_id' => $good_id, 'value' => $value, 'name' => $name))) return true;
    else return false;
}

function deleteGood($id)
{
    if($_SESSION['user']['group_id'] != 1){
        buildMsg('У вас нет прав для этой функций', 'warning');
        return false;
    }

    if(q2(SQL_DELETE_GOOD, array('id' => $id))) return true;
    else return false;
}

function getGoodImages($id)
{
    $images = q(SQL_GET_GOOD_IMAGES, array('good_id' => $id));
    return $images;
}

?>