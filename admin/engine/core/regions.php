<?php

function getRegions()
{
    $r = q(SQL_GET_REGIONS, null);
    return $r;
}

function addCompanyRegion($comp_id, $reg_id)
{
    if(q2(INSERT_NEW_COMP_REGION, array('comp_id' => $comp_id, 'reg_id' => $reg_id))) return true;
    else return false;
}

?>