<?php
/**
 * File: services.php
 * Хранит функций связанные с платными услугами
 */

function getServices(){
    $s = q(SQL_GET_SERVICES, null);
    return $s;
}

function getService($id)
{
    $s = q(SQL_GET_SERVICE, array('id' => $id));
    return $s[0];
}

function editService($post)
{

    $err = array();

    if(empty($post['name'])) $err[] = 'Заполните поле Название';
    if(empty($post['price'])) $err[] = 'Заполните поле Цена';

    if(count($err) > 0){
        foreach($err as $e) buildMsg($e, 'danger');
        return false;
    }else{
        if(q2(SQL_UPDATE_SERVICE, array('name' => $post['name'], 'price' => $post['price'], 'ser_id' => $post['ser_id']))){
            buildMsg('Услуга отредактирована');
            return true;
        }else return false;
    }
}

function deleteService($id)
{
    if(q2(SQL_DELETE_SERVICE, array('id' => $id))) return true;
    else return false;
}

?>