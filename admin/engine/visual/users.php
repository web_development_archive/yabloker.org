<?php

function showCarrierSearch()
{
    return <<<HTML
    <strong class="chatRightTitle">Поиск перевозчиков</strong>
        <section class="chatRightTop">
          <header>Наталья предлагает заключить сделку</header>
          <footer>12.07.2014</footer>
        </section>
        <script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=51OJ7DpZsk9GBh5oflGyd8dOHboU4aac&width=223&height=200"></script>
        <label style="margin: 20px 0 0px 0" class="chatRightLabel">Откуда</label>
        <input type="text" id="chatInput1_3">
        <label class="chatRightLabel">Куда</label>
        <input type="text" id="chatInput1_2">
        <label class="chatRightLabel">Тип товара</label>
        <select id="chatInput1_1" name="a">
          <option value="1" selected>1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
        </select>
        <label class="chatRightLabel">Объем</label>
        <input type="text" id="chatInput">
        <label class="chatRightLabel">Комментарий</label>
        <textarea class="chatRightTextArea"></textarea>
        <section class="btn btn-info chatRightBtn">Искать перевозчика</section>
HTML;
}

/*
 * Диалоги
 * @param int $id - id пользователя
 */
function showUserDialogs($id)
{
    return <<<HTML
    <section class="chatTitle">
          Сообщения
        </section>
        <section class="chat">
          <header>
            <section class="chatActiv" id="chat_1" onclick="showChat(1)">
              <section class="chatNews">4</section>
              <section class="chatNameTop">РотФронт(Сергей)</section>
            </section>
            <section class="chatNoneActiv" id="chat_2" onclick="showChat(2)">
              <section class="chatNews">4</section>
              <section class="chatNameTop">Vanya Petrov</section>
            </section>
            <section class="chatNoneActiv" id="chat_3" onclick="showChat(3)">
              <section class="chatNews">7</section>
              <section class="chatNameTop">Arseniy Zharov</section>
            </section>
          </header>
          <section id="chat1" class="chtcls">
            <section class="chatMsgRight">
              <section>
                <section class="chatMsgRightTime">10:31 AM</section>
                <section class="chatMsgRightName">Наталья Алексеева</section>
              </section>
              <section class="chatMsgRightMsg">
                Товарищи! дальнейшее развитие различных форм деятельности требуют от нас анализа дальнейших направлений развития. Повседневная практика показывает, что рамки и место обучения кадров требуют от нас анализа дальнейших направлений развития.
              </section>
              <img alt="img" src="img/msgRight.png" class="chatMsgRightZak">
              <img alt="img" class="chatUserPhotoB" src="img/userPhoto.png">
            </section>
            <section class="clearfix"></section>
            <section class="chatMsgLeft">
              <section class="chatMsgLeftTime">10:31 AM</section>
              <img alt="img" class="chatUserPhotoB" src="img/userPhoto.png">
              <img alt="img" src="img/msgLeft.png" class="chatMsgLeftZak">
              <section class="chatMsgLeftMsg">
                Товарищи! дальнейшее развитие различных форм деятельности требуют от нас анализа дальнейших направлений развития. Повседневная практика показывает, что рамки и место обучения кадров требуют от нас анализа дальнейших направлений развития.
              </section>
            </section>
            <section class="clearfix"></section>
            <section class="chatMsgLeft">
              <section class="chatMsgLeftTime">10:31 AM</section>
              <img alt="img" class="chatUserPhotoB" src="img/userPhoto.png">
              <img alt="img" src="img/msgLeft.png" class="chatMsgLeftZak">
              <section class="chatMsgLeftMsg">
                Товарищи! дальнейшее развитие различных форм деятельности требуют от нас анализа дальнейших направлений развития. Повседневная практика показывает, что рамки и место обучения кадров требуют от нас анализа дальнейших направлений развития.
              </section>
            </section>
            <section class="clearfix"></section>
          </section>
          <section id="chat2" class="chtcls" style="display:none">
            <section class="chatMsgRight">
              <section>
                <section class="chatMsgRightTime">10:31 AM</section>
                <section class="chatMsgRightName">Наталasfasfsafа</section>
              </section>
              <section class="chatMsgRightMsg">
                Товарищи! дальнейшерамки и место обучения кадров требуют от нас анализа дальнейших направлений развития.
              </section>
              <img alt="img" src="img/msgRight.png" class="chatMsgRightZak">
              <img alt="img" class="chatUserPhotoB" src="img/userPhoto.png">
            </section>
            <section class="clearfix"></section>
            <section class="chatMsgLeft">
              <section class="chatMsgLeftTime">10:31 AM</section>
              <img alt="img" class="chatUserPhotoB" src="img/userPhoto.png">
              <img alt="img" src="img/msgLeft.png" class="chatMsgLeftZak">
              <section class="chatMsgLeftMsg">
                Товарищи! дальнейшее развитие различных форм деятельности требуют от нас анализа дальнейших направазвития.
              </section>
            </section>
            <section class="clearfix"></section>
            <section class="chatMsgLeft">
              <section class="chatMsgLeftTime">10:31 AM</section>
              <img alt="img" class="chatUserPhotoB" src="img/userPhoto.png">
              <img alt="img" src="img/msgLeft.png" class="chatMsgLeftZak">
              <section class="chatMsgLeftMsg">
                Товарищи! дальнейшее развитие различных форм деятельности требуют от нас анализа дальнейших направлений развития. Повседневная практика показывает, что рамки и место обучения кадров требуют от нас анализа дальнейших направлений развития.
              </section>
            </section>
            <section class="clearfix"></section>
          </section>
          <section id="chat3" class="chtcls" style="display:none">
            <section class="chatMsgRight">
              <section>
                <section class="chatMsgRightTime">10:31 AM</section>
                <section class="chatMsgRightName">Натаasfлексеева</section>
              </section>
              <section class="chatMsgRightMsg">
                Товарищи! дальнейшее развитие разли141421412ия.
              </section>
              <img alt="img" src="img/msgRight.png" class="chatMsgRightZak">
              <img alt="img" class="chatUserPhotoB" src="img/userPhoto.png">
            </section>
            <section class="clearfix"></section>
            <section class="chatMsgLeft">
              <section class="chatMsgLeftTime">10:31 AM</section>
              <img alt="img" class="chatUserPhotoB" src="img/userPhoto.png">
              <img alt="img" src="img/msgLeft.png" class="chatMsgLeftZak">
              <section class="chatMsgLeftMsg">
                Товарищи! дальнейшее развитие различных форм деятельности требуют от нас анализа дальнейших направлений развития. Повседневная практика показывает, что рамки и мasfqwrqw развития.
              </section>
            </section>
            <section class="clearfix"></section>
            <section class="chatMsgLeft">
              <section class="chatMsgLeftTime">10:31 AM</section>
              <img alt="img" class="chatUserPhotoB" src="img/userPhoto.png">
              <img alt="img" src="img/msgLeft.png" class="chatMsgLeftZak">
              <section class="chatMsgLeftMsg">
                Товарищи! дальнейшее развитие различных форм деятельности требуют от нас анализа дальнейq41fqfkpq fsna vkas kans kasи место обучения кадров требуют от нас анализа дальнейших направлений развития.
              </section>
            </section>
            <section class="clearfix"></section>
          </section>
          <footer>
            <img alt="img" class="chatUserPhoto" src="img/userPhoto.png">
            <textarea></textarea>
            <section>
              <section class="btn btn-info chatUserBtn">Отправить сообщение</section>
              <input type="file">
            </section>
          </footer>
        </section>
        <section class="chatCntLeftBtm">
          <section class="btn btn-success chatCntLeftBtmBtn">Готово</section>
          <section class="chatCntLeftBtmTxt">
            Нажимая кнопку «Готово» сделка добавляется в список сделок пользователя после запроса дополнительных параметров, которые пользователь может ввести по желанию. Остальные пользователи – участники диалога получают оповещение с предложением также ввести дополнительные параметры сделки для добавления в журнал сделок.
          </section>
        </section>
HTML;
}

// Форма регистраций
function showSignUpForm()
{
    $errors = getMiniErrors(); // Маленькие системные сообщения

    if($errors != false){
        $errors = <<<HTML
        <section class="regUserAhtung">
            <section class="regUserAhtungHeader">Ошибки!</section>
            <section class="regUserAhtungFooter">
              {$errors}
            </section>
          </section>
HTML;
    }else $errors = '';

    return <<<HTML
    <section class="regUserTop">
        <section class="regUserTopText">Регистрация пользователя</section>
        <!--<a href="" class="regUserTopCom">Зарегистрировать компанию</a>-->
      </section>
      <form action="/login.php?action=signUp" method="post" enctype="multipart/form-data">
        <section class="regUserCol1">
          <img alt="img" src="http://6.firepic.org/6/images/2014-07/31/d0u4p95a0m9y.jpg" id="userImage" class="regUserLogo">
          <input type="file" name="avatar" onchange="readURL(this);" value="Загрузить фото" id="fileUpload" style="display: none">
          <section class="btn btn-info regUserBtnPhoto" id="uploadHandler">Загрузить</section>
          <!--<section class="regUserFirst">
            <section class="regUserFirstX">x</section>
            <section class="regUserFirstDelete">Удалить фото</section>
          </section>-->
        </section>
        <section class="regUserCol2">
          <label class="regUserLabel">Имя</label>
          <input type="text" name="name" value="{$_SESSION['regTemp']['name']}" id="regUserInput1_1">
          <label class="regUserLabel">Фамилия</label>
          <input type="text" name="surname" value="{$_SESSION['regTemp']['surname']}" id="regUserInput1_2">
          <label class="regUserLabel">Отчество</label>
          <input type="text" name="patro" value="{$_SESSION['regTemp']['patro']}" id="regUserInput1_3">
        </section>
        <section class="regUserCol3">
          <label class="regUserLabel">Email</label>
          <input type="text" name="mail" value="{$_SESSION['regTemp']['mail']}" id="regUserInput1_4">
          <label class="regUserLabel">Пароль</label>
          <input type="password" name="pass" id="regUserInput1_5">
          <label class="regUserLabel">Город</label>
          <input type="text" name="city" value="{$_SESSION['regTemp']['city']}" id="regUserInput1_6">
          <label class="regUserLabel">Дата рождения</label>
          <!--          <input type="text" data-format="dd/MM/yyyy hh:mm:ss" id="regUserInput">-->
          <div class="input-append date">
            <input type="text" name="birthday" value="{$_SESSION['regTemp']['birthday']}" class="datepicker" id="regUserInput2">
          </div>
          <section>
            <input type="radio" id="sex1" name="sex" class="r" value="1">
            <label for="sex1" class="regUserPol">мужской пол</label>
          </section>
          <section style="margin:10px 0 0 0;">
            <input type="radio"  id="sex2" name="sex" class="r" value="0">
            <label for="sex2" class="regUserPol">женский пол</label>
          </section>
          <input class="btn btn-info regUserBtnPhoto" name="subSignUp" style="padding:10px;" value="Зарегистрироваться" type="submit">
        </section>
        <section class="regUserCol4">
        {$errors}
          <section class="regUserWhy">
            <section class="regUserAhtungHeader">Зачем регистрироваться?</section>
            <section class="regUserWhyFooter">
              Не следует, однако забывать, что реализация намеченных плановых заданий способствует подготовке и реализ ации существенных финансовых и административных условий. Не следует, однако забывать, что начало повседневной работы по формированию позиции обеспечивает широкому кругу (специалистов) участие в формировании новых предложений.
            </section>
          </section>
        </section>
      </form>
HTML;
}

function showUserLoginFormTop()
{
    return <<<HTML
<section class="btn-group" style="padding-top:13px">
<a href="#loginFormModal" class="btn btn-default" data-toggle="modal">Вход</a>
<a href="login.php?action=showSignUp" class="btn btn-default">Регистрация</a>
<div id="loginFormModal" class="modal modal-sm hide fade" tabindex="-1" role="dialog" aria-labelledby="loginFormModal" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Форма входа</h3>
  </div>
  <div class="modal-body">
    <form action="/login.php?action=signIn" method="post">
        <div class="form-group">
            <label>Email</label>
            <input type="text" name="mail" class="form-control input-sm" />
        </div>
        <div class="form-group">
            <label>Пароль</label>
            <input type="password" name="pass" class="form-control input-sm" />
        </div>
        <div class="form-group"><input type="submit" name="subLogin" value="Войти" class="btn btn-warning" /></div>
     </form>
  </div>
</div>
</section>
HTML;
}

function showUserMiniProfile()
{
    global $config;

    $me = getSession();

    $img = (empty($me['avatarName'])) ? $config['defaultAvatarMini'] : getImageThumb($me['avatarName'], 50, 50);

    return <<<HTML
        <section class="lsTop btn-group">
              <section data-toggle="dropdown">
                <img src="{$img}" alt="img" class="lsTopP inlineTable" />
                <label class="lsTopU inlineTable">
                    {$me['name']}
                </label>
                <!--<label class="lsTopN inlineTable">2</label>-->
              </section>
              <ul class="dropdown-menu" style="margin: 0 0 0 0px;">
                <li><a href="{$config['home']}cabinet/"><i class="icon-home"></i> Личный кабинет</a></li>
                <li class="divider"></li>
                <li><a href="{$config['home']}login.php?action=logout"><i class="icon-off"></i> Выход</a></li>
              </ul>
            </section>
HTML;

}

?>