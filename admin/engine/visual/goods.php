<?php

/*
 * Вывод категорий на главной странице
 */

function showBreadcrumbs($type, $target)
{
    global $config;
    $show = '';

    switch($type){
        case "good":
            $show = <<<HTML
    <ul class="breadcrumb">
      <li><a href="{$config['home']}">Главная</a> <span class="divider">/</span></li>
      <li><a href="/goods.php?cat={$target['cat_id']}">{$target['cat']}</a> <span class="divider">/</span></li>
      <li class="active">{$target['title']}</li>
    </ul>
HTML;
            break;
        case "cat":
            $show = <<<HTML
    <ul class="breadcrumb">
      <li><a href="{$config['home']}">Главная</a> <span class="divider">/</span></li>
      <li class="active">{$target['cat']}</li>
    </ul>
HTML;
            break;
    }
    return $show;
}

/*
 * Выводит информацию об опреледеленном товаре
 * Аргумент $good является массивом, в котором хранится все данные
 */

function showGoodOne($good)
{
    $mainImage = ($good['logo'] != 0) ? getImageThumb($good['logo'], 320, 200) : 'http://placehold.it/320x200';

    $images = '<div class="active item"><img src="'.$mainImage.'"></div>';

    if(!empty($good['images'])){
        $iArray = explode('::', $good['images']);
        foreach($iArray as $img){
            $img = getImageThumb($img, 320, 200);
            if(strcasecmp($mainImage, $img) == 0) continue;
            $images .= '<div class="item"><img src="'.$img.'"></div>';
        }
    }

    $breadcrumbs = showBreadcrumbs("good", $good);

    return <<<HTML
    <section class="itemRightTop">
    {$breadcrumbs}
          <header class="itemRightTopHeader">
            {$good['title']}
          </header>
        </section>
        <section class="itemMain">
          <section class="itemMainLeft">
            <section>
            <div id="myCarousel" class="carousel slide">
              <!-- Carousel items -->
              <div class="carousel-inner">
                {$images}
              </div>
              <!-- Carousel nav -->
              <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
              <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
            </div>
            </section>
            <!--<section>
              <img alt="img" src="/img/imagesItem1.png" onclick="someItem(1)" class="itemMainLeftSmallImg imlsi1">
              <img alt="img" src="/img/imagesItem2.png" onclick="someItem(2)" class="itemMainLeftSmallImg imlsi2">
              <img alt="img" src="/img/imagesItem3.png" onclick="someItem(3)" class="itemMainLeftSmallImg imlsi3">
            </section>-->
          </section>
          <section class="itemMainRight">
            <section class="itemMainRightHeader">
              <section class="itemMainRightCoin">
                <section class="itemMainRightRu">
                  {$good['price']} Р
                </section>
                <section class="itemMainRightKg">
                  /кг
                </section>
              </section>
              <section class="itemMainRightBtn">
                <a href="/im.php" class="btn btn-info itemRightBtn">
                  Договориться
                </a>
              </section>
            </section>
            <section class="itemMainRightText">
              {$good['description']}
            </section>
          </section>
        </section>

        <section class="reviewRightTitle2">
          Дополнительная документация и файлы
        </section>
        <section>
          <section class="reviewFile">
            <img alt="img" src="/img/file1.png" class="reviewFileImg">
            <section class="reviewFileRight">
              <section class="reviewFileTitle"><a href="">Разрешение на торговлю пищевыми продуктами</a></section>
              <section class="reviewFileSize">pdf, 546 Кб</section>
            </section>
          </section>
          <section class="reviewFile">
            <img alt="img" src="/img/file2.png" class="reviewFileImg">
            <section class="reviewFileRight">
              <section class="reviewFileTitle"><a href="">Прайс-лист на молоко</a></section>
              <section class="reviewFileSize">pdf, 546 Кб</section>
            </section>
          </section>
          <section class="reviewFile">
            <img alt="img" src="/img/file2.png" class="reviewFileImg">
            <section class="reviewFileRight">
              <section class="reviewFileTitle"><a href="">Прайс-лист на молоко</a></section>
              <section class="reviewFileSize">pdf, 546 Кб</section>
            </section>
          </section>
        </section>
        <!--<section class="reviewRightTitle2">
          Предоставляемый товар
        </section>
        <section>
          <section class="reviewWhat">Кисломолочные </section><section class="reviewRightNumbers">78</section>
        </section>
        <section class="reviewTovars">
          <img alt="img" src="/img/tovar1.png" class="reviewTovarsImg">
          <section class="reviewTovarsText">Деревенский домашний сыр</section>
        </section>
        <section class="reviewTovars">
          <img alt="img" src="/img/tovar2.png" class="reviewTovarsImg">
          <section class="reviewTovarsText">Деревенский кефир</section>
        </section>
        <section class="reviewTovars">
          <img alt="img" src="/img/tovar3.png" class="reviewTovarsImg">
          <section class="reviewTovarsText">Деревенский творог</section>
        </section>
        -->
HTML;

}

function showGoods($user_id = 0, $breadcrumb = "cat")
{
    global $config;

    include_once ROOT."/engine/classes/Pagination/Pagination.class.php";

    // --=Pagination=-- //
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $per = $config['rowPerPage'];

    $start = ($page-1)*$per; // Старт
    $limit = $per; // Сколько выводить на страницу
    $goods = getGoodsSite($start, $limit, $user_id);

    if(count($goods) == 0) return 'Ничего не найдено';

    $count = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $count)); // Создаем пагинацию
    $pagination->setRPP($per); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    // ----------------------- //

    $show = showBreadcrumbs($breadcrumb, $goods[0]);
    $show .= showGoodsShort($goods);

    $show .= $markup;

    return $show;
}

function showGoodsShort($goods)
{
    global $config;
    $show = '';

    foreach($goods as $good){
        $img = ($good['image_id'] == 0) ? 'http://placehold.it/225x130' : getImageThumb($good['image'], 225, 130);
        $show .= <<<HTML
        <section class="goodsItem">
          <img alt="img" src="{$img}" class="reviewTovarsImg">
          <section class="reviewTovarsText"><a href="{$config['home']}good/{$good['id']}">{$good['title']}</a></section>
        </section>
HTML;
    }
    return $show;
}

function outCatsTree($category_arr, $parent_id, $level) {
    global $config;

    $show = '';
    if (isset($category_arr[$parent_id])) {
        $ulClass = ($level != 0) ? ' class="podMenu'.$parent_id.' podMenu" style="display:none"' : '';

        $show = '<ul'.$ulClass.'>';

        $liClass = ' class="textTitleMenu"';
        $numClass = ' class="newMenuSimple inlineTable"';

        $i = 0;
        foreach ($category_arr[$parent_id] as $cat) { //Обходим
            $level++;
            $cats = outCatsTree($category_arr, $cat["id"], $level);
            $level--;

            $onclick = ($level == 0 && !empty($cats)) ? 'onclick="showMenu('.$cat['id'].'); return false;"' : '';
            $section = ' class="textTitleMenuMain textTitleMenuMain'.$cat['id'].' inlineTable"';

            if(empty($cats) && $level != 0){
                $liClass = " class=\"podMenuText\"";
                $section = ' class="podMenuTextTitle"';
                $numClass = ' class="podNewMenuSimple inlineTable"';
            }

            $style = (is_numeric($_GET['cat']) && $_GET['cat'] == $cat['id']) ? ' style="font-weight:bold"' : '';

            $show .= ($level !=0 && $i == 0) ? "<li class=\"sliz\"></li>" : '';
            $show .= "<li{$liClass}><section {$section}{$style}><a href=\"{$config['home']}goods.php?cat={$cat['id']}\" {$onclick}>{$cat['name']}</a></section><section{$numClass}><a href=\"goods.php?cat={$cat['id']}\">{$cat['num']}</a></section>";
            $show .= $cats."</li>";
            $i++;
        }
        $show .= '</ul>';
    }
    return $show;
}

function genereateCategories()
{
    $catss = getCats();

    $cats = array();

    foreach($catss as $cat){
        //$cats_ID[$cat['id']][] = $cat;
        $cats[$cat['parent_id']][] =  $cat;
    }

    $catList = outCatsTree($cats, 0, 0);

    return <<<HTML
        <section class="leftMenuText">
          <section class="textMenuLeft">Это все у нас есть!</section>
        </section>
        <section style="margin:0 20px 0 20px">
          <section class="menuSimple">
          {$catList}
            </section>
            <img src="img/main.png" alt="img" style="width:225px; height350px; margin:40px 0 40px 0;"  />
          </section>
HTML;

}

?>