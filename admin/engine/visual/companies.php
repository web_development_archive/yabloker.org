<?php

function companyType($val, $type)
{
    switch($type){
        case "sale":
            if($val == 1){
                return <<<HTML
                <section class="workersRightOnce">
                   <img alt="img" src="/img/dogovor1.png" class="workersRightDogovorImg">
                   <section class="workersRightDogovorText">Продажа</section>
                </section>
HTML;
            }
            break;
        case "buy":
            if($val == 1){
                return <<<HTML
            <section class="workersRightOnce">
                <img alt="img" src="/img/dogovor2.png" class="workersRightDogovorImg">
                <section class="workersRightDogovorText">Покупка</section>
            </section>
HTML;
            }
            break;
        case "logic":
            if($val == 1){
               return <<<HTML
            <section class="workersRightOnce">
              <img alt="img" src="/img/dogovor3.png" class="workersRightDogovorImg">
              <section class="workersRightDogovorText">Логистика</section>
            </section>
HTML;
            }
            break;
    }
    return null;
}

/*
 * Вывод всех компаний по интересам
 */

function getAllComByInterest($id, $name)
{
    $nameOfInterest = $name;
    $allCom = '';

    $all = getAllComByInt($id);

    if($all == null){
        return '<section class="reviewTovarsText">Компаний с таким интересом нет</section>';
    }else{
        foreach($all as $company)
        {
            $logo = getImageThumb($company['logo'], 225, 130);
            $href = '/company/'.$company['id'];
            $allCom .= <<<HTML
        <a href="{$href}">
            <section class="itemsTovars">
              <img alt="img" src="{$logo}" class="reviewTovarsImg">
              <section class="reviewTovarsText">{$company['title']}</section>
            </section>
        </a>
HTML;
        }
    }

    $content = <<<HTML
    <section style="margin:0 0 0 20px;">
        <section class="reviewRightTitle">
          {$nameOfInterest}
        </section><br>
        <section class="itemsItems">
          {$allCom}
        </section>
      </section>
HTML;
    return $content;
}

/*
 * Вывод всех сотрудников
 */

function showCompany($company)
{

    $goods = getGoodsSite(null, null, $company['id']);
    $tovar = sortGoodsByCats($goods);

    $files = getFiles(0, $company['id']);

    $description = $company['description'];
    $title = $company['title'];

    $sale = companyType($company['sale'], "sale");
    $buy = companyType($company['buy'], "buy");
    $logic = companyType($company['logic'], "logic");

    $user = (isset($_SESSION['user'])) ? $_SESSION['user']['id'] : 0;
    $company_id = $company['id'];

    $fav = getFavorite($user, $company_id);
    $empty = ($fav == false) ? 'favorite-out.png' : 'dogovor4.png';

    return <<<HTML
    <section class="reviewRightTitle">
          {$title}
        </section>
        <section class="reviewLine">
          <section class="reviewRightLine">
            {$sale}
            {$buy}
            {$logic}
            <img alt="img" src="/img/{$empty}" class="reviewRightDogovorImg2" id="addToFavoriteBtn">
            <section class="reviewRightDogovorText2"  onclick="addToFavorites($user,$company_id, 'addToFavoriteBtn');">В избранное</section>
          </section>
          <a href="/im.php" class="btn btn-info reviewRightBtn">
            Договориться
          </a>
        </section>
        <section class="reviewMap"><div id="map" style="width:735px;height:250px"></div></section>
        <!--<section class="reviewRightHeader"></section>-->
        <section class="reviewRightFooter">
            {$description}
        </section>
        <section class="reviewRightFooter">
            <p><b>Регионы:</b> {$company['regions']}</p>
        </section>
        <section class="reviewRightTitle2">
          Дополнительная документация и файлы
        </section>
        <section>
          {$files}
        </section>
        <section class="reviewRightTitle2">
          Предоставляемый товар
        </section>
        {$tovar}
        <script type="text/javascript">
        ymaps.ready(initMap);
        var myMap;

        function initMap(){
            myMap = new ymaps.Map("map", {
                center: [{$company['lat']}, {$company['lng']}],
                zoom: 13
            });

            myPlacemark = new ymaps.Placemark([{$company['lat']}, {$company['lng']}], {
                hintContent: 'Мы здесь'
            });

            myMap.geoObjects.add(myPlacemark);
        }
        </script>
HTML;

}

function showCompaniesShortForm($comps)
{
    $show = '';
    foreach($comps as $company)
    {
        $logo = (!is_null($company['logo'])) ? getImageThumb($company['logo'], 225, 130) : 'http://placehold.it/225x130';
        $href = '/company/'.$company['id'];
        $show .= <<<HTML
            <section class="itemsTovars">
              <a href="$href"><img alt="img" src="{$logo}" class="reviewTovarsImg"></a>
              <a href="$href"><section class="reviewTovarsText">{$company['title']}</section></a>
            </section>
HTML;
    }
    return $show;
}

function getFiles($table, $id)
{
    global $config;
    $files = getAttachments($table, $id);
    $text ='';

    if(count($files)>0)
    {
        foreach($files as $file)
        {
        $path = $config['home'].$config['uploads'];
        $filename = $path.$file['title'];
        $fileType = filetype($filename);

    $text .= <<<HTML
        <section class="reviewFile">
            <img alt="img" src="/img/file1.png" class="reviewFileImg">
            <section class="reviewFileRight">
              <section class="reviewFileTitle"><a href="{$filename}">{$file['title']}</a></section>
              <section class="reviewFileSize">{$fileType}, {filesize($filename)}</section>
            </section>
        </section>

HTML;
        }
    }elseif(count($files)<=0)
    {
        $text .= <<<HTML
              <section class="reviewFileSize">Файлов нет</section>
HTML;
    }

    return $text;
}

// Сортировка товаров по категориям
function sortGoodsByCats($goods)
{
    global $config;
    $show = '';

    if(count($goods) == 0) return '<div class="reviewFileSize">Товаров нет</div>';

    $cats = array();
    foreach($goods as $good)
    {
        $cats[$good['cat_id']]['title'] = $good['cat'];
        $cats[$good['cat_id']]['goods'][] = $good;
    }

    foreach($cats as $cat)
    {
        $show .= '<section><section class="reviewWhat">'.$cat['title'].'</section><section class="reviewRightNumbers">'.count($cat['goods']).'</section></section>';
        foreach($cat['goods'] as $good){
            $img = ($good['image_id'] == 0) ? 'http://placehold.it/225x130' : getImageThumb($good['image'], 225, 130);
            $show .= <<<HTML
            <section class="reviewTovars">
              <img alt="img" src="{$img}" class="reviewTovarsImg">
              <section class="reviewTovarsText"><a href="{$config['home']}good/{$good['id']}">{$good['title']}</a></section>
            </section>
HTML;
        }
    }
    return $show;
}

function generateCompanyLeftBlock($company_id)
{
    $company = getCompany($company_id);

    $logo = (!is_null($company['logo'])) ? getImageThumb($company['logo'], 225, 130) : 'http://placehold.it/225x130';

    $contacts = <<<HTML
            <section class="reviewLeftText">
              Вы не может увидеть контакты этой компании пока не зарегистрируетесь
            </section>
            <a href="/login.php?action=showSignUp" target="_blank" class="btn btn-info reviewLeftReg">
              Зарегистрироваться
            </a>
HTML;
    if(isset($_SESSION['user'])){
        $contacts = <<<HTML
            <section class="reviewLeftText">
            <dl>
              <dt>Skype</dt>
              <dd>{$company['skype']}</dd>
              <dt>Email</dt>
              <dd>{$company['com_mail']}</dd>
              <dt>Телефон</dt>
              <dd>{$company['phone']}</dd>
              <dt>Адрес</dt>
              <dd>{$company['address']}</dd>
            </dl>
            </section>
HTML;
    }

    $more = (!isset($_GET['company_id'])) ? '<section style="padding-top:10px;"><a style="display:block" href="/company/'.$company_id.'" class="btn btn-default">Подробнее</a></section>' : '';
    $user = $_SESSION['user']['id'];
    $fav = (!isset($_GET['company_id'])) ? '<img alt="img" src="/img/dogovor4.png" class="itemLeftImg"><section class="itemLeftText2" onclick="addToFavorites('.$user.','.$company_id.');">В избранное</section>' : '';

    return <<<HTML
    <section style="margin:0 20px 0 20px">
          <h4>{$company['title']}</h4>
          <img alt="img" src="{$logo}" class="previewTitle">
          {$more}
          <section class="previewLeftTitle">Контакты</section>
          <section class="reviewSliz"></section>
          <section class="reviewLeftNoAccess">
          {$contacts}
          </section>
          <section class="previewLeftTitle">Интересует</section>
          {$company['interests']}
          <section class="itemLeftLine">
            <section class="itemLeftSec2">{$fav}</section>
          </section>
        </section>
HTML;

}

function showSignUpCompany()
{
    $errors = getMiniErrors(); // Маленькие системные сообщения

    if($errors != false){
        $errors = <<<HTML
        <section class="regUserAhtung">
            <section class="regUserAhtungHeader">Ошибки!</section>
            <section class="regUserAhtungFooter">
              {$errors}
            </section>
          </section>
HTML;
    }else $errors = '';

    $rArrays = getRegions();
    $regions = '';
    if(count($rArrays) > 0){
        foreach($rArrays as $reg) $regions .= "<option value=\"{$reg['id']}\">{$reg['name']}</option>";
    }

    return <<<HTML
    <section class="regUserTop">
        <section class="regUserTopText">Регистрация компании</section>
        <!--<a href="" class="regUserTopCom">Зарегистрировать пользователя</a>-->
      </section>
      <form action="/login.php?action=signUpCompany" method="post" enctype="multipart/form-data">
        <section class="regComCol1">
          <img alt="img" id="userImage" src="http://placehold.it/225x130" class="regUserLogo">
          <input type="file" value="Загрузить фото" onchange="readURL(this);" name="logotype" id="fileUpload" class="regUserBtnPhoto btn btn-info" style="display:none">
          <section class="btn btn-info regUserBtnPhoto" id="uploadHandler">Загрузить логотип</section>
          <!--<section class="regUserFirst">
            <section class="regUserFirstX">x</section>
            <section class="regUserFirstDelete">Удалить фото</section>
          </section>-->
        </section>
        <section class="regComCol2">
          <label class="regUserLabel">Название компании</label>
          <input type="text" id="regComInput" value="{$_SESSION['regTemp']['title']}" name="title">
          <section class="regComPodCol1">
            <label class="regUserLabel">Телефон</label>
            <input type="text" name="phone" value="{$_SESSION['regTemp']['phone']}" id="regComInput2">
          </section>
          <section class="regComPodCol2">
            <label class="regUserLabel">Skype</label>
            <input type="text" name="skype" value="{$_SESSION['regTemp']['skype']}" id="regComInput2_1">
          </section>
          <section class="regComPodCol1">
            <label class="regUserLabel">Элестронная почта</label>
            <input type="text" name="com_mail" value="{$_SESSION['regTemp']['com_mail']}" id="regComInput2_2">
          </section>
          <section class="regComPodCol2">
            <label class="regComLabel" style="margin-top:0">Тип бизнеса</label>
              <select class="regComSlt" name="business_type">
                <option value="0">Организация</option>
                <option value="1">Частный предприниматель</option>
                <option value="2">Неоф. бизнес</option>
              </select>
          </section>
          <div>
            <label class="regUserLabel">Адрес</label>
             <input type="text" name="address" value="{$_SESSION['regTemp']['address']}" id="address" class="regInput" style="margin: 0 0 20px 0;background: #ddebf1;width: 470px;height: 30px;" />
          </div>
          <label class="regUserLabel">Метоположение</label>
          <div id="map" style="width: 484px; height: 200px"></div>
          <input type="hidden" name="lat" value="{$_SESSION['regTemp']['lat']}" id="lat" />
          <input type="hidden" name="lng" value="{$_SESSION['regTemp']['lng']}" id="lng" />
          <label class="regComLabel">Сфера занятости компании</label>
          <section>
            <section class="regComThreeBtn" onclick="return toggleWorking(1);">
              <img alt="img" src="/img/dogovor1.png" class="regComThreeBtnImg">
              <label class="regComThreeBtnLbl">
                Продажа
              </label>
              <input type="checkbox" name="sale" class="working1" style="display: none" value="1" />
            </section>
            <section class="regComThreeBtn" onclick="return toggleWorking(2);">
              <img alt="img" src="/img/dogovor2.png" class="regComThreeBtnImg">
              <label class="regComThreeBtnLbl">
                Покупка
              </label>
              <input type="checkbox" name="buy" class="working2" style="display: none" value="1" />
            </section>
            <section class="regComThreeBtn" onclick="return toggleWorking(3);">
              <img alt="img" src="/img/dogovor3.png" class="regComThreeBtnImg">
              <label class="regComThreeBtnLbl">
                Логистика
              </label>
              <input type="checkbox" name="logic" class="working3" style="display: none" value="1" />
            </section>
          </section>
          <label class="regComLabel">Регионы</label>
          <section>
            <select id="regComInputRegion">
                {$regions}
            </select>
            <section class="btn btn-info regComBtnRegion" onclick="return newRegion()" style="padding:4px 0">Добавить рагион</section>
          </section>
          <section id="regionArea"></section>
          <label class="regComLabel">Описание компании</label>
          <textarea rows="4" cols="50" name="description" class="regComTxtArea">{$_SESSION['regTemp']['description']}</textarea>
          <input type="submit" class="btn btn-info regUserBtnPhoto" style="padding:10px" name="sub" value="Зарегистрироваться" />
        </section>
        <section class="regComCol3">
          {$errors}
          <section class="regUserWhy">
            <section class="regUserAhtungHeader">Зачем регистрироваться?</section>
            <section class="regUserWhyFooter">
              Не следует, однако забывать, что реализация намеченных плановых заданий способствует подготовки и реализ ации существенных финансовых и административных условий. Не следует, однако забывать, что начало повседневной работы по формированию позиции обеспечивает широкому кругу (специалистов) участие в формировании новых предложений.
            </section>
          </section>
        </section>
      </form>
HTML;

}

?>