<?php
	include "core.php"; // Тут все функции движка — общие для админки и для сайта
	include "visual.php"; // Тут функции, отвечающий за вывод на экран — касается только админки

	if($_REQUEST['authorize']==1) // Если пользователь пытается залогиниться
	{
		$phone = $_REQUEST['phone'];
		$pass = $_REQUEST['pass'];
		
		$user = engineLoginAttempt($phone, $pass); // возвращает либо FALSE либо все данные пользователя, взятые из БД
		if($user !== FALSE) // Если пользователь ввел правильные логин и пароль
		{
			$_SESSION['user'] = $user; // сохраняем все его данные в сессию — потом пригодятся
		}
	}


	if(!isset($_SESSION['user']))  // Обрубаем незалогиненых чуваков
	{
		die(masterRender("Вход в админстраторскую панель",renderAuth(),0));
	}

?>