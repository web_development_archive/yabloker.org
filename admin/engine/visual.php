<?php

include_once "visual/posts.php";
include_once "visual/users.php"; // Функций вывода связанные с пользователями


/*
 * Текстовые блоки (соглашение, услуги)
 */

function showTexts()
{
    $texts = getTexts();
    if(count($texts) == 0) return 'Ничего не найдено';

    $show = '<table class="table table-bordered table-clickable"><tr><th>ID</th><th>Наименование</th></tr>';
    foreach($texts as $text){
        $show .= "<tr onclick=\"window.location.href='texts.php?action=edit&text_id={$text['id']}'\"><td>{$text['id']}</td><td>{$text['name']}</td></tr>";
    }
    $show .= '</table>';

    return $show;
}

function editTextForm($id)
{
    $text = getTextOne($id);

    return <<<HTML
    <form action="texts.php?action=doEdit" method="post">
        <div class="form-group">
            <label>Название</label>
            <input type="text" name="name" value="{$text['name']}" class="form-control" />
        </div>
        <div class="form-group">
            <label>Текст</label>
            <textarea name="txt" class="form-control" id="fields"  rows="7">{$text['txt']}</textarea>
        </div>
        <div class="form-group">
            <input type="hidden" name="text_id" value="{$text['id']}" />
            <input type="submit" value="Редактировать" class="btn btn-default btn-sm" name="sub" />
        </div>
    </form>
HTML;

}

/*
 * Adverts
 */

function showAdverts()
{
    include_once ROOT."/admin/engine/classes/Pagination/Pagination.class.php";
    global $config;

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $adverts = getAdverts($start, $limit);

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    if(count($adverts) == 0) return 'Ничего не найдено';

    $show = <<<HTML
    <div class="panel panel-primary">
        <div class="panel-heading">Управление объявлениями контексной рекламы</div>
        <table class="table table-bordered">
        <tr><th>Фото</th><th>Название</th><th>Статус</th><th>Дата создания</th><th>Старт</th><th>Конец</th><th>Создал</th></tr>
HTML;

    foreach($adverts as $adv){
        $m[$adv['status']] = ' selected';
        $status = <<<HTML
        <form action="adverts.php?action=changeStatus" class="form-inline" method="post">
            <input type="hidden" name="advert_id" value="{$adv['id']}" />
            <select name="status" class="form-control input-sm">
                <option value="0"{$m[0]}>Проходит модерацию</option>
                <option value="1"{$m[1]}>Модерация пройдена</option>
                <option value="2"{$m[2]}>Модерация не пройдена</option>
            </select>
            <input type="submit" name="sub" value="Сменить" class="btn btn-default btn-sm" />
        </form>
HTML;

        $show .= '<tr><td><img src="'.$adv['filename'].'" width="30" /></td><td>'.$adv['title'].'</td><td>'.$status.'</td><td>'.$adv['dt_created'].'</td><td>'.$adv['dt_start'].'</td><td>'.$adv['dt_finish'].'</td><td><a href="index.php?action=edit&user_id='.$adv['user_id'].'">'.$adv['name'].'</a></td></tr>';
    }
    $show .= '</table>';
    if(!empty($markup)) $show .= '<div class="panel panel-footer">'.$markup.'</div>';
    $show .= '</div>';

    return $show;
}

/*
 * Deals
 */

function showDeals()
{
    include_once ROOT."/admin/engine/classes/Pagination/Pagination.class.php";
    global $config;

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $deals = getDeals($start, $limit);

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    if(count($deals) == 0) return 'Ничего не найдено';

    $show = '<div class="panel panel-info"><div class="panel-heading">Сделки</div><table class="table table-bordered table-clickable"><tr><th>Тип</th><th>Товар</th><th>Между</th><th>Дата сделки</th><th>Дата отгрузки</th><th>Статус</th></tr>';

    foreach($deals as $d){
        switch($d['type']){
            case 0: $d['type'] = 'Покупка'; break;
            case 1: $d['type'] = 'Продажа'; break;
            case 2: $d['type'] = 'Логистика'; break;
        }
        switch($d['status']){
            case 0: $d['status'] = 'Ожидает выполнения'; break;
            case 1: $d['status'] = 'Завершен'; break;
            case 2: $d['status'] = 'Отменена'; break;
        }
        $show .= '<tr onclick="document.location.href=\'deals.php?action=edit&deal_id='.$d['id'].'\'"><td>'.$d['type'].'</td><td>'.$d['good'].'</td><td><a href="index.php?action=edit&user_id='.$d['first'].'">'.$d['firstName'].'</a> - <a href="index.php?action=edit&user_id='.$d['second'].'">'.$d['secondName'].'</a></td><td>'.$d['make_dt'].'</td><td>'.$d['shipment_dt'].'</td><td>'.$d['status'].'</td>';
    }
    $show .= '</table></div>';

    return $show;
}

function editDealForm($id)
{
    $deal = getDeal($id);

    $users = getUsers(null, null);
    $goods = getGoods(null, null);

    $type[$deal['type']] = ' selected="selected"';

    $first = '';
    $second = '';
    foreach($users as $u){
        $a1 = ($u['id'] == $deal['first']) ? ' selected="selected"' : '';
        $a2 = ($u['id'] == $deal['second']) ? ' selected="selected"' : '';

        $first .= '<option value="'.$u['id'].'"'.$a1.'>'.$u['name'].'</option>';
        $second .= '<option value="'.$u['id'].'"'.$a2.'>'.$u['name'].'</option>';
    }

    $good = '';
    foreach($goods as $g){
        $a = ($g['id'] == $deal['good_id']) ? ' selected="selected"' : '';
        $good .= '<option value="'.$g['id'].'"'.$a.'>'.$g['title'].'</option>';
    }
    $status[$deal['status']] = ' selected';

    return <<<HTML
    <h2>Редактирование сделки</h2>
    <form action="deals.php?action=doEdit" method="post">
        <div class="row">
            <div class="col-sm-4">
                <label>Тип</label>
                <select name="type" class="form-control">
                    <option value="0"{$type[0]}>Покупка</option>
                    <option value="1"{$type[1]}>Продажа</option>
                    <option value="2"{$type[2]}>Логистика</option>
                </select>
            </div>
            <div class="col-sm-4">
                <label>Первый пользователь</label>
                <select name="first" class="form-control">
                    {$first}
                </select>
            </div>
            <div class="col-sm-4">
                <label>Второй пользователь</label>
                <select name="second" class="form-control">
                    {$second}
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <label>Товар</label>
                <select name="good_id" class="form-control">
                    {$good}
                </select>
            </div>
            <div class="col-sm-4">
                <label>Дата сделки</label>
                <input type="text" name="make_dt" value="{$deal['make_dt']}" class="form-control datetimepicker" />
            </div>
            <div class="col-sm-4">
                <label>Дата отгрузки</label>
                <input type="text" name="shipment_dt" value="{$deal['shipment_dt']}" class="form-control datetimepicker" />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <label>Статус</label>
                <select name="status" class="form-control">
                    <option value="0"{$status[0]}>Ожидает выполнения</option>
                    <option value="1"{$status[1]}>Завершен</option>
                    <option value="2"{$status[2]}>Отменена</option>
                </select>
            </div>
            <div class="col-sm-7">
                <label>Комментарий</label>
                <textarea class="form-control" name="comment">{$deal['comment']}</textarea>
            </div>
        </div>
        <div class="row">
            <input type="hidden" name="deal_id" value="{$deal['id']}" />
            <input type="submit" name="sub" class="btn btn-default" />
        </div>
    </form>
HTML;

}

/*
 * Services
 */

function showServices()
{
    $services = getServices();

    $show = '<div class="panel panel-default"><div class="panel-heading">Управление ценами платных услуг</div><table class="table table-bordered table-clickable"><tr><th>Название</th><th>Цена</th></tr>';

    foreach($services as $s){
        $show .= '<tr onclick="document.location.href=\'services.php?action=edit&ser_id='.$s['id'].'\'"><td>'.$s['name'].'</td><td>'.$s['price'].' руб</td><td><a href="services.php?action=delete&ser_id='.$s['id'].'"><i class="glyphicon glyphicon-remove"></i></a></td></tr>';
    }
    $show .= '</table></div>';
    return $show;
}

function editServiceForm($id)
{
    $service = getService($id);

    return <<<HTML
    <form action="services.php?action=doEdit" method="post">
        <div class="row">
            <div class="col-sm-8">
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="name" class="form-control" value="{$service['name']}" />
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Цена</label>
                    <input type="text" name="price" class="form-control" value="{$service['price']}" />
                </div>
            </div>
        </div>
        <div class="row">
            <input type="hidden" name="ser_id" value="{$service['id']}" />
            <input type="submit" value="Отправить" class="btn btn-default" name="sub" />
        </div>
    </form>
HTML;

}

/*
 * Companies
 */

function editCompanyForm($id)
{
    global $config;
    $c = getCompany($id);

    $b = array();
    $b[$c['business_type']] = ' selected';

    $c['logotype'] = (!empty($c['logotype'])) ? $c['logotype'] : 0;

    $c['logo'] = (empty($c['logo'])) ? 'http://placehold.it/150x150' : $config['home'].$config['thumbs'].$c['logo'];

    $buy = ($c['buy'] == 1) ? 'checked' : '';
    $sale = ($c['sale'] == 1) ? 'checked' : '';
    $logic = ($c['logic'] == 1) ? 'checked' : '';

    return <<<HTML
    <h2>Редактирование компании</h2>
        <form action="companies.php?action=doEdit" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form-group">
                                    <label>Наименование</label>
                                    <input type="text" name="title" value="{$c['title']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>Загрузить логотип</label>
                                    <input type="file" name="logotype" />
                                </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Skype</label>
                                <input type="text" name="skype" value="{$c['skype']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" name="com_mail" value="{$c['com_mail']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Телефон</label>
                                <input type="text" name="phone" value="{$c['phone']}" class="form-control input-sm" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Адрес</label>
                                <input type="text" name="address" id="address" value="{$c['address']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Тип</label>
                                <select name="business_type" class="form-control input-sm">
                                    <option value="0"{$b[0]}>Организация</option>
                                    <option value="1"{$b[1]}>Частный предприниматель</option>
                                    <option value="2"{$b[2]}>Неофицальный бизнес</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-lg-4 col-sm-4">
                    <div class="form-group">
                        <label>Описание</label>
                        <textarea name="description" rows="8" class="form-control input-sm">{$c['description']}</textarea>
                    </div>
                </div>
                <div class="col-lg-2 col-lg-2 col-sm-2">
                    <img src="{$c['logo']}" />
                </div>
            </div>
            <div class="row">
                <input type="hidden" name="old_logo" value="{$c['logotype']}" />
                <input type="hidden" name="lat" id="lat" value="{$c['lat']}" />
                <input type="hidden" name="lng" id="lng" value="{$c['lng']}" />
                <input type="hidden" name="company_id" value="{$c['id']}" />
                <div class="row" style="width:360px">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label><input type="checkbox" value="1" name="sale" {$sale} /> Продажа</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label><input type="checkbox" value="1" name="buy" {$buy} /> Покупка</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label><input type="checkbox" value="1" name="logic" {$logic} /> Логистика</label>
                        </div>
                    </div>
                </div>
                <input type="submit" name="sub" value="Редактировать" class="btn btn-default" />
            </div>
        </form>
        <script>
			    $(document).ready(function(){
			        $('#address').change(function(){
			            var val = $(this).val();
                        val = val.replace(/\s/g, '+');

			            $.ajax({
			                type: 'get',
			                url: 'ajax.php?action=address',
			                data: {address: 'http://maps.googleapis.com/maps/api/geocode/json?address='+val+'&sensor=false&language=ru'},
			                success: function(data){
			                    var js = $.parseJSON(data);

			                    $('#lat').val(js.results[0].geometry.location.lat);
			                    $('#lng').val(js.results[0].geometry.location.lng);
			                }
			            });
			        });
			    });

        </script>
HTML;

}

function showAllCompanies()
{
    global $config;
    include_once ROOT."/admin/engine/classes/Pagination/Pagination.class.php";

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $comps = getCompanies($start, $limit);

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    if(count($comps) == 0) return 'Ничего не найдено';

    $show = '<div class="panel panel-default"><div class="panel-heading">Управление компаниями</div><table class="table table-bordered table-clickable"><tr><th>Название</th><th>Телефон</th><th>Email</th><th>Skype</th><th>Тип</th><th>Адрес</th></tr>';

    foreach($comps as $c){
        switch($c['business_type']){
            case 0: $type = 'Организация'; break;
            case 1: $type = 'Частный предприниматель'; break;
            case 2: $type = 'Неофицальный бизнес'; break;
        }
        $show .= '<tr onclick="window.location.href=\'companies.php?action=edit&company_id='.$c['id'].'\'"><td>'.$c['title'].'</td><td>'.$c['phone'].'</td><td>'.$c['com_mail'].'</td><td>'.$c['skype'].'</td><td>'.$type.'</td><td>'.$c['address'].'</td><td><a href="companies.php?action=delete&company_id='.$c['id'].'"><i class="glyphicon glyphicon-remove"></i></a></td></tr>';
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= '</table>'.$markup;

    return $show;
}

/*
 * Категории
 */

function showAllCats()
{
    global $config;
    include_once ROOT."/admin/engine/classes/Pagination/Pagination.class.php";

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $users = getCats($start, $limit);

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    if(count($users) == 0) return 'Ничего не найдено'; // Если нет строк

    $menu = <<<HTML
    <nav class="navbar navbar-default" role="navigation">
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li><a href="goods.php?action=toModerate">Товары для модерации</a></li>
        <li class="active"><a href="cats.php">Категории</a></li>
        <li><a href="cats.php?action=new">Добавить категорию</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
</nav>
HTML;


    $show = '<div class="panel panel-info"><div class="panel-heading">Управление категориями</div>'.$menu.'<table class="table table-bordered table-clickable"><tr><th>Название</th><th>Родитель</th></tr>';
    // Разбор массива пользователей
    foreach($users as $u)
    {
        if(empty($u['parent'])) $u['parent'] = 'Отсутствует';
        $show .= "<tr onclick=\"window.location.href='cats.php?action=edit&cat_id={$u['id']}'\"><td>{$u['name']}</td><td>{$u['parent']}</td><td><a href=\"cats.php?action=delete&cat_id={$u['id']}\"><i class='glyphicon glyphicon-remove'></i></a></td>";
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= "</table>".$markup;
    return $show;

}

function editCatForm($id)
{
    $cat = getCat($id);

    $act = array();
    $act[$cat['parent_id']] = ' selected';

    $cats = getCats();
    $options = '';

    foreach($cats as $c) $options .= '<option value="'.$c['id'].'"'.$act[$c['id']].'>'.$c['name'].'</option>';
    unset($cats);

    return <<<HTML
    <form action="cats.php?action=doEdit" method="post">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="name" value="{$cat['name']}" class="form-control input-sm" />
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="form-group">
                    <label>Родитель</label>
                    <select name="parent_id" class="form-control input-sm">
                        <option value="0"{$act[0]}> Родитель отсутствует</option>
                        {$options}
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <input type="hidden" name="cat_id" value="{$cat['id']}" />
            <input type="submit" class="btn btn-default" value="Редактировать" />
        </div>
    </form>
HTML;

}

function addCatForm()
{

    $cats = getCats();
    $options = '';

    foreach($cats as $c) $options .= '<option value="'.$c['id'].'">'.$c['name'].'</option>';
    unset($cats);

    return <<<HTML
    <form action="cats.php?action=doAdd" method="post">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="name" value="" class="form-control input-sm" />
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="form-group">
                    <label>Родитель</label>
                    <select name="parent_id" class="form-control input-sm">
                        <option value="0"> Родитель отсутствует</option>
                        {$options}
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <input type="submit" class="btn btn-default" value="Добавить" />
        </div>
    </form>
HTML;

}

/*
 * Goods
 */

function showAllGoods()
{
    global $config;
    include_once ROOT."/admin/engine/classes/Pagination/Pagination.class.php";

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $users = getGoods($start, $limit, " ORDER BY g.id DESC");

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    // End pagination

    if(count($users) == 0) return 'Ничего не найдено'; // Если нет строк

    $menu = <<<HTML
    <nav class="navbar navbar-default" role="navigation">
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li><a href="goods.php?action=toModerate">Товары для модерации</a></li>
        <li><a href="cats.php">Категории</a></li>
        <li><a href="goods.php?action=new">Добавить товар</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
    </nav>
HTML;


    $show = '<div class="panel panel-info"><div class="panel-heading">Управление товарами</div>'.$menu.'<table class="table table-bordered table-clickable"><tr><th>Название</th><th>Цена</th><th>Компания</th><th>Категория</th></tr>';
    // Разбор массива пользователей
    foreach($users as $u)
    {
        $show .= "<tr onclick=\"window.location.href='goods.php?action=edit&good_id={$u['id']}'\"><td>{$u['title']}</td><td>{$u['price']}</td><td>{$u['company']}</td><td>{$u['cat']}</td><td><a href=\"goods.php?action=delete&good_id={$u['id']}\"><i class='glyphicon glyphicon-remove'></i></a></td>";
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= "</table>".$markup;
    return $show;
}

function showGoodsToModerate()
{
    global $config;
    include_once ROOT."/admin/engine/classes/Pagination/Pagination.class.php";

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $users = getGoods($start, $limit, ' WHERE g.cat_id = 1');

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    // End pagination

    if(count($users) == 0) return 'Ничего не найдено'; // Если нет строк

    $menu = <<<HTML
    <nav class="navbar navbar-default" role="navigation">
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="goods.php?action=toModerate">Товары для модерации</a></li>
        <li><a href="cats.php">Категории</a></li>
        <li><a href="goods.php?action=new">Добавить товар</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
</nav>
HTML;

    $show = '<div class="panel panel-info"><div class="panel-heading">Управление товарами для модерации</div>'.$menu.'<table class="table table-bordered table-clickable"><tr><th>Название</th><th>Цена</th><th>Компания</th><th>Категория</th></tr>';
    // Разбор массива пользователей
    foreach($users as $u)
    {
        $show .= "<tr onclick=\"window.location.href='goods.php?action=edit&good_id={$u['id']}'\"><td>{$u['title']}</td><td>{$u['price']}</td><td>{$u['company']}</td><td>{$u['cat']}</td><td><a href=\"goods.php?action=delete&good_id={$u['id']}\"><i class='glyphicon glyphicon-remove'></i></a></td>";
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= "</table>".$markup;
    return $show;
}

function editGoodForm($id)
{
    $good = getGood($id);

    $cSelect = '<select name="company_id" class="form-control input-sm">';
    $comps = getCompanies();

    foreach($comps as $c){
        $active = ($c['id'] == $good['company_id']) ? ' selected' : '';
        $cSelect .= '<option value="'.$c['id'].'"'.$active.'>'.$c['title'].'</option>';
    }
    $cSelect .= '</select>';
    
    $c1Select = '<select name="cat_id" class="form-control input-sm">';

    unset($comps);

    $cats = getCats();

    foreach($cats as $c){
        $active = ($c['id'] == $good['cat_id']) ? ' selected' : '';
        $c1Select .= '<option value="'.$c['id'].'"'.$active.'>'.$c['name'].'</option>';
    }
    $c1Select .= '</select>';

    unset($cats);

    return <<<HTML
    <h2>Редактирование товара</h2>
        <form action="goods.php?action=doEdit" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7">
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="form-group">
                                <label>Название</label>
                                <input type="text" name="title" value="{$good['title']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <label>Загузить фото</label>
                            <input type="file" name="image" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Компания</label>
                                {$cSelect}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Категория</label>
                                {$c1Select}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Цена</label>
                                <input type="text" name="price" value="{$good['price']}" class="form-control input-sm" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-lg-5 col-sm-5">
                    <div class="form-group">
                        <label>Описание</label>
                        <textarea name="description" rows="5" class="form-control input-sm">{$good['description']}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <input type="hidden" name="good_id" value="{$good['id']}" />
                <input type="hidden" name="old_image" value="{$good['image_id']}" />
                <input type="submit" name="sub" value="Редактировать" class="btn btn-default" />
                <a href="goods.php?action=images&good_id={$good['id']}" class="btn btn-info">Картинки</a>
            </div>
        </form>
HTML;

}

function showGoodImages($id)
{
    $iArray = getGoodImages($id);
    if(count($iArray) == 0) $images = 'Фотографий отсутствуют';
    else{
        $images = '<ul id="goodImages">';
        foreach($iArray as $img)
        {
            $url = getImageThumb($img['filename']);
            $images .= <<<HTML
            <li><img src="{$url}" /><a href="goods.php?action=deleteImage&image_id={$img['id']}&good_id={$id}">X</a></li>
HTML;
        }
        $images .= '</ul>';
    }

    return <<<HTML
    <div class="row">
        <h2>Фотографий / <a href="goods.php?action=edit&good_id={$id}"><small>Назад</small></a></h2>
    </div>
    <div class="row">
        <div class="col-sm-7">
            {$images}
        </div>
        <div class="col-sm-5">
            <form action="goods.php?action=uploadImage" method="POST" enctype="multipart/form-data">
                <input type="file" name="image" />
                <input type="hidden" name="good_id" value="{$id}" />
                <input type="submit" value="Загрузить" name="subImage" />
            </form>
        </div>
    </div>
HTML;

}

function addGoodForm()
{

    $cSelect = '<select name="company_id" class="form-control input-sm">';
    $comps = getCompanies();

    foreach($comps as $c){
        $cSelect .= '<option value="'.$c['id'].'">'.$c['title'].'</option>';
    }
    $cSelect .= '</select>';

    $c1Select = '<select name="cat_id" class="form-control input-sm">';

    unset($comps);

    $cats = getCats();

    foreach($cats as $c){
        $c1Select .= '<option value="'.$c['id'].'">'.$c['name'].'</option>';
    }
    $c1Select .= '</select>';

    unset($cats);

    return <<<HTML
    <h2>Добавление товара</h2>
        <form action="goods.php?action=doAdd" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7">
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="form-group">
                                <label>Название</label>
                                <input type="text" name="title" value="" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <label>Загузить фото</label>
                            <input type="file" name="image" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Компания</label>
                                {$cSelect}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Категория</label>
                                {$c1Select}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Цена</label>
                                <input type="text" name="price" value="" class="form-control input-sm" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-lg-5 col-sm-5">
                    <div class="form-group">
                        <label>Описание</label>
                        <textarea name="description" rows="5" class="form-control input-sm"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <input type="submit" name="sub" value="Добавить" class="btn btn-default" />
            </div>
        </form>
HTML;

}

/*
 * Пользователи
 */

function showUsers()
{
    global $config;
    include_once ROOT."/admin/engine/classes/Pagination/Pagination.class.php";

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $users = getUsers($start, $limit);

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    // End pagination

    if(count($users) == 0) return 'Ничего не найдено'; // Если нет строк

    $show = '<div class="panel panel-default"><div class="panel-heading">Управление пользователями / <a href="index.php?action=new">Добавить пользователя</a></div><table class="table table-bordered table-clickable"><tr><th>Nickname</th><th>Группа</th><th>Телефон</th></tr>';
    // Разбор массива пользователей
    foreach($users as $u)
    {
        $show .= "<tr onclick=\"window.location.href='index.php?action=edit&user_id={$u['user_id']}'\"><td>{$u['user_nickname']}</td><td>{$u['type_name']}</td><td>{$u['user_phone']}</td><td><a href=\"index.php?action=delete&user_id={$u['user_id']}\"><i class='glyphicon glyphicon-remove'></i></a></td>";
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= "</table>".$markup;
    return $show;
}

function showAddUserForm()
{
    $groups = getGroups();
    $groupsSelect = '<select name="group_id" class="form-control input-sm"><option>Выберите группу</option>';

    $i = 0;
    foreach($groups as $g) {
        $disable = '';//($g['id'] == 1 || ($_SESSION['user']['addModer'] == 0 && $g['id'] == 2)) ? ' disabled' : '';
        $groupsSelect .= '<option value="'.$g['type_id'].'"'.$disable.'>'.$g['type_name'].'</option>';
    }
    $groupsSelect .= '</select>';

    return <<<HTML
    <h2>Добавление Пользователя</h2>
        <form action="users.php?action=doAdd" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Фамилия</label>
                                <input type="text" name="surname" value="" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Имя</label>
                                <input type="text" name="name" value="" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Отчество</label>
                                <input type="text" name="patro" value="" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Логин</label>
                                <input type="text" name="nickname" value="" class="form-control input-sm" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Город</label>
                                <input type="text" name="city" value="" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Пол</label>
                                <select name="sex" class="form-control input-sm">
                                    <option value="5">Женский</option>
                                    <option value="1">Мужской</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Дата рождения</label>
                                <input type="date" name="birthday" value="" class="datepicker form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Группа</label>{$groupsSelect}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Почта</label>
                                <input type="mail" name="mail" value="" class="datepicker form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Главный телефон</label>
                                <input type="number" name="main_phone" value="" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Примечание</label>
                                <textarea  name="info_others" class="form-control input-sm"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Пароль</label>
                                <input type="text" name="pass" value="" class="form-control input-sm" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
            <input type="submit" name="sub" value="Добавить" class="btn btn-default" />
            </div>
        </form>
HTML;
}

function showUserEditFormSecond($user_id)
{
    $u = getUser($user_id);
    if($u == false) return 'Такого пользователя не существует';

    $groups = getGroups();
    $groupsSelect = '';
    foreach($groups as $g) {

        if($g['type_id'] == $u['type_id'])
        {
            $groupsSelect .= '<div>'.$g['type_name'].'</div>';
        }

    }
    $groupsSelect .= '';

    $sex[$u['info_sex']] = ' selected';
    return <<<HTML
        <h2>Редактирование пользоватея</h2>
        <form action="index.php?action=doEdit" method="post">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Фамилия</label>
                                <input type="text" name="surname" value="{$u['info_familiya']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Имя</label>
                                <input type="text" name="name" value="{$u['info_imya']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Отчество</label>
                                <input type="text" name="patro" value="{$u['info_otchstvo']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Логин</label>
                                <input type="text" name="nickname" value="{$u['user_nickname']}" class="form-control input-sm" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Город</label>
                                <input type="text" name="city" value="{$u['info_address']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Пол</label>
                                <select name="sex" class="form-control input-sm">
                                    <option value="5"{$sex[5]}>Женский</option>
                                    <option value="1"{$sex[1]}>Мужской</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Дата рождения</label>
                                <input type="date" name="birthday" value="{$u['info_bd']}" class="datepicker form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Пароль</label>
                                <input type="text" name="user_password" value="{$u['user_password']}" class="datepicker form-control input-sm" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Почта</label>
                                <input type="mail" name="mail" value="{$u['info_mail']}" class="datepicker form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Главный телефон</label>
                                <input type="number" name="main_phone" value="{$u['user_phone']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Примечание</label>
                                <textarea  name="info_others" class="form-control input-sm">{$u['info_others']}</textarea>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <input type="hidden" name="old_group" value="{$u['type_id']}" />
                                <input type="hidden" name="user_id" value="{$u['user_id']}" />
                                <input type="submit" name="sub" value="Редактировать" class="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

            </div>
        </form>
HTML;

}

function showUserEditForm($user_id)
{
    $u = getUser($user_id);
    if($u == false) return 'Такого пользователя не существует';

    $groups = getGroups();
    $groupsSelect = '<select name="group_id" class="form-control input-sm">"';
    foreach($groups as $g) {
        $active = ($g['type_id'] == $u['type_id']) ? ' selected' : '';
        $disable = '';//($g['id'] == 1 || ($_SESSION['user']['addModer'] == 0 && $g['id'] == 2)) ? ' disabled' : '';
        $groupsSelect .= '<option value="'.$g['type_id'].'"'.$active.$disable.'>'.$g['type_name'].'</option>';
    }
    $groupsSelect .= '</select>';

    $sex[$u['info_sex']] = ' selected';
    return <<<HTML
        <h2>Редактирование пользоватея</h2>
        <form action="users.php?action=doEdit" method="post">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Фамилия</label>
                                <input type="text" name="surname" value="{$u['info_familiya']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Имя</label>
                                <input type="text" name="name" value="{$u['info_imya']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Отчество</label>
                                <input type="text" name="patro" value="{$u['info_otchstvo']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Логин</label>
                                <input type="text" name="nickname" value="{$u['user_nickname']}" class="form-control input-sm" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Город</label>
                                <input type="text" name="city" value="{$u['info_address']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Пол</label>
                                <select name="sex" class="form-control input-sm">
                                    <option value="5"{$sex[5]}>Женский</option>
                                    <option value="1"{$sex[1]}>Мужской</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Дата рождения</label>
                                <input type="date" name="birthday" value="{$u['info_bd']}" class="datepicker form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Группа</label>{$groupsSelect}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Почта</label>
                                <input type="mail" name="mail" value="{$u['info_mail']}" class="datepicker form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Главный телефон</label>
                                <input type="number" name="main_phone" value="{$u['user_phone']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <label>Примечание</label>
                                <textarea  name="info_others" class="form-control input-sm">{$u['info_others']}</textarea>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="form-group">
                                <input type="hidden" name="old_group" value="{$u['type_id']}" />
                                <input type="hidden" name="user_id" value="{$u['user_id']}" />
                                <input type="submit" name="sub" value="Редактировать" class="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

            </div>
        </form>
HTML;

}

/*
 * Админка
 */

/**
 * All
*/
function showAllFlood()
{
    $content = <<<HTML
    <div class="blocks content_header_text">- Время менять этот мир к лучшему. Начнем с себя -</div>
  <div class="clr30"></div>
  <div>
    <div class="content_1 blocks">
      <div class="content_1_header">
        Сообщения / <div class="content_1_header_new">+1</div>
      </div>
      <div class="content_1_footer">
        <img src="images/admin_sms.png">
      </div>
    </div>
    <div class="clr34"></div>
    <div class="content_1 blocks">
      <div class="content_1_header">
        Написать новый шедевр
      </div>
      <div class="content_1_footer">
        <img src="images/admin_plus.png">
      </div>
    </div>
    <div class="clr34"></div>
    <div class="content_1 blocks">
      <div class="content_1_header">
        Профиль
      </div>
      <div class="content_1_footer">
        <div class="content_1_footer_1">
          <div class="content_1_footer_1_n">312</div>
          <div class="content_1_footer_1_n">1200</div>
          <div class="content_1_footer_1_n">543</div>
        </div>
        <div class="content_1_footer_1">
          <div class="content_1_footer_1_t">статей</div>
          <div class="content_1_footer_1_t">просмотров</div>
          <div class="content_1_footer_1_t">комментария</div>
        </div>
      </div>
    </div>
  </div>
  <div class="clr30"></div>

  <div class="blocks content_header_text">- Мои статьи -</div>
  <div class="clr30"></div>
  <div class="content_table">
    <table class="blocks">
      <tr>
        <th>Статус</th>
        <th>Заголовок</th>
        <th>дата</th>
        <th>категория</th>
        <th>просмотры</th>
      </tr>
      <tr>
        <td><img src="images/status_done.png"></td>
        <td>Новый взгляд на продуктивность, или Что не так с нашей работой</td>
        <td>25 Янвая 2015</td>
        <td> Лайфхаки</td>
        <td>99 921</td>
      </tr>
      <tr>
        <td><img src="images/status_time.png"></td>
        <td>Новый взгляд на продуктивность, или Что не так с нашей работой</td>
        <td>25 Янвая 2015</td>
        <td> Лайфхаки</td>
        <td>99 921</td>
      </tr>
      <tr>
        <td><img src="images/status_x.png"></td>
        <td>Новый взгляд на продуктивность, или Что не так с нашей работой</td>
        <td>25 Янвая 2015</td>
        <td> Лайфхаки</td>
        <td>99 921</td>
      </tr>
    </table>
  </div>
HTML;
    return $content;
}

/**
 * Все пользователи
 */

function showAllUsers()
{
    global $config;
    include_once ROOT."/admin/engine/classes/Pagination/Pagination.class.php";//ROOT.

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $users = getUsers($start, $limit, " ORDER BY u.user_id DESC");

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    // End pagination

    if(count($users) == 0) return 'Ничего не найдено'; // Если нет строк


    $show = '<table class="blocks">
              <tr>
                <th>Логин</th>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Телефон</th>
                <th>Группа</th>
              </tr>';
    // Разбор массива пользователей
    foreach($users as $u)
    {
        $show .= "
<tr onclick=\"window.location.href='users.php?action=edit&user_id={$u['user_id']}'\">
<td>{$u['user_nickname']}</td><td>{$u['info_imya']}</td><td>{$u['info_familiya']}</td><td>{$u['user_phone']}</td><td>{$u['type_name']}</td>";
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= "</table>".$markup;
    return $show;
}

// getPosts
function showAllPosts()
{
    global $config;
    include_once ROOT."/admin/engine/classes/Pagination/Pagination.class.php";//ROOT.

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $users = getPosts($start, $limit);

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    // End pagination


    if(count($users) == 0) return 'Ужас! Постов нет!'; // Если нет строк


    $show = '<table class="blocks">
              <tr>
                <th>Статус</th>
                <th>Заголовок</th>
                <th>дата</th>
                <th>категория</th>
                <th>просмотры</th>
              </tr>';

    /*
     *


     */
    // Разбор массива пользователей
    foreach($users as $u)
    {
        if($u['status_id']==1) $stat = '<img src="images/status_done.png">';
        if($u['status_id']==2) $stat = '<img src="images/status_time.png">';
        if($u['status_id']==3) $stat = '<img src="images/status_x.png">';
        if($u['status_id']==4) $stat = '<img src="images/status_time.png">';
        $show .= "
<tr onclick=\"window.location.href='posts.php?action=edit&post_id={$u['post_id']}'\">
<td>{$stat}</td><td>{$u['post_name']}</td><td>{$u['post_date']}</td>
<td>{$u['cat_name']}</td><td>{$u['post_views']}</td>";
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= "</table>".$markup;
    return $show;
}


/**
 * Вывод страницы админки
 */

function showContentAdmin($user)
{

    $posts = showAllPostsOfUser($user['user_id']);
    $comments = 0;
    $views = getSumOfViewsOfPostsOfUser($user['user_id']);
    $articles = getCountOfPostsOfUser($user['user_id']);

    $html ='<div class="content">
  <div class="blocks content_header_text">- Время менять этот мир к лучшему. Начнем с себя -</div>
  <div class="clr30"></div>
  <div>
    <div class="content_1 blocks">
      <div class="content_1_header">
        Сообщения / <div class="content_1_header_new">+1</div>
      </div>
      <div class="content_1_footer">
        <img src="images/admin_sms.png">
      </div>
    </div>
    <div class="clr34"></div>
    <div class="content_1 blocks">
      <div class="content_1_header">
        Написать новый шедевр
      </div>
      <div class="content_1_footer" onclick="window.location.href=\'posts.php?action=new\'">
        <img src="images/admin_plus.png">
      </div>
    </div>
    <div class="clr34"></div>
    <div class="content_1 blocks">
      <div class="content_1_header">
        Профиль
      </div>
      <div class="content_1_footer">
        <div class="content_1_footer_1">
          <div class="content_1_footer_1_n">'.$articles['count_post'].'</div>
          <div class="content_1_footer_1_n">'.$views['views_post'].'</div>
          <div class="content_1_footer_1_n">'.$comments.'</div>
        </div>
        <div class="content_1_footer_1">
          <div class="content_1_footer_1_t">статей</div>
          <div class="content_1_footer_1_t">просмотров</div>
          <div class="content_1_footer_1_t">комментария</div>
        </div>
      </div>
    </div>
  </div>
  <div class="clr30"></div>

  <div class="blocks content_header_text">- Мои статьи -</div>
  <div class="clr30"></div>

  <div class="content_table">
  '.$posts.'

  </div>
</div>
<div class="clr40"></div>';

    return $html;
}

/**
 * @return Вывод меню для главной в админке для Богов
 */
function showGodContent()
{
    $html='';
    $html .= '
            <div class="blocks content_header_text">- Время менять этот мир к лучшему. Начнем с себя -</div>
          <div class="clr30"></div>
          <div>
            <div class="content_1 blocks content_1_show_first">
              <div class="content_1_header_god">
                Пользователи
              </div>
              <div class="content_1_header_add" onclick="window.location.href=\'users.php?action=new\'">+</div>
            </div>
            <div class="clr34"></div>
            <div class="content_1 blocks content_1_show_second">
              <div class="content_1_header_god">
                Страницы
              </div>
              <div class="content_1_header_add" onclick="window.location.href=\'pages.php?action=new\'">+</div>
            </div>
            <div class="clr34"></div>
            <div class="content_1 blocks content_1_show_third">
              <div class="content_1_header_god">
                Посты
              </div>
              <div class="content_1_header_add" onclick="window.location.href=\'posts.php?action=new\'">+</div>
            </div>
          </div>
          <div class="clr30"></div>
          <div class="blocks content_header_text content_table_users">- Авторы -</div>
          <div class="blocks content_header_text content_table_pages">- Страницы -</div>
          <div class="blocks content_header_text content_table_posts">- Посты -</div>
            <div class="clr30"></div>
          <div class="content_table content_table_users">
            '.showAllUsers().'
          </div>
          <div class="content_table content_table_pages">
    <table class="blocks">
      <tr>
        <th>Статус</th>
        <th>Заголовок</th>
        <th>дата</th>
        <th>категория</th>
        <th>просмотры</th>
      </tr>
      <tr>
        <td><img src="images/status_done.png"></td>
        <td>Новый взгляд на продуктивность, или Что не так с нашей работой</td>
        <td>25 Янвая 2015</td>
        <td> Лайфхаки</td>
        <td>99 921</td>
      </tr>
      <tr>
        <td><img src="images/status_time.png"></td>
        <td>Новый взгляд на продуктивность, или Что не так с нашей работой</td>
        <td>25 Янвая 2015</td>
        <td> Лайфхаки</td>
        <td>99 921</td>
      </tr>
      <tr>
        <td><img src="images/status_x.png"></td>
        <td>Новый взгляд на продуктивность, или Что не так с нашей работой</td>
        <td>25 Янвая 2015</td>
        <td> Лайфхаки</td>
        <td>99 921</td>
      </tr>
    </table>
  </div>
          <div class="content_table content_table_posts">
          '.showAllPosts().'

          </div>
          ';

    return $html;
}

function renderAuth() // окошко авторизации
{

    $alert = '';
    if($_REQUEST['authorize']==1) // Если мы сюда попали с предыдущей попытки авторизации,
    {
        // сообщим пользователю, что он неудачник
        $alert = "
				<script>
					alert('В доступе отказано!')
				</script>";
    }


    $html =<<<HTML

            {$alert}
            <div class="blocks content_header_text">- Время менять этот мир к лучшему. Начнем с себя -</div>
					<div id='authDiv' class="blocks">
					<img src="images/user.png" alt/>
						<h2>Представьтесь:</h2>
						<form method='POST'>
							<input class="form-control input-sm" type='number' name='phone' placeholder='7752963994'/>
							<input class="form-control input-sm" type='password' name='pass' placeholder='Пароль'/>
							<input type='hidden' name='authorize' value='1'>
							<input class="btn btn-default loginAuth" type='submit' value='Войти'>
						</form>
					</div>

HTML;
    return $html;
}

function renderMenu($active)
{

    $highlight = array();
    $highlight[$active] = " class='active'";
    $html = ($_SESSION['user']['usersAccess']) ? '<li'.$highlight[0].'><a href="index.php"><i class="glyphicon glyphicon-user"></i> Пользователи</a></li>' : '';
    $html .= ($_SESSION['user']['goodsAccess']) ? '<li'.$highlight[1].'><a href="goods.php"><i class="glyphicon glyphicon-th-list"></i> Каталог товаров</a></li>' : '';
    $html .= ($_SESSION['user']['compsAccess']) ? '<li'.$highlight[2].'><a href="companies.php"><i class="glyphicon glyphicon-home"></i> Компании</a></li>' : '';

    return $html;
}

function masterRender($title, $content, $activeMenuItem,$js = null) // Вставляет контент в общую страницу — всякий заголовки, менюшки и т.д.
{
    //if($_SESSION['user']['adminAccess'] == 1){

        /* Массив вывода сообщений и ошибок пользователюя */
    global $config;

        $messages = getMessages();

        $menu = renderMenu($activeMenuItem);
    $user_name = getUser($_SESSION['user']['user_id']);

       $url = $config['home'];
        $user_info='';

    if(isset($_SESSION['user']))
    {
        $user_info .=<<<HTML

    <div class="header_1_3">

        <img src="images/user.png" class="header_1_3_photo">


        <div class="header_1_3_1">
            <a href="index.php?action=edit">{$user_name['info_imya']}</a>
            <a href="index.php?action=logOut">Выйти</a>
        </div>
    </div>
HTML;
    }


        $html = <<<HTML
			<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/animations.css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="js/alertify/themes/alertify.core.css">
        <meta name="description" content="">
        <meta name="author" content="">
        <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
        <title>{$title}</title>
        {$js}
    </head>

  <body>
    <div id="my_notifications">{$messages}</div>

    <div class="header_1" id="top">
  <div class="content">
    <div class="header_1_1 ripplelink">
      <a href="{$url}" class="">Яблокер</a>
    </div>
    <div class="header_1_2">
      <div class="header_1_2_text">
        И один человек может что-то изменить, а <br>попытаться должен каждый.
      </div>
      <div class="header_1_2_author">
        Джон Ф. Кеннеди
      </div>
    </div>
    {$user_info}

  </div>
</div>
<!--     END   HEADER    -->
<!--        CONTENT    -->
<div class="content">
    {$content}
</div>
<div class="clr40"></div>
<script type="text/javascript" src="js/alertify/alertify.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
</body>
</html>
HTML;
        return $html;

}


/**
 * Посты
 */
/**
 * @param $user_id
 * @return all posts of user
 */
function showAllPostsOfUser($user_id)
{
    global $config;
    include_once ROOT."/admin/engine/classes/Pagination/Pagination.class.php";//ROOT.

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $users = getPostsOfUser($start, $limit, $user_id);

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    // End pagination

    $title = '<h2>Посты пользователя:</h2>';

    if(count($users) == 0) return $title.'Пока постов нет'; // Если нет строк


    $show = '<table class="blocks">
              <tr>
                <th>Статус</th>
                <th>Заголовок</th>
                <th>Дата</th>
                <th>Категория</th>
                <th>Просмотры</th>
              </tr>';
    // Разбор массива пользователей
    foreach($users as $u)
    {
        if($u['status_id']==1) $stat = '<img src="images/status_done.png">';
        if($u['status_id']==2) $stat = '<img src="images/status_time.png">';
        if($u['status_id']==3) $stat = '<img src="images/status_x.png">';
        if($u['status_id']==4) $stat = '<img src="images/status_time.png">';

        $show .= "
<tr onclick=\"window.location.href='posts.php?action=edit&post_id={$u['post_id']}'\">
<td>{$stat}</td><td>{$u['post_name']}</td><td>{$u['post_date']}</td><td>{$u['cat_name']}</td>
<td>{$u['post_views']}</td>
";
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= "</table>".$markup;
    return $show;
}

function showAllActivityOfUser($user_id)
{
    global $config;
    include_once ROOT."/admin/engine/classes/Pagination/Pagination.class.php";//ROOT.

    // Pagination
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $start = ($page-1)*$config['rowPerPage']; // Старт
    $limit = $config['rowPerPage']; // Сколько выводить на страницу
    $users = getActivityOfUser($start, $limit, $user_id);

    $countUsers = qRows(); // Общее количество пользователя для пагинаций
    $pagination = (new Pagination($page, $countUsers)); // Создаем пагинацию
    $pagination->setRPP($config['rowPerPage']); // Количество строк
    $markup = $pagination->parse(); // Генерируем пагинацию

    // End pagination

    $title = '<h2>Действия пользователя:</h2>';

    if(count($users) == 0) return $title.'Пока действий нет'; // Если нет строк


    $show = '<table class="blocks">
              <tr>
                <th>Время</th>
                <th>Категория</th>
              </tr>';
    // Разбор массива пользователей
    foreach($users as $u)
    {
        $show .= "
<tr>
<td>{$u['ac_date']}</td><td>{$u['uat_name']}</td>";
    }
    if(!empty($markup)) $markup = '<div class="panel-footer">'.$markup.'</div>';

    $show .= "</table>".$markup;
    return $show;
}


/**
 * Show Add Posts
 */

function showAddPostsForm()
{

    $groups = getGroupsPosts();
    $groupsSelect = '<select name="post_category" class="form-control input-sm"><option>Выберите категорию</option>';

    $i = 0;
    foreach($groups as $g) {
        $disable = '';//($g['id'] == 1 || ($_SESSION['user']['addModer'] == 0 && $g['id'] == 2)) ? ' disabled' : '';
        $groupsSelect .= '<option value="'.$g['cat_id'].'"'.$disable.'>'.$g['cat_name'].'</option>';
    }
    $groupsSelect .= '</select>';

    return <<<HTML
    <h2>Добавление поста</h2>
        <form action="posts.php?action=doAdd" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Название</label>
                                <input type="post_title" name="post_name" value="" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Категория</label>
                                {$groupsSelect}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Загрузить фото</label>
                                <input type="file" name="image" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Описание</label>
                                <textarea  name="post_description" class="form-control input-sm"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Текст</label>
                                <textarea name="post_text" id="editor1" cols="45" rows="5"></textarea>
                                <script type="text/javascript">
                                CKEDITOR.replace( 'editor1');
                                </script>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>SEO Title</label>
                                <input type="text" name="post_seo_title" value="" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>SEO Description</label>
                                <input type="text" name="post_seo_description" value="" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <input type="hidden" name="sub" value="1">
                                <input type="submit" name="sub" value="Добавить" class="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

            </div>
        </form>
HTML;
}


/**
 * Show Edit Posts
 */

function showEditPostsForm($post_id)
{

    $post = getPost($post_id);
    $groups = getGroupsPosts();
    $groupsSelect = '<select name="post_category" class="form-control input-sm"><option>Выберите категорию</option>';

    foreach($groups as $g) {
        $activ = '';
        $disable = '';//($g['id'] == 1 || ($_SESSION['user']['addModer'] == 0 && $g['id'] == 2)) ? ' disabled' : '';
        if($post['cat_id']==$g['cat_id']) $activ = "selected";
        $groupsSelect .= '<option value="'.$g['cat_id'].'"'.$activ.'>'.$g['cat_name'].'</option>';
    }
    $groupsSelect .= '</select>';

    return <<<HTML
    <h2>Редактирование поста</h2>
        <form action="posts.php?action=doEdit" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Название</label>
                                <input type="post_title" name="post_name" value="{$post['post_name']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>Категория</label>
                                {$groupsSelect}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                            <img src="uploads/images/{$post['post_image']}" style="width: 325px;" alt>
                                <label>Изменить фото</label>
                                <input type="checkbox" name="image_edit">
                                <input type="file" name="image" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Описание</label>
                                <textarea  name="post_description" class="form-control input-sm">{$post['post_description']}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Текст</label>
                                <textarea name="post_text" id="editor1" cols="45" rows="5">{$post['post_text']}</textarea>
                                <script type="text/javascript">
                                CKEDITOR.replace( 'editor1');
                                </script>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>SEO Title</label>
                                <input type="text" name="post_seo_title" value="{$post['post_seo_title']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <label>SEO Description</label>
                                <input type="text" name="post_seo_description" value="{$post['post_seo_title']}" class="form-control input-sm" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                                <input type="hidden" name="post_id" value="{$post_id}">
                                <input type="submit" name="sub" value="Изменить" class="btn btn-default" />
                                <a href="posts.php?action=delete&post_id={$post_id}" class="btn btn-default">Удалить </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

            </div>
        </form>
HTML;
}

?>