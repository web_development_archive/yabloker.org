<?php
/**
 * Created by PhpStorm.
 * User: erzhan_kst
 * Date: 29.01.2015
 * Time: 10:38
 */

include "engine/core.php";
include "engine/visual.php";

$action = $_GET['action'];

switch($action){
    case "delete":
        delPost($_GET['post_id']);
        header("Location:index.php");
        break;
    case "doAdd":
        if(isset($_POST['sub']) && isset($_FILES['image'])){
            if(addPost($_POST,$_FILES['image'])) header("Location: index.php");
            else header("Location: posts.php?action=new");
        }
        break;
    case "new":
        $content = showAddPostsForm();
        echo masterRender('Добавить пост', $content, 0);
        break;
    case "edit":
        if(is_numeric($_GET['post_id'])){
            $content = showEditPostsForm($_GET['post_id']);
            echo masterRender('Редактировать пользователя', $content, 0);
        }
        break;
    case "doEdit":
        if(is_numeric($_POST['post_id'])){
            if(editPost($_POST,$_FILES['image'])) header("Location: index.php");
            else header("Location: posts.php?action=edit&post_id=".$_POST['post_id']);
        }
        break;
    default:
        $content['left'] = genereateCategories();
        $content['right'] = '<div class="box">'.showGoods().'</div>';
        echo masterRender('Товары',  $content, "left-sidebar");
}

?>