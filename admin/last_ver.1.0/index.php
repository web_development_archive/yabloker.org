<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/animations.css">  
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
  <!--      BOOTSTRAP    
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>-->
</head>
<body>
<!--        HEADER    -->
<div class="header_1" id="top">
  <div class="content">
    <div class="header_1_1 ripplelink">
      <a href="" class="">Яблокер</a>
    </div>
    <div class="header_1_2">
      <div class="header_1_2_text">
        И один человек может что-то изменить, а <br>попытаться должен каждый.
      </div>
      <div class="header_1_2_author">
        Джон Ф. Кеннеди
      </div>
    </div>
    <div class="header_1_3">
      <img src="images/img.png" class="header_1_3_photo">
    </div>
  </div>
</div>
<!--     END   HEADER    -->
<!--        CONTENT    -->

<div class="content">
  <div class="blocks content_header_text">- Время менять этот мир к лучшему. Начнем с себя -</div>
  <div class="clr30"></div>
  <div>
    <div class="content_1 blocks">
      <div class="content_1_header">
        Сообщения / <div class="content_1_header_new">+1</div>
      </div>
      <div class="content_1_footer">
        <img src="images/admin_sms.png">
      </div>
    </div>
    <div class="clr34"></div>
    <div class="content_1 blocks">
      <div class="content_1_header">
        Написать новый шедевр
      </div>
      <div class="content_1_footer">
        <img src="images/admin_plus.png">
      </div>
    </div>
    <div class="clr34"></div>
    <div class="content_1 blocks">
      <div class="content_1_header">
        Профиль
      </div>
      <div class="content_1_footer">
        <div class="content_1_footer_1">
          <div class="content_1_footer_1_n">312</div>
          <div class="content_1_footer_1_n">1200</div>
          <div class="content_1_footer_1_n">543</div>
        </div>
        <div class="content_1_footer_1">          
          <div class="content_1_footer_1_t">статей</div>
          <div class="content_1_footer_1_t">просмотров</div>
          <div class="content_1_footer_1_t">комментария</div>
        </div> 
      </div>
    </div>
  </div>
  <div class="clr30"></div>

  <div class="blocks content_header_text">- Мои статьи -</div>
  <div class="clr30"></div>
  <div class="content_table">
    <table class="blocks">
      <tr>
        <th>Статус</th>
        <th>Заголовок</th>
        <th>дата</th>
        <th>категория</th>
        <th>просмотры</th>
      </tr>
      <tr>
        <td><img src="images/status_done.png"></td>
        <td>Новый взгляд на продуктивность, или Что не так с нашей работой</td>
        <td>25 Янвая 2015</td>
        <td> Лайфхаки</td>
        <td>99 921</td>
      </tr>
      <tr>
        <td><img src="images/status_time.png"></td>
        <td>Новый взгляд на продуктивность, или Что не так с нашей работой</td>
        <td>25 Янвая 2015</td>
        <td> Лайфхаки</td>
        <td>99 921</td>
      </tr>
      <tr>
        <td><img src="images/status_x.png"></td>
        <td>Новый взгляд на продуктивность, или Что не так с нашей работой</td>
        <td>25 Янвая 2015</td>
        <td> Лайфхаки</td>
        <td>99 921</td>
      </tr>
    </table>
  </div>
</div>
<div class="clr40"></div>

<script type="text/javascript" src="js/main.js"></script>
</body>
</html>