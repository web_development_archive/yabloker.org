<?php
/**
 * Created by PhpStorm.
 * User: erzhan_kst
 * Date: 29.01.2015
 * Time: 10:38
 */

include "engine/core.php";
include "engine/visual.php";

$action = $_GET['action'];

switch($action){
    case "delete":
        deleteUser($_GET['user_id']);
        header("Location:index.php");
        break;
    case "doAdd":
        if(isset($_POST['sub'])){
            if(addUser($_POST)) header("Location: index.php");
            else header("Location: users.php?action=new");
        }
        break;
    case "new":
        $content = showAddUserForm();
        echo masterRender('Добавить пользователя', $content, 0);
        break;
    case "edit":
        if(is_numeric($_GET['user_id'])){
            $content = showUserEditForm($_GET['user_id']);
            $content .='<div class="row"><div class="col-lg-12"><div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">';

            $content .= showAllPostsOfUser($_GET['user_id']);
            $content .='</div> <div class="col-lg-6 col-md-6 col-sm-6">';

            $content .= showAllActivityOfUser($_GET['user_id']);

            $content .= '</div></div></div></div>';
            echo masterRender('Редактировать пользователя', $content, 0);
        }
        break;
    case "doEdit":
        if(is_numeric($_POST['user_id'])){
            if(editUser($_POST)) header("Location: index.php");
            else header("Location: users.php?action=edit&user_id=".$_POST['user_id']);
        }
        break;
    default:
        $content['left'] = genereateCategories();
        $content['right'] = '<div class="box">'.showGoods().'</div>';
        echo masterRender('Товары',  $content, "left-sidebar");
}

?>