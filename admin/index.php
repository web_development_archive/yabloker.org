<?php
/*
if(!isset($_REQUEST['authorize']) && !isset($_SESSION['user']))  // Обрубаем незалогиненых чуваков
{
    header('Location: index.php?authorize=1');
}*/
include "engine/admin_core.php"; // Главный файл для админки. Проверяет вошел ли пользователь в админку

$action = $_GET['action'];

switch($action)
{
    case "logOut":
        session_start();
        session_destroy();
        header("Location: index.php");
        break;
    case "edit":
        if(is_numeric($_SESSION['user']['user_id'])){
            $content = showUserEditFormSecond($_SESSION['user']['user_id']);

            echo masterRender('Редактировать данные', $content, 0);
        }
        break;
    case "doEdit":
        if(is_numeric($_POST['user_id'])){
            if(editUserSecond($_POST)) header("Location: index.php");
            else header("Location: users.php?action=edit&user_id=".$_POST['user_id']);
        }
        break;
    default:
        $js =
        '
        <script type="text/javascript">
            $(document).ready( function (){
                $(".content_1_show_first").click(function(e){
                    $(".content_table_pages").hide();
                    $(".content_table_posts").hide();
                    $(".content_table_users").slideToggle("slow");

                });
                $(".content_1_show_second").click(function(e){
                    $(".content_table_posts").hide();
                    $(".content_table_users").hide();
                    $(".content_table_pages").slideToggle("slow");
                });
                $(".content_1_show_third").click(function(e){
                    $(".content_table_users").hide();
                    $(".content_table_pages").hide();
                    $(".content_table_posts").slideToggle("slow");
                });            });
        </script>
        ';




        if($_SESSION['user']['type_id']!=1)
            $content = showContentAdmin($_SESSION['user']);
        else
            $content = showGodContent();

            echo masterRender('Админ панель', $content, 0,$js);

}

?>

